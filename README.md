[[_TOC_]]

## Creating the ML environment

For secure and predictable use, be sure to use a virtual environment:

    python3 -m venv /path/to/new/virtual/environment

if lalsuite is already installed, you can use the `--system-site-packages` option to include it and all other packages already installed.

Then, after activating your virtual environment:

    source /path/to/new/virtual/environment/bin/activate

install PyTorch with pip by following the instructions here: https://pytorch.org/. \
Currently, I have pytorch installed with version 11 for CUDA! To see your CUDA version, run `nvcc --version`. If `nvcc` is not added to $PATH, then run it directly from `/usr/local/cuda/bin/nvcc`. To see more info about the GPU's (and still get the CUDA version) you can run `nvidia-smi`.  

Also be sure to intall WebDataSet with `pip install webdataset`, and if needed h5py with `pip install h5py`.


## Current set-up
### Multi-GPU
This is currently working by using [`DataParallel`](https://pytorch.org/docs/stable/generated/torch.nn.DataParallel.html#torch.nn.DataParallel)  
However, when there is a mis-match between GPU's, it might become slower than running everything on the fastest GPU. Each batch is simply split in equal parts and send to all the GPU's, so you have to wait for the slowest GPU. This might be fixable by implementing [`DistributedDataParallel'](https://pytorch.org/docs/stable/generated/torch.nn.parallel.DistributedDataParallel.html#torch.nn.parallel.DistributedDataParallel) instead, which is the recommended way of doing multi GPU anyway and has the ability to extend beyond single-node training.  

### Dataset
The dataset consists of 1000 waveforms with the astrophysical distribution outlined in the paper https://arxiv.org/pdf/1712.06041.pdf, the waveforms were generated with the TaylorT2 pproximant and simulated on the H1 detector. Gaussian noise is generated to match the detectors PSD. Each waveform is placed in a segment, with the location of the peak amplitude placed randomly between 0.75 and 0.95 of the segment. This is done for twenty (independent) noise realisations for each waveform.

The shortest waveform is 1.66 s, the longest 130.41 s and the average duration is is 20.62 s.
### Performance
*This is outdated, to be updated soon*
The network performs reasably well, being able to correctly classify the majority of data. During training, the loss on in-sample and out-of-sample data don't seem to diverge during 300 epochs meaningfully using the Adam optimizer with a learning rate of $`10^{-5}`$.  This is all summarized in the plots below.  

![Four plots showing, from top left to bottom right, loss, accuracy, true negative rate, true positive rate.](/synthetic_test/Plots/validation_lr_1e-5_adam.svg)  

![One plot showing loss for each batch during training, with loss for each epoch for the validation set.](/synthetic_test/Plots/train_losses_lr_1e-5_adam.svg)

## Milestones
- [ ] Reproduce the results from https://arxiv.org/pdf/1712.06041.pdf
- [ ] Add (posterior) parameters regression using [Swyft](https://swyft.readthedocs.io/en/latest/)
- [ ] Simultanious parameter and significance estimation

## To do
### Short term
- Data
   - Implement SNR-based data instead of standard devation.
   - Speed up data generation?

- GPU
    - Keep all batches and targets in system memory, as DataParallel will send it to the appropariate GPU when needed. This allows for larger batches than a single GPU would be able to hold in memory.
    

### Longer term
- Variable and flexible network architecture.
- Get noise data from the detector.
- Include glitches.
- Include external parameters for waveforms, such as detector response.
- Perform signal-injection search.
- Implement cross-validation.
- Implement curricilum learning.
- Add multiple detector channels as input.
- Do regression instead of (only) classification.
- Implement [`DistributedDataParallel'](https://pytorch.org/docs/stable/generated/torch.nn.parallel.DistributedDataParallel.html#torch.nn.parallel.DistributedDataParallel).

### Known bugs
- Multiprocessing leads to a lot of temporary files and directories in the parent directory of --train-dir. Currenntly I remove them manually with [extended glob](https://www.tecmint.com/delete-all-files-in-directory-except-one-few-file-extensions/).

## Future outlook
### Waveforms that are partially in a segment
Instead of giving a binary 'one hot' response, it could be of interest to have a response that scales with the amount of signal that's in the segment. The idea is that a signal with have a distinct time-evolution of network output. Currently I'm thinking of:
- Percentage of total time in segment that contains signal.
- Percentage of power in segment that comes from signal.
- Signal-to-noise ratio in the segment.

### Wavelet Kernel
Inspired by the use of a wavelet-transform in burst searches, I'm interested in implementing a wavelet kernel. This has not been done much, but there is one example that shows promising results: https://www.tandfonline.com/doi/pdf/10.1080/15325008.2020.1854384?casa_token=2iKAzFdNYqUAAAAA%3A09RmeTD0N3tDJN65hFR4GVPrBjHcX5CZh7EIoKUck1DX8RQf-i0j-XNQJ39cWaVfrF2U9Zq9T6Zs& Next to wavelet-kernel channels, there can still be regular kernel channels to capture non-wavelet information. Inspecting the network after training can tell us about which channels are mostly used.

### Genetic algorithm
Use a genetic algorithm to tune the hyperparameters of a model. Measure of fitness should be an appropariate summary of validation results. Along with random initialisations, include some that were found to work well by hand.

### Topological Data Analysis
Inlude persistant (landscape) of a delayed time embedding of the signal as input to the network, similar to https://arxiv.org/pdf/1910.08245.pdf.

### Manifold-valued data
Generlise the convolution to manifold valued data by using a weighted Fréchet mean. A challenge here is defining a metric on time-series. A possibility would be to create a time-delay embedding, calculate the correlation mmatrix and use the metric on Singular Positive Definite (SPD) matrices as is done here: https://arxiv.org/pdf/1809.06211.pdf. Alternitavely, one can produce a correlation matrix from the output channels of a first convolution layer.

### Functions on a manifold: spherical CNN
The detectors on the surface of the earth are probably acting functions defined on a manifold, akin to camera's on a self-driving car. I want to investigate this after having added support for multiple detectors. There is a short passage on this in the article on manifold-valued convolution linked above.
