from optparse import OptionParser
import h5py

import numpy as np
from scipy import stats
from time import time

from Simulation import SimulateGW

from tqdm import tqdm

"""
Generates detector-response waveforms in time domain, with the same
distribution as https://arxiv.org/pdf/1712.06041.pdf. The waveforms
are saved in an hdf5 file for later use.
"""

def parse_command_line():
    parser = OptionParser(description = __doc__)
    parser.add_option("--num-wave",type="int", dest = "n_wave", help = "Set the number of waveforms to generate.")
    parser.add_option("--approx", type='string', dest = "approx", help = "Set the LAL approximant.")
    detectors = ["H1", "L1", "V1"]
    parser.add_option("--detector", type ="choice", choices = detectors, dest = "detector", help = f"Set the detector, choices are:{', '.join(detectors)}")
    #TODO: make approximant limited choice by pulling the available approximants from lalsim (or hardcode them)
    parser.add_option("--out-file", metavar = "filename", dest = "f_out", help = "Set the output hdf5 filename with extension.")


    options, filenames = parser.parse_args()
    return options, filenames


def gen_wave_pars(N=1, m_min=5., m_max=95.):
    """
    Generates parameters for N waveforms, returns an array where each entry  is
    a parameter vector.

    Mass given in solar masses
    """

    # Masses are drawn according to $m_{1,2} \sim \log m_{1,2}
    # This uses the natural log. That's no problem as any base would work
    # https://en.wikipedia.org/wiki/Reciprocal_distribution
    m_dist = stats.loguniform(m_min, m_max)
    m1 = m_dist.rvs(size=N)
    m2 = m_dist.rvs(size=N)

    two_pi_flat = stats.uniform(loc=0., scale=2*np.pi)

    # Phase drawn uniformly from $[0,2\pi]$
    phase = two_pi_flat.rvs(size=N)

    # Easier to draw the uniform, since the transformed distribution has
    # a singularity at angle 0
    # Maybe there is a way to do this with distribution from scipy?
    # Could make my own if needed to speed up
    # Inclination drawn from uniform cosine of angle
    cos_incl = stats.uniform(loc=-1., scale=2.).rvs(size=N)
    incl = np.arccos(cos_incl)

    # Right ascension and declination come from uniform isotropic prior
    ra = two_pi_flat.rvs(size=N)
    decl = stats.uniform(loc=-0.5*np.pi, scale=np.pi).rvs(size=N)

    # Polarization angle drawn uniformly
    pol = two_pi_flat.rvs(size=N)

    # this does the same as zip (in this case), but returns an array instead
    return np.stack((m1, m2, phase, incl, ra, decl, pol), axis=1)


def generate_waveforms(filename, pars_arr, sim):
    """
    Generates waveforms and saves them in an hdf5 file.
    """
    with h5py.File(filename, 'w') as f:
        # loop over waveforms in parameter form
        idx = 0
        for pars in tqdm(pars_arr):
            # simulate detector response
            h = sim(pars)

            # create a group for the waveform in the hdf5 structure with
            # idx as name
            grp = f.create_group(str(idx))

            # save the parameters as attribute in dictionary form
            pars_dict = dict(zip(["m1", "m2", "phase", "incl",
                                  "ra", "decl", "pol"],
                                 pars))
            grp.attrs.update(pars_dict)

            # to keep similarity with the lal data format we create a single
            # dataset with the name "data" in the current group
            # and give it the non trivial attributes of the REAL8TimeSeries
            dset = grp.create_dataset("data", data=h.data.data)

            dset.attrs['f0'] = h.f0
            dset.attrs['deltaT'] = h.deltaT
            dset.attrs['length'] = h.data.length

            idx += 1


if __name__ == "__main__":
    # command line arguments should only be used when this is called as main
    options, filenames = parse_command_line()

    # generate parameters
    pars_arr = gen_wave_pars(N=options.n_wave)

    print("Generating and saving waveforms...")
    t0 = time()
    idx = 0

    # initialize simulator
    sim = SimulateGW(srate=8192., approx=options.approx,
                     detector=options.detector)

    # generate and save
    generate_waveforms(options.f_out, pars_arr, sim)

    t1 = time()

    # time formatting
    m, s = divmod(t1 - t0, 60)
    h, m = divmod(m, 60)
    s, m, h = int(s), int(m), int(h)
    print("\nGenerating and saving detector response took "
          f"{str(h)+'hours ' if h > 0 else ''}"
          f"{str(m)+' minutes ' if m>0 else ''}"
          f"{str(s)+' seconds ' if s>0 else ''}")
