import h5py

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.ioff()
from optparse import OptionParser
import sys

def parse_command_line():
	parser = OptionParser(description = __doc__)
	parser.add_option("--tmplt-file", metavar = "filename",dest="tmplts", default = "templates_TD.hdf5", help = "Set the hdf5 original template file.")
	parser.add_option("--index", dest = "idx",type = "int", default = 0,  help = "Set the index to check")

	options, filenames = parser.parse_args()
	return options, filenames

options, filenames = parse_command_line()

with h5py.File(options.tmplts, 'r') as f:
	signal = f[str(options.idx)]['hplus'][()]

size = len(signal)
k_size = 2.

std = np.std(signal)
#multiplier for standard deviation
k_std = 3.

noise = np.random.normal(0, k_std*std, int(k_size*size))

#zero-pad
diff = len(noise) - len(signal)
cut = diff//2
signal = np.concatenate((np.zeros(cut), signal, np.zeros(diff - cut)))

#plot
plt.plot(noise+signal)
plt.plot(signal)
plt.savefig("signal_with_noise.png")
