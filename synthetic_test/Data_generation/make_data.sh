#!/bin/bash

for snr in {10..1}
do
  python3 make_test_data.py --in-file IMRPhenomD-H1-1000-testset.hdf5 --basename SNR$snr --out-dir /project/gravwav/kkoender/datasets/IMRPhenomD --shard-size 5e9 --shard-samples 2400 --snr $snr
done
