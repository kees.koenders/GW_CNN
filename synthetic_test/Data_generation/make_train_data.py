import h5py
import webdataset as wds
from optparse import OptionParser
import math
import numpy as np
import os

from Dataset_Generation import gen_psd, gen_noise, make_dataset

'''
TODO: change to only 1 second of data, add tukey window to the end of the
signal so the CNN is not picking up edge effects
'''


def parse_command_line():
    parser = OptionParser(description=__doc__)
    parser.add_option("--in-file", type="string", dest="in_file", default=0,
                      help="Set the hdf5 file with template waveforms")
    parser.add_option("--out-dir", type="string", dest="out_dir",
                      help="Set the output directory")
    parser.add_option("--basename", type="string", dest="basename",
                      help="Set the output base name")
    parser.add_option("--shard-size", type="float", dest="shard_size",
                      default=1e9, help="Set the maximum shard size in bytes")
    parser.add_option("--shard-samples", type="float",
                      dest="shard_samples", default=1e3,
                      help="Set the maximum amount of samples in a shard")
    parser.add_option("--train-frac", type="float",
                      dest="train_frac", default=.9,
                      help="Set the fraction of data to be used for training")
    parser.add_option("--snr", type="float", dest="snr", default=10,
                      help="Set the SNR to which the waveform will be scaled")

    options, filenames = parser.parse_args()
    return options, filenames


options, filenames = parse_command_line()

# First we need some basics
flow = 10
srate = 8192  # sampling rate in Hz

# One seconds of data
seg_len = srate

# Create PSD
psd_name = "aLIGOZeroDetHighPower"
psd = gen_psd(psd_name, flow, srate, seg_len)

shard_size = int(options.shard_size)
shard_samples = int(options.shard_samples)

base_dir = os.path.join(options.out_dir, "shards_"+options.basename)

train_dir = os.path.join(base_dir, "training")
val_dir = os.path.join(base_dir, "validation")

# make sure directory exists
if not os.path.exists(train_dir):
    os.makedirs(train_dir)

if not os.path.exists(val_dir):
    os.makedirs(val_dir)
# TODO clear directory if it is not empty
# TODO do zero-padding in here as well

with h5py.File(options.in_file, 'r') as f:
    keys = list(f.keys())
    n_waveforms = len(keys)

    # we shuffle the key indices
    key_indices = np.arange(n_waveforms)
    np.random.shuffle(key_indices)

    # Split in training and validation
    n_train = int(options.train_frac*n_waveforms)
    n_val = n_waveforms - n_train
    key_indices_train = key_indices[:n_train]
    key_indices_val = key_indices[n_train:]

    # repeat each array 25 times and shuffle again and make an iterator
    train_indices = np.tile(key_indices_train, 25)
    np.random.shuffle(train_indices)
    train_indices = iter(train_indices)

    val_indices = np.tile(key_indices_val, 25)
    np.random.shuffle(val_indices)
    val_indices = iter(val_indices)

    # 0 is noise 1 is signal, used for book keeping
    train_classes = np.concatenate((np.ones(25*n_train, dtype=int),
                                    np.zeros(25*n_train, dtype=int)))
    np.random.shuffle(train_classes)

    val_classes = np.concatenate((np.ones(25*n_val, dtype=int),
                                  np.zeros(25*n_val, dtype=int)))
    np.random.shuffle(val_classes)

    train_mag = int(math.log10(n_train))
    val_mag = int(math.log10(n_val))
    # bound on number of shards
    # TODO: bound can be higher if shard size is the limiting factor
    # To be on the safe side, using one magnitude higher
    max_train_shards = n_train//shard_samples + 1
    max_val_shards = n_val//shard_samples + 1
    train_shard_mag = int(math.log10(max_train_shards)) + 1
    val_shard_mag = int(math.log10(max_val_shards)) + 1

    train_pattern = os.path.join(train_dir,
                                 (f"{options.basename}-training-%0"
                                  f"{train_shard_mag + 1}d.tar"))

    train_shard_samples, train_shards = make_dataset(psd, srate, seg_len,
                                                     options.snr,
                                                     train_pattern,
                                                     shard_size,
                                                     shard_samples, f,
                                                     train_indices,
                                                     train_classes, train_mag)
    val_pattern = os.path.join(val_dir,
                               (f"{options.basename}-validate-%0"
                                f"{val_shard_mag + 1}d.tar"))
    print("writing validation data:")
    val_shard_samples, val_shards = make_dataset(psd, srate, seg_len,
                                                 options.snr,
                                                 val_pattern, shard_size,
                                                 shard_samples, f,
                                                 val_indices,
                                                 val_classes, val_mag)

    # write the metadata
    # TODO: generalize metadata more, maybe have seperate metadata files for
    # training, validation and testing sets?
    # use std from generated noise
    std = np.std(gen_noise(psd, srate, seg_len))

    meta_dict = {
        "type": "train",
        "n_train": len(train_classes),
        "n_val": len(val_classes),
        "train_shard_samples": train_shard_samples,
        "val_shard_samples": val_shard_samples,
        "train_shards": train_shards,
        "val_shards": val_shards,
        "train_mag": train_mag,
        "val_mag": val_mag,
        "train_shard_mag": train_shard_mag,
        "val_shard_mag": val_shard_mag,
        "std": std,
        "name": options.basename
    }

    print("writing metadata")
    meta_path = os.path.join(base_dir,
                             f"{options.basename}-metadata.tar")

    metadata = {"__key__": "metadata", "pyd": meta_dict}
    sink = wds.TarWriter(meta_path)
    sink.write(metadata)
    sink.close()
