import h5py
import webdataset as wds
from optparse import OptionParser
import math
import numpy as np
import os

from Dataset_Generation import gen_psd, gen_noise, make_dataset
""""
This script makes a testing dataset matching certain astronomical
distributions.
"""


def parse_command_line():
    parser = OptionParser(description=__doc__)
    parser.add_option("--in-file", type="string", dest="in_file", default=0,
                      help="Set the hdf5 file with template waveforms")
    parser.add_option("--out-dir", type="string", dest="out_dir",
                      help="Set the output directory")
    parser.add_option("--basename", type="string", dest="basename",
                      help="Set the output base name")
    parser.add_option("--shard-size", type="float", dest="shard_size",
                      default=1e9, help="Set the maximum shard size in bytes")
    parser.add_option("--shard-samples", type="float",
                      dest="shard_samples", default=1e3,
                      help="Set the maximum amount of samples in a shard")
    parser.add_option("--snr", type="float", dest="snr", default=10,
                      help="Set the SNR to which the waveform will be scaled")

    options, filenames = parser.parse_args()
    return options, filenames


options, filenames = parse_command_line()

# First we need some basics
flow = 10
srate = 8192  # sampling rate in Hz

# One seconds of data
seg_len = srate

# Create PSD
psd_name = "aLIGOZeroDetHighPower"
psd = gen_psd(psd_name, flow, srate, seg_len)

shard_size = int(options.shard_size)
shard_samples = int(options.shard_samples)

test_dir = os.path.join(options.out_dir, "shards_"+options.basename, "testing")

# make sure directory exists
if not os.path.exists(test_dir):
    os.makedirs(test_dir)

# TODO clear directory if it is not empty

# TODO check if in file exists, otherwise generate it

with h5py.File(options.in_file, 'r') as f:
    keys = list(f.keys())
    n_waveforms = len(keys)

    # now, we shuffle the key indices
    key_indices = np.arange(n_waveforms)
    np.random.shuffle(key_indices)

    # repeat each array 25 times and shuffle again and make an iterator
    test_indices = np.tile(key_indices, 25)
    np.random.shuffle(test_indices)
    test_indices = iter(test_indices)

    # 0 is noise 1 is signal, used for book keeping
    test_classes = np.concatenate((np.ones(25*n_waveforms, dtype=int),
                                   np.zeros(25*n_waveforms, dtype=int)))
    np.random.shuffle(test_classes)

    test_mag = int(math.log10(n_waveforms*25))
    # bound on number of shards
    # TODO: bound can be higher if shard size is the limiting factor
    # To be on the safe side, using one magnitude higher
    max_test_shards = n_waveforms*25//shard_samples + 1
    test_shard_mag = int(math.log10(max_test_shards)) + 1

    test_pattern = os.path.join(test_dir, (f"{options.basename}-testing-%0"
                                           f"{test_shard_mag + 1}d.tar"))
    print("writing testing data:")
    test_shard_samples, test_shards = make_dataset(psd, srate, seg_len,
                                                   options.snr, test_pattern,
                                                   shard_size, shard_samples,
                                                   f, test_indices,
                                                   test_classes, test_mag)

    # write the metadata
    # use std from generated noise
    std = np.std(gen_noise(psd, srate, seg_len))
    meta_dict = {
        "type": "test",
        "n_test": len(test_classes),
        "shard_samples": test_shard_samples,
        "shards": test_shards,
        "mag": test_mag,
        "shard_mag": test_shard_mag,
        "std": std,
        "name": options.basename
    }

    print("writing metadata")
    meta_path = os.path.join(test_dir,
                             f"{options.basename}-metadata.tar")

    metadata = {"__key__": "metadata", "pyd": meta_dict}
    sink = wds.TarWriter(meta_path)
    sink.write(metadata)
    sink.close()
