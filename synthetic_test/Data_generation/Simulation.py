import lal
import lalsimulation as lalsim
import numpy as np


class SimulateGW:
    """
    Simulates a graviational wave as seen by a detector. Returns a lal
    timeseries.
    """
    def __init__(self, srate=2048., approx="IMRPhenomD", detector="H1"):
        self.approx = approx
        self.lal_detector = lalsim.DetectorPrefixToLALDetector(detector)
        self.srate = srate  # sampling rate in Hz

    def __call__(self, pars):
        m1, m2, incl, phase, ra, decl, pol = pars
        parameters = {}

        parameters['m1'] = lal.MSUN_SI*m1
        parameters['m2'] = lal.MSUN_SI*m2
        parameters['S1x'] = 0.
        parameters['S1y'] = 0.
        parameters['S1z'] = 0.
        parameters['S2x'] = 0.
        parameters['S2y'] = 0.
        parameters['S2z'] = 0.
        parameters['distance'] = 1.e6 * lal.PC_SI
        parameters['inclination'] = incl
        parameters['phiRef'] = phase
        parameters['longAscNodes'] = 0.
        parameters['eccentricity'] = 0.
        parameters['meanPerAno'] = 0.
        parameters['deltaT'] = 1./self.srate
        parameters['f_min'] = 10.0
        parameters['f_ref'] = 0.
        parameters['LALparams'] = None
        parameters['approximant'] = lalsim.GetApproximantFromString(self.approx)

        hplus, hcross = lalsim.SimInspiralTD(**parameters)

        # for some reason a GPS leap-second error can occur
        epoch = lal.LIGOTimeGPS(0)
        hplus.epoch = epoch
        hcross.epoch = epoch

        h = lalsim.SimDetectorStrainREAL8TimeSeries(hplus, hcross, ra, decl,
                                                    pol, self.lal_detector)

        return h
