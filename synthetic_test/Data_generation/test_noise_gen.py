import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import lal
import lalsimulation as lalsim
import pycbc.psd
import pycbc.noise as noise
from pycbc.types import FrequencySeries
import numbers
import time

'''
This is a script detailing how to use lalsim to generate a psd and noise with python.
It also shows how to create some of the lal datatypes in python
'''
t0 = time.time()

#First we need some basics
flow = 40
duration = 32 #duration of segment in seconds
srate = 4096 #sampling rate in Hz
slength = int(duration*srate) #segment length
#stride = slength//2

#now the needed lal datatypes
rng = lal.gsl_rng("ranlux", 127) #wrapper for a gsl rng
epoch = lal.LIGOTimeGPS(0) #gps time in seconds, nanoseconds
df = 1./duration
#starting frequency is set to a very small number due to bug in LALSimNoisePSD.c
psd= lal.CreateREAL8FrequencySeries('', epoch, 1e-25, df, lal.SecondUnit, slength//2 + 1)
seg = lal.CreateREAL8TimeSeries("STRAIN", epoch, 0, 1./srate, lal.StrainUnit, slength)

#Create PSD
psd_name = "aLIGOZeroDetHighPower"
_name_prefix = 'SimNoisePSD'
#Adding 'Ptr' to the end of a lalsuite function returns it as a SWIG wrapped pointer to the C function, as opposed to returning a python callable
_name_suffix = 'Ptr'

psd_func = lalsim.__dict__[_name_prefix + psd_name + _name_suffix]
lalsim.SimNoisePSD(psd, flow, psd_func)

#psd_alt = pycbc.psd.from_string(psd_name, slength//2 +1, df, flow).lal() #pycbc.psd.aLIGOZeroDetHighPower(slength//2 +1, df, flow).lal()

#Simulate noise
lalsim.SimNoise(seg, seg.data.length, psd, rng)
lalsim.SimNoise(seg, seg.data.length, psd, rng)
noise_series = seg.data.data

t1 = time.time()

print(f"{t1-t0:.3f} seconds")

#plotting
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

plt.plot(noise_series)
plt.savefig("noise_gen_test.png")
