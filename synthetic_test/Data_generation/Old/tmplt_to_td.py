import h5py
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.ioff()
from optparse import OptionParser
import sys

def parse_command_line():
	parser = OptionParser(description = __doc__)
	parser.add_option("--tmplt-file", metavar = "filename",dest="tmplts", help = "Set the hdf5 template file.")
	parser.add_option("--out-file", metavar = "filename", dest = "out", help = "set the hdf5 output file.")

	options, filenames = parser.parse_args()
	return options, filenames

options, filenames = parse_command_line()

with h5py.File(options.tmplts, 'r') as f_tmplts:
	key_list = list(f_tmplts.keys())
	n_tmplts = len(key_list)
	print("number of templates to convert: %i"%n_tmplts)
	
	count = 0
	wvfrms = []
	#go through all templates
	for key_x in key_list:

		tmplt = f_tmplts[key_x]
		hplus = tmplt['hplus'][()]
		#hcross = tmplt['hcross'][()]
		f_len = tmplt['hplus'].attrs['length']
		df = tmplt['hplus'].attrs['deltaF']
		f_start = tmplt['hplus'].attrs['f0']
	
		#Only hplus for now, remove first 100 due to boundary issue	
		hplus_td = np.fft.irfft(hplus)[100:]*df
		wvfrms.append(hplus_td)
		count +=  1

		sys.stdout.write("\r\033[K")
		sys.stdout.write("\rConverting to timedomain: %.2f%%"%(100.*count/(n_tmplts)))
		sys.stdout.flush()

	sys.stdout.write("\n")
	
	wvfrms = np.array(wvfrms)
	with h5py.File(options.out, 'w') as f_wvrm:
		f_wvrm.create_dataset("data", data = wvfrms)	


