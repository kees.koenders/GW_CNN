import h5py
import numpy as np
from optparse import OptionParser
import webdataset as wds

from tqdm import tqdm

import os

def parse_command_line():
    parser = OptionParser(description = __doc__)
    parser.add_option("--in-file", type = "string", dest = "in_file", help = "set the input hdf5 file to convert")
    parser.add_option("--out-dir", type = "string", dest = "out_dir", help = "set directory to place shards in")
    parser.add_option("--out-name", type = "string", dest = "out_name", help = "set the base name of each shard")
    parser.add_option("--shard-size", type = "int", dest = "shard_size", default = 5000000000, help = "set the shard size in bytes (so 1e9 is a GB")
    parser.add_option("--shard-records", type = "int", dest = "shard_records", default = 1000, help = "set the max number of records in a shard")
    parser.add_option("--std-sample", type = "float", dest = "std_sample", default = 1., help = "set the fraction of data used to calculate std")
    parser.add_option("--train-frac", type = "float", dest = "train_frac", default = .9, help = "set the fraction of data to become training data")
    options, filenames = parser.parse_args()
    return options, filenames

options, filnames = parse_command_line()

train_dir = os.path.join(options.out_dir, "training")
val_dir = os.path.join(options.out_dir, "validation")

#make sure directory exists
if not os.path.exists(train_dir):
    os.makedirs(train_dir)

if not os.path.exists(val_dir):
    os.makedirs(val_dir)
#TODO clear directory if it is not empty

with h5py.File(options.in_file, 'r') as f:
    data = f['data']
    labels = f['labels']
    
    n_samples = data.shape[0]

    classes = np.argmax(labels, axis=1)
    #to be sure, shuffle
    indices = np.arange(n_samples)
    np.random.shuffle(indices)

    #split in training and validation
    n_train = int(options.train_frac*n_samples)
    n_val = n_samples - n_train

    #I like to declare variables when I need them in a larger scope
    train_shards = 0
    val_end_idx = 0

    #path to save shards, should not have more than 1e6 of them
    train_pattern = os.path.join(options.out_dir, "training",f"{options.out_name}-train-%06d.tar")
    print("writing training data:")
    with wds.ShardWriter(train_pattern, maxsize = options.shard_size, maxcount = options.shard_records) as sink:
        for i in tqdm(indices[:n_train]):
            gwave = np.real(data[i])
            cls = classes[i]
            
            #key is just the index, we expect less than 2e5 total waveforms
            key = "%06d" % i
            
            #for now encode the time-series as a python pickle
            #other supported formats would be pytorch pickle (pth), image (jpg), tensor (ten). See:https://modelzoo.co/model/webdataset
            sample = {"__key__": key, "pyd": gwave, "cls": cls}
            
            #write to the tar archive
            sink.write(sample)

        #get values for metadata
        train_shards = sink.shard
        print(f"Total amount of training shards written: {train_shards}")

    val_pattern = os.path.join(options.out_dir, "validation", f"{options.out_name}-validate-%06d.tar")
    print("writing validation data:")
    with wds.ShardWriter(val_pattern, maxsize = options.shard_size, maxcount = options.shard_records) as sink:
        for i in tqdm(indices[n_train:]):
            gwave = np.real(data[i])
            cls = classes[i]
            
            #key is just the index, we expect less than 2e5 total waveforms
            key = "%06d" % i
            
            #for now encode the time-series as a python pickle
            #other supported formats would be pytorch pickle (pth), image (jpg), tensor (ten). See:https://modelzoo.co/model/webdataset
            sample = {"__key__": key, "pyd": gwave, "cls": cls}
            
            #write to the tar archive
            sink.write(sample)

        #metadata
        val_shards = sink.shard
        print(f"Total amound of validation shards written: {val_shards}")

    #write the metadata
    n_std = int(options.std_sample*n_samples)
    print("calculating std...")
    std = np.std(data[np.sort(indices[:n_samples]),:])
    print("done!")
    
    meta_dict = {
        "n_train": n_train,
        "n_val": n_val,
        "train_shards": train_shards,
        "val_shards": val_shards,
        "std": std,
        "name": options.out_name
    }
    
    print("writing metadata")
    meta_path = os.path.join(options.out_dir, f"{options.out_name}-metadata.tar")
    
    metadata = {"__key__": "metadata", "pyd":meta_dict}
    sink = wds.TarWriter(meta_path)
    sink.write(metadata)
    sink.close()

print("all done!")
