from optparse import OptionParser
from ligo import segments
from ligo.lw import ligolw
from ligo.lw import lsctables
from glue.lal import Cache
from ligo.lw import utils as ligolw_utils
import lal
from lal.series import *
from lal.utils import CacheEntry
import lalsimulation as lalsim
import h5py

import numpy as np
from time import time
import sys

from tqdm import tqdm

def parse_command_line():
	parser = OptionParser(description = __doc__)
	parser.add_option("--tmplt-file", metavar = "filename", help = "Set the tmplt xml file.")
	parser.add_option("--out-file", metavar = "filename", dest = "f_out", help = "Set the output hdf5 filename with extension.")

	options, filenames = parser.parse_args()
	return options, filenames

@lsctables.use_in
class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
	pass

options, filenames = parse_command_line()

xmldoc = ligolw_utils.load_filename(options.tmplt_file, verbose = True, contenthandler = LIGOLWContentHandler)
sngl_inspiral_table = lsctables.SnglInspiralTable.get_table(xmldoc)


print("Generating and saving waveforms...")
t1 = time()

count = 0
n_tmplts = len(sngl_inspiral_table)
with h5py.File(options.f_out, 'w') as f:
	for idx, tmplt in enumerate(tqdm(sngl_inspiral_table)):
		#sys.stdout.write("\r\033[K")
		#sys.stdout.write("\r Generating and saving template waveforms: %.2f%%"%(100.*count/n_tmplts))
		#sys.stdout.flush()
		parameters = {}
		parameters['m1'] = lal.MSUN_SI * tmplt.mass1
		parameters['m2'] = lal.MSUN_SI * tmplt.mass2
		parameters['S1x'] = tmplt.spin1x
		parameters['S1y'] = tmplt.spin1y
		parameters['S1z'] = tmplt.spin1z
		parameters['S2x'] = tmplt.spin2x
		parameters['S2y'] = tmplt.spin2y
		parameters['S2z'] = tmplt.spin2z
		parameters['distance'] = 1.e6 * lal.PC_SI
		parameters['inclination'] = 0.
		parameters['phiRef'] = 0.
		parameters['longAscNodes'] = 0.
		parameters['eccentricity'] = 0.
		parameters['meanPerAno'] = 0.
		parameters['deltaF'] = 0.1
		parameters['f_min'] = 30.0
		parameters['f_max'] = 1024.0
		parameters['f_ref'] = 0.
		parameters['LALparams'] = None
		parameters['approximant'] = lalsim.GetApproximantFromString(str('TaylorT2'))

		hplus, hcross = lalsim.SimInspiralTD(**parameters)

		#save the generated template as a group in the hdf5 structure with idx as name
		grp = f.create_group(str(idx))

		#save the important parameters as group attributes
		del parameters["LALparams"]
		parameters['tau0'] = tmplt.tau0
		parameters['tau3'] = tmplt.tau3
		grp.attrs.update(parameters)

		#The complex numpy arrays from hplus and hcross both get their own dataset
		hp = grp.create_dataset("hplus", data=hplus.data.data)
		hc = grp.create_dataset("hcross", data= hcross.data.data)

		#Set attributes of importance
		
		hp.attrs['f0'] = hplus.f0
		hp.attrs['deltaF'] = hplus.deltaF
		hp.attrs['length'] = hplus.data.length
		
		hc.attrs['f0'] = hcross.f0
		hc.attrs['deltaF'] = hcross.deltaF
		hc.attrs['length'] = hcross.data.length
		
		count += 1
t2 = time()
print("\nGeneration and saving took %.2g s" %(t2-t1))
print("Templates saved in %s"%options.f_out)
