import h5py
import webdataset as wds
from optparse import OptionParser
import math
import numpy as np
import os
from tqdm import tqdm


def parse_command_line():
    parser = OptionParser(description = __doc__)
    parser.add_option("--in-file", type =  "string", dest = "in_file", default = 0, help = "Set the hdf5 file with template waveforms")
    parser.add_option("--out-dir", type =  "string", dest = "out_dir", help = "Set the output directory")
    parser.add_option("--out-name", type = "string", dest = "out_name", help = "Set the output base name")
    parser.add_option("--num-wvfrm", type = "int", dest = "n_wvfrm", default = 1000, help = "Set the amound of waveforms to use")
    parser.add_option("--shard-size", type = "float", dest = "shard_size", default = 1e9, help = "Set the maximum shard size in bytes")
    parser.add_option("--shard-samples", type = "float", dest = "shard_samples", default = 1e3, help = "Set the maximum amount of samples in a shard")
    parser.add_option("--train-frac", type = "float", dest = "train_frac", default = .9, help = "Set the fraction of data to be used for training")
    parser.add_option("--std-frac", type = "float", dest = "std_frac", default = 1, help = "Set the fraction of data to use for std calculation")

    options, filenames = parser.parse_args()
    return options, filenames

options, filnames = parse_command_line()
shard_size = int(options.shard_size)
shard_samples = int(options.shard_samples)


train_dir = os.path.join(options.out_dir, "training")
val_dir = os.path.join(options.out_dir, "validation")

#make sure directory exists
if not os.path.exists(train_dir):
    os.makedirs(train_dir)

if not os.path.exists(val_dir):
    os.makedirs(val_dir)
#TODO clear directory if it is not empty
#load the templates in timedomain
#TODO do zero-padding in here as well
signals = []
stds = []
sizes = []
with h5py.File(options.in_file, 'r') as f:
    keys = list(f.keys())
    #make sure we are picking random waveforms
    indices = np.random.permutation(len(keys))[:options.n_wvfrm]
    #only extract plus polarization for now
    print('extracting waveforms...')
    for idx in tqdm(indices):
        key = keys[idx]
        hplus = f[key]['hplus'][()]
        signals.append(hplus)
        stds.append(np.std(hplus))
        sizes.append(len(hplus))

#longest waveform
size = np.max(sizes)
k_size = 2.
seg_len = int(k_size*size)

                    
#zero-pad the signal
#and duplicate 20 times
#TODO make duplicate a variable
low_idx = int(0.75*seg_len)
high_idx = int(0.95*seg_len)
peak_window = np.arange(low_idx, high_idx)

#For splitting into training and validation
n_train_signal = int(options.train_frac*options.n_wvfrm)
train_signals_padded = []
val_signals_padded = []

print('padding signal...')
for idx, signal in enumerate(tqdm(signals)):
    signal = np.real(signal) #for some reason these are imaginary
    for i in range(20):
        peak_idx = np.argmax(signal)
        signal_len = len(signal)
        place_idx = np.random.choice(peak_window)

        lead_zeros = place_idx - peak_idx
        if lead_zeros < 0:
            #we need to cut part of the signal at the start off
            signal_padded = signal[-lead_zeros:]
        else:
            signal_padded = np.concatenate((np.zeros(lead_zeros), signal))

        trail_zeros = seg_len - signal_len -lead_zeros
        if trail_zeros <0:
            #we need to cut part of the signal at the end off
            signal_padded = signal_padded[:trail_zeros]
        else:
            signal_padded = np.concatenate((signal_padded, np.zeros(trail_zeros)))

        #split into training and validation
        if idx<n_train_signal:
            train_signals_padded.append(signal_padded)
        else:
            val_signals_padded.append(signal_padded)
        
#memory management
del signals


#avg standard deviation
print('finding mean std')
std = np.mean(stds)
#multiplier for standard deviation
k_std = 3.
#TODO base this on optimal SNR instead
gaus_std = k_std*std

#create array of noise sequences
#need 20 noise for every signal
num_noise = 20*options.n_wvfrm
print('generating noise...')
noises = np.random.normal(0, gaus_std, (num_noise, seg_len))

pure_noise = np.random.normal(0, gaus_std, (num_noise, seg_len))

print('Adding noise to padded signal...')
n_train = 20*n_train_signal
train_signal = train_signals_padded + noises[:n_train]
val_signal = val_signals_padded + noises[n_train:]
#memory management
del noises

#create labels
print('Creating classes...')
sgnl_cls = np.ones(num_noise)
noise_cls = np.zeros(num_noise)
#join the arrays
print('Joining noise and signal + noise arrays...')
#TODO this might become to big to keep in memory!
train_data = np.concatenate((train_signal, pure_noise[:n_train]))
val_data = np.concatenate((val_signal, pure_noise[n_train:]))
train_classes = np.concatenate((sgnl_cls[:n_train], noise_cls[:n_train]))
val_classes = np.concatenate((sgnl_cls[n_train:], noise_cls[n_train:]))

#update n_train now that noise has been added
n_train = 2*n_train
print('Shuffling data')
n_val = len(val_classes)
train_perm = np.random.permutation(n_train)
val_perm = np.random.permutation(n_val)

print("Getting ready to write to disk")
train_mag = int(math.log10(n_train))
val_mag = int(math.log10(n_val))
#bound on number of shards
#TODO: bound can be higher if shard size is the limiting factor
#To be on the safe side, using one magnitude higher
max_train_shards = n_train//shard_samples + 1
max_val_shards = n_val//shard_samples + 1
train_shard_mag = int(math.log10(max_train_shards)) + 1
val_shard_mag = int(math.log10(max_val_shards)) + 1
train_shard_samples = []
val_shard_samples = []

train_pattern = os.path.join(options.out_dir, "training",f"{options.out_name}-train-%0{train_shard_mag + 1}d.tar")
with wds.ShardWriter(train_pattern, maxsize = shard_size, maxcount = shard_samples) as sink: 
    for idx in tqdm(train_perm, position = -1, leave=True):
        gwave = train_data[idx]
        cls = int(train_classes[idx])
        
        #key is just the index, we format by order of magnitude of total samples
        key = f"{idx:0{train_mag +1}d}" 

        #for now encode the time-series as a python pickle
        #other supported formats would be pytorch pickle (pth), image (jpg), tensor (ten). See:https://modelzoo.co/model/webdataset
        sample = {"__key__": key, "pyd": gwave, "cls": cls}
        
        #check if data will get written to a new shard
        if(
            sink.count >= sink.maxcount
            or sink.size >= sink.maxsize
        ):
            #current count is the amount of samples in this shard
            train_shard_samples.append(sink.count)

        #write to the tar archive
        sink.write(sample)
        

    #get values for metadata
    train_shards = sink.shard
    #number of samples in final shard
    train_shard_samples.append(sink.count)

    print(f"Total amount of training shards written: {train_shards}")

val_pattern = os.path.join(options.out_dir, "validation", f"{options.out_name}-validate-%0{val_shard_mag + 1}d.tar")
print("writing validation data:")
with wds.ShardWriter(val_pattern, maxsize = options.shard_size, maxcount = options.shard_samples) as sink:
    for idx in tqdm(val_perm, position = -1, leave=True):
        gwave = val_data[idx]
        cls = int(val_classes[idx])
        
        #key is just the index, we format by order of magnitude of total samples
        key = f"{idx:0{val_mag +1}d}" 
        
        #for now encode the time-series as a python pickle
        #other supported formats would be pytorch pickle (pth), image (jpg), tensor (ten). See:https://modelzoo.co/model/webdataset
        sample = {"__key__": key, "pyd": gwave, "cls": cls}
        
        #check if data will get written to a new shard
        if(
            sink.count >= sink.maxcount
            or sink.size >= sink.maxsize
        ):
            #current count is the amount of samples in this shard
            val_shard_samples.append(sink.count)

        #write to the tar archive
        sink.write(sample)

    #metadata
    val_shards = sink.shard
    #number of samples in final shard
    val_shard_samples.append(sink.count)

    print(f"Total amound of validation shards written: {val_shards}")

#write the metadata
n_std = int(options.std_frac*n_train)
print("calculating std...")
#TODO use np.choice
indices = np.random.permutation(n_train)
std = np.std(train_data[indices[:n_std]])
print("done!")

meta_dict = {
    "n_train": n_train,
    "n_val": n_val,
    "train_shard_samples": train_shard_samples,
    "val_shard_samples": val_shard_samples,
    "train_shards": train_shards,
    "val_shards": val_shards,
    "train_mag": train_mag,
    "val_mag": val_mag,
    "train_shard_mag": train_shard_mag,
    "val_shard_mag":val_shard_mag,
    "std": std,
    "name": options.out_name
}

print("writing metadata")
meta_path = os.path.join(options.out_dir, f"{options.out_name}-metadata.tar")

metadata = {"__key__": "metadata", "pyd":meta_dict}
sink = wds.TarWriter(meta_path)
sink.write(metadata)
sink.close()
