import h5py
import webdataset as wds
import math
import numpy as np
import os
from tqdm import tqdm
import collections

import lal
import lalsimulation as lalsim


# needed lal datatypes and rng initialization
# TODO: random seed option
rng = lal.gsl_rng("ranlux", 127)  # wrapper for a gsl rng

def add_waveform(noise, waveform, psd, SNR = 10.):
    '''
    Adds the waveform to the noise, with the peak of the waveform randomly
    between 0.75 and 0.95 of the segment, and scales the waveform to have the required SNR.
    TODO SNR should be calculated with flow corresponding to the frequency at which the
    waveform starts in the segment.
    '''
    seg_len = len(noise)
    low_idx = int(0.75*seg_len)
    high_idx = int(0.95*seg_len)
    peak_window = np.arange(low_idx, high_idx)
    peak_idx = np.argmax(waveform)
    wave_len = len(waveform)
    place_idx = np.random.choice(peak_window)
    

    lead_zeros = place_idx - peak_idx
    if lead_zeros < 0:
        #we need to cut part of the signal at the start off
        data = waveform[-lead_zeros:]
    else:
        data = np.concatenate((np.zeros(lead_zeros), waveform))

    trail_zeros = seg_len - wave_len -lead_zeros
    if trail_zeros <0:
        #we need to cut part of the signal at the end off
        data = data[:trail_zeros]
    else:
        data = np.concatenate((data, np.zeros(trail_zeros)))
    
    #TODO check if this is done properly by only using the data within the segment
    epoch = lal.LIGOTimeGPS(0) 
    data_lal = lal.CreateREAL8TimeSeries("STRAIN", epoch, 0, 1./8192., lal.StrainUnit, seg_len)
    data_lal.data.data[:] = data

    SNR_curr = lalsim.MeasureSNR(data_lal, psd, 10, -1)
    #scale waveform to get required SNR
    data *= SNR/SNR_curr

    return noise + data


def gen_psd(psd_name, flow, sample_rate, segment_length):
    '''
    Generates the psd using LALsimulation, based on the psd name, and returns
    it as a LAL frequency series.
    TODO add link to named psd functions
    '''

    df = sample_rate/segment_length
    epoch = lal.LIGOTimeGPS(0)  # gps time in seconds, nanoseconds

    # starting frequency is set to a very small number due to bug in LALSimNoisePSD.c
    psd = lal.CreateREAL8FrequencySeries('', epoch, 1e-25, df, lal.SecondUnit,
                                         segment_length//2 + 1)

    _name_prefix = 'SimNoisePSD'
    # Adding 'Ptr' to the end of a lalsuite function returns it as a SWIG
    # wrapped pointer to the C function, as opposed to returning a python
    callable
    _name_suffix = 'Ptr'

    psd_func = lalsim.__dict__[_name_prefix + psd_name + _name_suffix]
    lalsim.SimNoisePSD(psd, flow, psd_func)

    return psd


def gen_noise(psd, sample_rate, segment_length):
    '''
    Generates noise based on psd and returns it as array. Make sure to call
    this function after having
    defined the gsl rng!
    '''
    epoch = lal.LIGOTimeGPS(0)
    seg = lal.CreateREAL8TimeSeries("STRAIN", epoch, 0, 1./sample_rate,
                                    lal.StrainUnit, segment_length)

    lalsim.SimNoise(seg, seg.data.length, psd, rng)

    return seg.data.data


def make_metadata():
    # TODO: implement
    pass


def make_dataset(psd, srate, seg_len, snr, pattern, max_size, max_count,
                 hdf5_file, indices, classes, magnitude):
    '''
    Writes a dataset of injection GWs to disk as shards, using GWs from an
    opened hdf5 file.
    '''

    # indices has to be an iterator
    if not isinstance(indices, collections.Iterator):
        indices = iter(indices)

    keys = list(hdf5_file.keys())
    # bookkeeping
    shard_samples = []

    with wds.ShardWriter(pattern,
                         maxsize=max_size,
                         maxcount=max_count) as sink:
        idx = 0
        for cls in tqdm(classes, position=-1, leave=True):
            # generate noise
            # TODO uses globals psd, srate, seg_len
            noise = gen_noise(psd, srate, seg_len)
            if cls == 0:
                # just noise
                data = noise
            else:
                # add gravitational wave to noise
                key = keys[next(indices)]
                waveform = hdf5_file[key]['data'][()]
                # TODO: uses globals psd, options.snr
                data = add_waveform(noise, waveform, psd, snr)

            # key is just the index, we format by order of magnitude of total
            # samples
            key = f"{idx:0{magnitude +1}d}"

            # for now encode the time-series as a python pickle
            # other supported formats would be pytorch pickle (pth),
            # image (jpg), tensor (ten).
            # See:https://modelzoo.co/model/webdataset
            sample = {"__key__": key, "pyd": data, "cls": cls}

            # check if data will get written to a new shard
            if(
                sink.count >= sink.maxcount
                or sink.size >= sink.maxsize
            ):
                # current count is the amount of samples in this shard
                shard_samples.append(sink.count)

            # write to the tar archive
            sink.write(sample)
            # next index
            idx += 1

        # number of samples in final shard
        shard_samples.append(sink.count)

        print(f"Total amount of training shards written: {sink.shard}")
        # returns values for metadata
        return shard_samples, sink.shard

# TODO add waveform generation?
