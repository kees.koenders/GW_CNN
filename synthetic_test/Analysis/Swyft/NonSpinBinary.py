import numpy as np
from numpy import pi
from scipy.constants import c
from scipy.constants import G
from scipy.constants import parsec

mpc = parsec*10**6  # megaparsec in meters
m_sol = 1.9891*10**30


def Chirp_mass(m1, m2):
    m1, m2 = m_sol*m1, m_sol*m2
    return ((m1*m2)**(3/5))/((m1+m2)**(1/5))


def T_coal(m1, m2, f=40):
    """
    m1, m2 in solar masses.
    """
    Mc = Chirp_mass(m1, m2)
    return (5/256)*((pi*f)**(-8/3)*(G*Mc/(c**3))**(-5/3))


def Phi(tau, m1, m2):
    """
    m1, m2 in solar masses.
    """
    return -2*(((5*G*Chirp_mass(m1, m2))/(c**3))**(-5/8))*tau**(5/8)


def h_p(t, m1, m2, r=1):
    """
    t in seconds
    m1, m2 in solar masses
    r in Mpc
    """
    r = r*mpc
    tau = T_coal(m1, m2) - t
    Mc = Chirp_mass(m1, m2)
    return (1/r)*(G*Mc/(c**2))**(5/4)*(5/(c*tau))**(1/4)*np.cos(Phi(tau,
                                                                    m1,
                                                                    m2))


def h_c(t, m1, m2, r=1):
    """
    t in seconds
    m1, m2 in solar masses
    r in Mpc
    """
    r = r*mpc
    tau = T_coal(m1, m2) - t
    Mc = Chirp_mass(m1, m2)
    return (1/r)*(G*Mc/(c**2))**(5/4)*(5/(c*tau))**(1/4)*np.sin(Phi(tau,
                                                                    m1,
                                                                    m2))
