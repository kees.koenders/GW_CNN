import numpy as np
import swyft
import torch.nn as nn
import torch.nn.functional as F
import torch.fft

import multiprocessing


class MockWave():
    """
    Sine, exponentially increasing in both amplitude and frequency.
    Implemented as callable class for use with Swyft.
    Also has noise function that can be supplied as sim hook.
    """
    def __init__(self, npoints, sigma):
        self.npoints = npoints
        self.sigma = sigma
        self.npars = 2
        self.par_names = ["$\\alpha$", "$\\beta$"]
        self.sim_shapes = {"ys": (npoints,)}

    def __call__(self, pars):
        alpha, beta = pars
        # alpha, beta are in [0,1]
        # scaling
        alpha = 0.1 + (alpha)*2/5

        # add some Gaussian noise to beta
        beta = np.random.normal(beta, 0.05)
        # make sure it's in domain
        eps = 1e-3
        beta = min(max(beta, eps), 1)
        # scale to final domain
        beta = beta/2
        xs = np.linspace(0, 5, self.npoints)
        ys = np.exp(1+alpha*xs)*np.sin(np.exp(3.5+beta*xs)-1)

        return dict(ys=ys)

    def noise(self, sim, v=None):
        ys = sim['ys']
        ys = ys + self.sigma*np.random.randn(len(ys))
        return dict(ys=ys)

    def simulator(self):
        """
        Returns swyft Simulator for this model.
        """
        return swyft.Simulator(self,
                               self.par_names,
                               self.sim_shapes)

    def prior(self):
        """
        Returns swyft prior for this model.
        """
        return swyft.Prior(lambda u: u, self.npars)


class simpleHead(swyft.Module):
    """
    Head network do extract sine-features.
    """
    def __init__(self, sim_shapes):
        super().__init__(sim_shapes=sim_shapes)

        segment_length = sim_shapes["ys"][0]
        self.onl_f = (swyft
                      .OnlineNormalizationLayer(
                          torch.
                          Size([segment_length])))
        self.conv1 = nn.Conv1d(1, 32, kernel_size=16, stride=1)
        self.conv2 = nn.Conv1d(32, 8, kernel_size=8, stride=4)
        self.conv3 = nn.Conv1d(8, 16, kernel_size=16, stride=8)

        # this allows (almost) any size for the input data,
        # but I'm also too lazy to calculate
        # calculation *is* possible  and would be slightly more efficient than
        # doing a partial forward pass at initialisation

        # sim shapes is used in swyft to give you the shape of the simulator
        # output, in our case a 1d segment
        # swyft needs this to construct the tail network
        self.n_features = self.num_features(self.
                                            conv(torch.randn(1, 1,
                                                             segment_length)))

    def num_features(self, x):
        # calculates the number of features when the tensor is flattened
        size = x.size()[1:]  # ignore batch dimension
        count = 1
        for s in size:
            count *= s
            return count

    def conv(self, x):
        x = self.conv1(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 8)

        x = self.conv2(x)
        x = F.elu(x)

        x = self.conv3(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 2)

        return x

    def forward(self, obs):
        x = obs["ys"]
        x = self.onl_f(x)

        # insert channel dimension
        x = torch.unsqueeze(x, 1)

        x = self.conv(x)

        # flatten into feature vector
        x = x.view(-1, self.n_features)

        return x


class SwyftTrainer():

    def __init__(self, head, dataset, marginals,
                 epochs=1, tail=None):
        self.head = head
        self.dataset = dataset
        self.marginals = marginals
        self.epochs = epochs
        self.tail = tail

    def doTraining(self, hyper_pars, device='cpu'):
        """
        Trains for epochs with swyft.
        device as option so starmap can work multi-gpu
        Returns posterior object.
        """

        # create posterior  object for this function instance
        post = swyft.Posteriors(self.dataset)
        post.add(self.marginals, device=device, head=self.head)

        optimizer_args = {key: hyper_pars[key]
                          for key in ["lr"]}
        scheduler_args = {key: hyper_pars[key]
                          for key in ["factor", "patience"]}

        post.train(self.marginals, max_epochs=self.epochs,
                   early_stopping_patience=hyper_pars
                   ["max_fruitless_epoch"],
                   optimizer_args=optimizer_args,
                   scheduler_args=scheduler_args)

        return post

    def performanceMetrics(posterior):
        """
        Returns performance metrics in a dict based on validation and training
        loss from the given posterior.
        """
        diagnostics = posterior.train_diagnostics([0, 1, (0, 1)])[0]
        train_loss = np.array(diagnostics["train_loss"])
        valid_loss = np.array(diagnostics["valid_loss"])

        # metrics of interest
        best_loss = valid_loss[-1]
        loss_mse = ((train_loss - valid_loss)**2).mean()

        return dict(best_loss=best_loss, loss_mse=loss_mse), diagnostics


"""
Multiprocessing with swyft.

TODO: change to torch.multiprocessing?
"""


class NoDaemonProcess(multiprocessing.Process):
    # make 'daemon' attribute always return False
    def _get_daemon(self):
        return False

    def _set_daemon(self, value):
        pass

    daemon = property(_get_daemon, _set_daemon)


class MyPool(multiprocessing.pool.Pool):
    """
    We sub-class multiprocessing.pool.Pool instead of multiprocessing.Pool,
    because the latter is only a wrapper function, not a proper class.
    """
    Process = NoDaemonProcess


"""
Below is all legacy code that exists to preserve some ideas for more
geometry-based geneteic operations.
"""

'''
def mutation(param, p=.5):
    """
    Returns a mutation given the parameter.
    p controls the size of expected deviation compared to the size of the
    parameter range.
    """

    lo, hi, type_str = hyper_range[param]
    size = hi-lo

    if type_str == "int":
        return rng.binomial((size//2)*2, p) - size//2
    elif type_str == "double":
        return rng.normal(scale=p*size/4)
    else:
        print(f"the given type {type_str} is not supported!",
              flush=True)
        raise ValueError


def mutate(param, value, p=.01):
    """
    Performs a mutation on value. This mutation will be akin to a bit flip, but
    scaled to the parameter range. This means the change will be linear in
    log.
    """

    lo, hi, type_str = hyper_range[param]
    size = hi-lo

    """
    Should be changed to a true bit-flip. So:
        -represent the numbers as bytecode
        -flip each bit with same probability (rng.binomial(1,p,n_bits))
        -return, rejection sampling should be done externally
        """

    # trial with odds p
    if rng.binomial(1, p) == 1:
        if type_str == "int":
            # TODO bit flip-ish
            return value + rng.binomial((size//2)*2, p) - size//2
        elif type_str == "double":
            # TODO change bytecode
            expo = rng.uniform(np.log(lo), np.log(hi))
            sign = rng.integers(2)*2-1
            return value + sign*np.exp(expo)
        else:
            print(f"the given type {type_str} is not supported!",
                  flush=True)
            raise ValueError
    else:
        # no mutation
        return value


def mate(parents, n_children, rng=np.random.default_rng()):
    """
    Takes in a tuple of N parents and returns n_children agents.
    """

    N = len(parents)     # number of vertices
    D = len(parents[0])  # parametric dimension

    # for now: only allow sampling on specific points of a hypercuboid defined
    # by a simplex.
    assert N <= D+1, ("Too many parents to embed the"
                      "simplex they define")

    """
    The possible points on the hypercuboid are:
        - all it's vertices
        - all midpoints of vertices
        - the centre of mass

    TODO: A review on the combinatorics.
    TODO: Support larger set of parents
    TODO: Different sampling (Fréchet mean?)
    """

    # seperate possible vertex values per dimension
    vertex_options = {param: [parent[param] for parent in parents]
                      for param in parents[0].keys()}

    # randomly pick vertices for children
    # this is done by a series of permutations of parent indexes, which
    # guarantees the most equall participation of each parent possible
    # Using map because of old version of numpy, better to use permute
    assignment = map(rng.permutation, np.tile(np.arange(N), (D//N + 1, 1)))
    assignment = np.array(list(assignment)).flatten()[:D]

    children = []
    for child_idx in range(n_children):
        child = {}

        # loop over D parent indexes and parameters
        for (parent_idx,
             (param, options)) in zip(assignment,
                                      vertex_options.items()):
            value = options[parent_idx]
            lower_bound, upper_bound, _ = hyper_range[param]

            new_value = mutate(param, value, p=0.1)
            while new_value < lower_bound or new_value > upper_bound:
                # retry until in range
                new_value = mutate(param, value, p=1.)

            child[param] = new_value
            children.append(child)

    return children


def nextGenAlt(pop, pop_perf, n_parents=4, min_mates=2, carry_over=2):
    """
    Implementation using intuition on the parameter space
    """

    perf_keys = pop_perf[0].keys()
    pop_ranks = {}

    n_agents = len(pop)
    assert n_agents >= n_parents, "too many parents requested for population"

    for perf_key in perf_keys:
        # sort population wrt perf_key
        # for all ranking statistics we have lower to be better
        sorted_pop = [agent for _, agent
                      in sorted(zip(pop_perf, pop),
                                key=lambda pair: pair[0][perf_key])]

        pop_ranks[perf_key] = sorted_pop

    # select survivors
    survivors = []
    for perf_key in perf_keys:
        # pick top carry_over to survive
        for pick_idx in range(carry_over):
            survivor = pop_ranks[perf_key][pick_idx]
            # TODO mutate
            survivors.append(survivor)

    n_survivors = len(survivors)

    # select parents from top ranking agents
    """
    For now just select equally from rankings.
    TODO: include biased selection.
    TODO: include growth factor
    TODO: indlude biased children alocation
    """

    n_ranks = len(pop_ranks)
    if n_parents < n_ranks:
        raise NotImplementedError
    if n_parents*n_ranks > n_agents:
        raise ValueError("Too many parents to keep population stable!")

    parents_per_rank = n_parents//n_ranks
    remaining_parents = n_parents % n_ranks

    parents = []

    for perf_key in perf_keys:
        # take the top performing agents in each ranking
        for pick_idx in range(parents_per_rank):
            parents.append(pop_ranks[perf_key][pick_idx])

    if remaining_parents > 0:
        # distribute remaining parents over ranks
        # TODO: use generator
        perf_keys_rem = np.random.choice(list(perf_keys), remaining_parents,
                                         replace=False)

        for perf_key in perf_keys_rem:
            parents.append(pop_ranks[perf_key][parents_per_rank])

    np.random.shuffle(parents)
    n_mates = n_parents//min_mates
    simplices = np.array_split(parents, n_mates)

    children = []
    n_agents_left = n_agents - n_survivors
    children_per_mates = n_agents_left//n_mates
    remaining_children = n_agents_left % n_mates

    n_children_mates = np.ones(n_mates, dtype=int)*children_per_mates
    if remaining_children > 0:
        add_child_idxs = np.random.choice(n_mates, remaining_children,
                                          replace=False)
        n_children_mates[add_child_idxs] += 1

    for mates, n_children in zip(simplices, n_children_mates):
        children += mate(mates, n_children)

    return children + survivors
'''
