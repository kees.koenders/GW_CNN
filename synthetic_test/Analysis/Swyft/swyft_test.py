import os
visible_devs = "0,1,2"
os.environ["CUDA_VISIBLE_DEVICES"] = visible_devs
import matplotlib
matplotlib.use('Agg')
from Simulation import SimulateWaveform
from Gabbard_Net import HeadNet, SimpleHead
import swyft
import torch
from scipy import stats
import numpy as np
import torch

def map_wave_pars(pars, m_min=5., m_max=95.):
    """
    gives a mapping from Unif(0,1) to the wave parameters
    Mass given in solar masses

    This needs to be monotonically increasing!
    """
    # these are not the actual parameters, but their hypercube counterpart
    m1_hc, m2_hc= pars
    # TODO: revert to using all pars
    incl_hc, phase_hc, ra_hc, decl_hc, pol_hc = 0, 0, 0, 0, 0
    # Masses are drawn according to $m_{1,2} \sim \log m_{1,2}
    # masses uniform in log mass, this uses the natural log. That's no problem
    # as any base would work
    # https://en.wikipedia.org/wiki/Reciprocal_distribution
    m_dist = stats.loguniform(m_min, m_max)

    m1, m2 = m_dist.ppf([m1_hc, m2_hc])

    # phase drawn uniformly from $[0,2\pi]$
    two_pi_flat = stats.uniform(loc=0., scale=2*np.pi)

    phase = two_pi_flat.ppf(phase_hc)

    # Inclination is drawn from a distrbution uniform over its cosine.
    # It's easier to draw the uniform, since the transformed
    # distribution has a singularity at angle 0 maybe there is a way to do this
    # with distribution from scipy? Could make my own if needed to speed up
    cos_incl = stats.uniform(loc=-1., scale=2.).ppf(incl_hc)
    # TODO: fix this and give a proper mapping from cdf to incl.
    incl = -np.arccos(cos_incl)

    # Right ascension and declination come from uniform isotropic prior
    ra = two_pi_flat.ppf(ra_hc)
    decl = stats.uniform(loc=-0.5*np.pi, scale=np.pi).ppf(decl_hc)

    # Polarization angle drawn uniformly
    pol = two_pi_flat.ppf(pol_hc)

    return m1, m2, incl, phase, ra, decl, pol


class SimpleSim(SimulateWaveform):
    """
    Wrapper that simulates only based on mass.
    """

    def __init__(self, seg_len, approx, detector, snr=2, nuis_pars=(0,0,0,0,0)):
        self.nuis_pars = nuis_pars
        super().__init__(seg_len, approx, detector, snr = snr)
    
    def __call__(self, masses):
        pars = tuple(masses) + self.nuis_pars
        return super().__call__(pars)


print("starting script", flush=True)

# TODO select device
DEVICE = torch.device('cuda:1')
max_fruitless_epoch = 250
factor = 0.70
patience = 30
EPOCHS = 500

seg_len = 8192*2
nwaves = 58
lr = 5e-2
m_min, m_max = 90., 96.
# initialize simulator
print("initializing simulator", flush=True)
simulator = SimpleSim(seg_len, "IMRPhenomD", "H1", snr=10.)
npars = 2  # simulator.npars
par_names = ["m1", "m2"]  # , "incl", "phase", "ra", "decl", "pol"]
sim = swyft.Simulator(simulator, par_names, sim_shapes=dict(data=(seg_len,)))

# Define hypercube prior
print("Defining hypercube prior", flush=True)
# flat prior for masses
prior = swyft.Prior(lambda pars_hc: (m_max - m_min)*pars_hc + m_min, npars)
# prior = swyft.Prior(lambda pars_hc: 
#                     np.array(map_wave_pars(pars_hc, 
#                                            m_min=m_min, m_max=m_max))[:2],
#                     npars)

# memory storage
print("Setting up memory store", flush=True)
store = swyft.MemoryStore(sim)
store.add(nwaves, prior)
store.simulate()

# dataset
print("Making dataset", flush=True)
dataset = swyft.Dataset(nwaves, prior, store)
post = swyft.Posteriors(dataset)

# Setting up the ML framework
print("Setting up ML", flush=True)

# TODO load in pre-trained values from state_dict
model_path = "Models/model_astro_snr2_e300_b400_lr7e-6.pt"
state_dict = torch.load(model_path)
# head_kwargs = dict(spectral=True)
head_kwargs = dict(state_dict=state_dict)
# Parameters are [m1, m2, incl, phase, ra, decl, pol]
marginals = [(0, 1)]  # parameters of interest, including pairs of parameters
# the head network has to given as class, so not yet initialized.
post.add(marginals, device=DEVICE, head=HeadNet)  # , head_args=head_kwargs)

# Train!
print("Training!", flush=True)
post.train(marginals, max_epochs=EPOCHS,
           early_stopping_patience=max_fruitless_epoch,
           optimizer_args=dict(lr=lr),
           scheduler_args=dict(factor=factor, patience=patience))

# sample from uniform hypercube
print("Drawing new sample as observation", flush=True)
hc_draw = stats.uniform.rvs(size=npars)
# map to parameters
# TODO could be faster to directly draw the wave parameters. Already have the
# code in generate_waveform_astro.py
# TODO change back to all pars
obs_p = np.array(map_wave_pars(hc_draw, m_min=m_min, m_max=m_max))[:2]
# simulate and observation
obs = simulator(obs_p)

# plot
print("Sampling from posterior", flush=True)
samples = post.sample(10000, obs)
fig, diags = swyft.plot_corner(samples, [0,1], truth=obs_p,
                           figsize=(10, 10), bins=100)

# save to disk
plot_path = os.path.join("..", "Plots", "posterior_test.svg")
fig.savefig(plot_path)

print(f"Done! Posterior plot saved at {plot_path}")
