import os
import swyft
import pickle
import numpy as np
import SimpleSwyftUtils as utils
from swyft.utils import plot_posterior
import matplotlib.pyplot as plt
from matplotlib import gridspec as gs


def doRound(store, prior, pars, device, nwaves, bound, obs,
            simhook=None, final=False):
    """
    Does a round of the nested posterior estimation. Effectively "zooming in"
    on the parameter space by Truncation.
    """
    # Update store to truncated prior
    store.add(nwaves, prior, bound=bound)
    # Simulate needed samples
    store.simulate()

    # The usual
    dataset = swyft.Dataset(nwaves, prior, store, bound=bound, simhook=simhook)

    # Only add the 2-dim marginal in last round, is not needed for truncation
    if final:
        marginals = [0, 1, (0, 1)]
    else:
        marginals = [0, 1]

    trainer = utils.SwyftTrainer(utils.simpleHead,
                                 dataset,
                                 marginals,
                                 epochs=30)
    post = trainer.doTraining(pars, device)

    # Truncate
    new_bound = post.truncate(marginals, obs)
    return post, new_bound


def centralConfidence(v, zm, conf=0.68):
    """
    Returns the indices that define the central confidence
    """
    v = v.flatten()
    v_arg = np.argsort(v)[::-1]  # Sort backwards
    total_mass = v.sum()
    enclosed_mass = np.cumsum(v[v_arg]) # Sum large to small
    select_conf = enclosed_mass >= total_mass * conf
    # translate to original indexes
    return v_arg[select_conf]


def plotProbableSignals(post_samples, n_plot, alpha_base=0.05, conf=.68, ax=None):
    """
    Uses posterior samples to plot examples within confidence, returns last
    drawn line.
    """

    x_alpha = post_samples['v'][:, 0]
    x_beta = post_samples['v'][:, 1]

    xs = np.linspace(0, np.pi, npoints)

    # determine confidence interval 
    # TODO: shoould do this jointly...
    v_a, e_a = np.histogram(x_alpha, bins=100, density=True)
    # central values
    zm_a = (e_a[1:] + e_a[:-1])/2
    conf_idx_a = centralConfidence(v_a, zm_a)
    # to be sure sort indices
    conf_idx_a.sort()
    # find when idx changes with more than 1
    # edges = (conf_idx_a[1:] - conf_idx_a[:1]) > 1
    conf_int_a = (zm_a[conf_idx_a[0]], zm_a[conf_idx_a[-1]])

    v_b, e_b = np.histogram(x_beta, density=True)
    zm_b = (e_b[1:] + e_b[:-1])/2
    conf_idx_b = centralConfidence(v_b, zm_b)
    conf_idx_b.sort()
    conf_int_b = (zm_b[conf_idx_b[0]], zm_b[conf_idx_b[-1]])

    select_conf = (conf_int_a[0] <= x_alpha) & (x_alpha <= conf_int_a[1]) &(conf_int_b[0] <= x_beta) & (x_beta <= conf_int_b[1])
    alpha_conf = x_alpha[select_conf]
    beta_conf = x_beta[select_conf]

    # plot the draws with high opacity
    if ax is None:
        fig = plt.figure(figsize=(5, 10))
        ax = fig.add_subplot(111)

    n_conf = len(alpha_conf)
    plot_idx = np.random.permutation(n_conf)
    if n_plot < n_conf:
        plot_idx = plot_idx[:n_plot]

    for idx in plot_idx:
        alpha = alpha_conf[idx]
        beta = beta_conf[idx]

        # TODO make this user providable
        signal = mockwave((alpha, beta))["ys"]
        guess = ax.plot(xs, signal, linewidth=0.5, color='tab:orange',
                        alpha=alpha_base + 1/n_plot)

    return guess


if __name__ == "__main__":
    # load in saved posteriors
    trained = "genetic-tested.pkl"
    plot_name = "genetic_posterior.svg"
    with open(trained, 'rb') as f:
        results = pickle.load(f)

    posteriors = results['posts']
    marginals = [0, 1, [0, 1]]
    best_loss = float('inf')
    best_post_idx = None
    # find best posterior
    for idx, post in enumerate(posteriors):
        loss = post.train_diagnostics(marginals)[0]['valid_loss'][-1]
        if loss < best_loss:
            best_loss = loss
            best_post_idx = idx

    post = posteriors[best_post_idx]
    pars = results['pop'][best_post_idx]

    # generate observation
    npoints = 1024
    mockwave = utils.MockWave(npoints, 10.)
    sim = swyft.Simulator(mockwave,
                          mockwave.par_names,
                          mockwave.sim_shapes)
    prior = mockwave.prior()
    store = swyft.MemoryStore(sim)

    npars = len(mockwave.par_names)
    u = np.random.uniform(size=(1, npars))
    v = prior.v(u).flatten()
    sgn = mockwave(v)
    obs = mockwave.noise(sgn)

    ROUNDS = 5
    MIN_VOL = 1e-1
    nwaves = 5000
    device = 'cuda:0'
    bound = post.truncate([0, 1], obs)
    for i in range(ROUNDS):
        post, bound = doRound(store, prior, pars, device, nwaves, bound, obs,
                              simhook=mockwave.noise, final=(i == (ROUNDS-1)))
        if bound.volume < MIN_VOL:
            post, bound = doRound(store, prior, pars, device, nwaves, bound,
                                  obs, simhook=mockwave.noise, final=True)
            break
    post.save("truncated_post.pt")

    # Plot
    par_names = mockwave.par_names
    resolution = 1e6
    post_samples = post.sample(int(resolution), obs)
    nrows = 2
    ncols = 4
    height = 4
    width = 4
    spec = gs.GridSpec(nrows=nrows, ncols=ncols)
    fig = plt.figure(figsize=(height*ncols, width*nrows))
    fig.suptitle(r'SWYFT with model $\exp\left(1+\alpha x\right)$'
                 r'$\sin\left(\exp\left(3.5+\beta x\right)-1\right)$')

    marg1d = [0, 1]
    for idx, pois in enumerate(marg1d):
        # For now okay since we only have 2 maringals
        curr_ax = fig.add_subplot(spec[0, idx])
        plot_posterior(post_samples,
                       (pois,),
                       ax=curr_ax,
                       grid_interpolate=False,
                       color="k",
                       bins=100,
                       contours=True)
        curr_ax.set_xlabel(par_names[pois])
        curr_ax.set_xlim(0, 1)

        # true value
        print(f"True value for {par_names[pois]}: {v[pois]:.3f}")
        curr_ax.axvline(v[pois], ls=":", color="r")

    # 2d contour
    curr_ax = fig.add_subplot(spec[:, 2:])
    plot_posterior(post_samples,
                   (0, 1),
                   ax=curr_ax,
                   grid_interpolate=False,
                   color="k",
                   bins=100,
                   contours=True)

    curr_ax.set_xlim(0, 1)
    curr_ax.set_ylim(0, 1)

    curr_ax.set_xlabel(par_names[0])
    curr_ax.axvline(v[0], ls=":", color='r')

    curr_ax.set_ylabel(par_names[1])
    curr_ax.axhline(v[1], ls=":", color='r')

    # signal
    ax = fig.add_subplot(spec[1, 0:2])
    xs = np.linspace(0, np.pi, npoints)
    ax.plot(xs, obs["ys"], label="Observation", linewidth=0.75)
    guess = plotProbableSignals(post_samples, 20, conf=38.2,
                                alpha_base=0.2, ax=ax)
    ax.plot(xs, sgn["ys"], label="Signal", color="tab:red", linestyle='--',
            linewidth=0.75)
    ax.legend()

    plot_path = os.path.join("..", "..", "Plots", "Swyft", "Mockwave",
                             plot_name)
    fig.savefig(plot_path)

    print(f"Posterior plot saved at {plot_path}")
