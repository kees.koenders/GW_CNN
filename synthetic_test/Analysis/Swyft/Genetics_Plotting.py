import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec as gs
import pickle
import os

if __name__ == "__main__":
    # only runs when script is run as main, so not during imports
    f = open("genetic_results.pkl", 'rb')
    results = pickle.load(f)
    pop_hist = results["pop_hist"]
    c = []
    points = {}
    avg_points = {}
    top_x = 5
    for gen, pop_perf in enumerate(pop_hist):
        n_agents = len(pop_perf)
        # color array
        c += [gen for _ in range(len(pop_perf))]

        for agent_perf in pop_perf:
            for key, value in agent_perf.items():
                if key not in points:
                    # TODO more efficient to initialize
                    points[key] = [value]
                    avg_points[key] = [value/n_agents]
                    continue
                points[key] += [value]
                if len(avg_points[key]) <= gen:
                    # expand list
                    avg_points[key] += [value/n_agents]
                    continue
                avg_points[key][gen] += value/n_agents

    # scale c from 1 to 100
    max_c = c[-1]
    scale_c = 100/max_c
    scaled_c = [val*scale_c for val in c]

    fig = plt.figure(figsize=[10, 10])

    if len(points) == 2:
        # we can do scatterplot
        keys = list(points.keys())
        ax = fig.add_subplot(111)
        plot = ax.scatter(points[keys[0]], points[keys[1]],
                          c=c, cmap="viridis", alpha=.25,
                          linewidths=0.0, marker="h")
        cbar = plt.colorbar(plot)
        cbar.set_label("generation")
        ax.set_xlabel(keys[0])
        ax.set_ylabel(keys[1])

    plot_path = os.path.join("..", "..", "Plots", "Swyft", "Mockwave",
                             "pop_history_mockwave.svg")
    fig.savefig(plot_path)
    f.close()



