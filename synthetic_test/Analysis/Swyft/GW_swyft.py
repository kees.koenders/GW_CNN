import os
visible_devs = "0,1,2,3"
os.environ["CUDA_VISIBLE_DEVICES"] = visible_devs
# add parent directory as searchable for modules 
import sys
sys.path.insert(0,'..')
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec as gs
import numpy as np
from numpy.random import default_rng
import swyft
from swyft.utils import plot_posterior
from swyft.networks.linear import LinearWithChannel
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.fft
import pickle
import multiprocessing
import multiprocessing.pool
from multiprocessing import Pool
from copy import deepcopy
from Genetics import Genetics
from Simulation import SimulateWaveform
import time
from NonSpinBinary import h_p, T_coal

class NoDaemonProcess(multiprocessing.Process):
    # make 'daemon' attribute always return False
    def _get_daemon(self):
        return False

    def _set_daemon(self, value):
        pass

    daemon = property(_get_daemon, _set_daemon)


# We sub-class multiprocessing.pool.Pool
# instead of multiprocessing.Pool
# because the latter is only a wrapper
# function, not a proper class.
class MyPool(multiprocessing.pool.Pool):
    Process = NoDaemonProcess


def simpleGW(pars):
    """
    Returns the plus polarization time-domain GW of a non spinning binary up to
    coalescence time. No ringdown. Places coalescence randomly.
    """
    m1, m2 = pars
    # how much time we have from f_gw = 40 Hz
    t_coal = T_coal(m1, m2, 40)
    dt = 1./srate
    # calculate points at sampling rate
    points_tot = int(t_coal*srate)
    # select npoints segment
    if(points_tot <= npoints):
        raise Exception("Wave too short!")
    # start_point = rng.choice(points_tot - npoints)
    t_start = t_coal - npoints*dt
    # t_start = start_point*dt
    ts = np.arange(npoints)*dt + t_start
    gwave = h_p(ts, m1, m2)

    return dict(data=gwave)


def gravWave(pars):

    (m_ratio,) = pars
    m1 = 40.
    m2 = m_ratio*m1
    # other pars
    ra, decl = (0., 0.)
    incl, phase, pol = (0., 0., 0.)

    # returns a dict(data=...)
    signal = SIM((m1, m2, incl, phase, ra, decl, pol))

    return signal

def noise(sim, v=None):
    ys = sim['ys']
    ys = ys + SIGMA*np.random.randn(len(ys))
    return dict(ys=ys)


class mockHead(swyft.Module):
    """
    Head network do extract sine-features.
    """
    def __init__(self, sim_shapes):
        super().__init__(sim_shapes=sim_shapes)

        segment_length = sim_shapes["data"][0]
        self.onl_f = swyft.OnlineNormalizationLayer(torch.Size([segment_length]))
        self.conv1 = nn.Conv1d(1, 32, kernel_size=16, stride=1)
        self.conv2 = nn.Conv1d(32, 8, kernel_size=8, stride=4)
        self.conv3 = nn.Conv1d(8, 16, kernel_size=16, stride=8)

        # this allows (almost) any size for the input data,
        # but I'm also too lazy to calculate
        # calculation *is* possible  and would be slightly more efficient than
        # doing a partial forward pass at initialisation

        # sim shapes is used in swyft to give you the shape of the simulator
        # output, in our case a 1d segment
        # swyft needs this to construct the tail network
        self.n_features = self.num_features(self.
                                            conv(torch.randn(1, 1,
                                                             segment_length)))

    def num_features(self, x):
        # calculates the number of features when the tensor is flattened
        size = x.size()[1:]  # ignore batch dimension
        count = 1
        for s in size:
            count *= s
        return count

    def conv(self, x):
        x = self.conv1(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 8)

        x = self.conv2(x)
        x = F.elu(x)

        x = self.conv3(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 2)

        return x

    def forward(self, obs):
        x = obs["data"]
        x = self.onl_f(x)

        # insert channel dimension
        x = torch.unsqueeze(x, 1)

        x = self.conv(x)

        # flatten into feature vector
        x = x.view(-1, self.n_features)

        return x


class theHead(swyft.Module):
    def __init__(self, sim_shapes):

        super().__init__(sim_shapes=sim_shapes)

        segment_length = sim_shapes["data"][0]
        self.onl_f = swyft.OnlineNormalizationLayer(torch.Size([segment_length]))

        n_channels = 1
        groups = 1

        # compression layer 4096 -> 2047
        self.conv1 = nn.Conv1d(n_channels, groups*128, groups=groups,
                               kernel_size=3, dilation=1, stride=2)
        # "two-point" correlations, L -> L-dilation
        self.conv2 = nn.Conv1d(groups*128, groups*128, groups=groups,
                               kernel_size=2, dilation=2, stride=1)
        self.conv3 = nn.Conv1d(groups*128, groups*128, groups=groups,
                               kernel_size=2, dilation=4, stride=1)
        self.conv4 = nn.Conv1d(groups*128, groups*128, groups=groups,
                               kernel_size=2, dilation=8, stride=1)
        self.conv5 = nn.Conv1d(groups*128, groups*128, groups=groups,
                               kernel_size=2, dilation=16, stride=1)
        self.conv6 = nn.Conv1d(groups*128, groups*128, groups=groups,
                               kernel_size=2, dilation=32, stride=1)
        self.conv7 = nn.Conv1d(groups*128, groups*128, groups=groups,
                               kernel_size=2, dilation=64, stride=1)
        self.conv8 = nn.Conv1d(groups*128, groups*128, groups=groups,
                               kernel_size=2, dilation=128, stride=1)
        self.conv9 = nn.Conv1d(groups*128, groups*128, groups=groups,
                               kernel_size=2, dilation=256, stride=1)
        self.conv10 = nn.Conv1d(groups*128, groups*128, groups=groups,
                                kernel_size=2, dilation=512, stride=1)
        # this throws out 1022 features, if this works there is a lot of
        # inefficiency
        self.conv11 = nn.Conv1d(groups*128, groups*128, groups=groups,
                                kernel_size=2, dilation=1024, stride=1)

        # go back to 1 channel
        self.conv12 = nn.Conv1d(groups*128, 1, groups=groups, kernel_size=1)
        # this allows (almost) any size for the input data,
        # but I'm also too lazy to calculate
        # calculation *is* possible  and would be slightly more efficient than
        # doing a partial forward pass at initialisation

        # sim shapes is used in swyft to give you the shape of the simulator
        # output, in our case a 1d segment
        # swyft needs this to construct the tail network
        self.n_features = self.num_features(self.
                                            conv(torch.randn(1, 1,
                                                             segment_length)))


    def num_features(self, x):
        # calculates the number of features when the tensor is flattened
        size = x.size()[1:]  # ignore batch dimension
        count = 1
        for s in size:
            count *= s
        return count

    def conv(self, x):
        # spec = gs.GridSpec(nrows=3, ncols=2)
        # fig = plt.figure(figsize=(10, 10)) 
        # ax = fig.add_subplot(spec[0,0])
        # ax.hist(x.cpu().detach().numpy().flatten(), bins=100)
        # ax.set_title("Input")
        x = self.conv1(x)
        x = F.relu(x)
        # ax = fig.add_subplot(spec[1,0])
        # ax.hist(x.cpu().detach().numpy().flatten())
        # ax.set_title("conv1")

        x = self.conv2(x)
        x = F.relu(x)
        # x = F.max_pool1d(x, 4)
        # ax = fig.add_subplot(spec[1,1])
        # ax.hist(x.cpu().detach().numpy().flatten())
        # ax.set_title("conv2")

        x = self.conv3(x)
        x = F.relu(x)
        # ax = fig.add_subplot(spec[2,0])
        # ax.hist(x.cpu().detach().numpy().flatten())
        # ax.set_title("conv3")

        x = self.conv4(x)
        x = F.relu(x)
        # x = F.max_pool1d(x, 8)
        # ax = fig.add_subplot(spec[2,1])
        # ax.hist(x.cpu().detach().numpy().flatten())
        # ax.set_title("conv4")
        
        x = self.conv5(x)
        x = F.relu(x)

        x = self.conv6(x)
        x = F.relu(x)
        x = self.conv7(x)
        x = F.relu(x)
        x = self.conv8(x)
        x = F.relu(x)
        x = self.conv9(x)
        x = F.relu(x)
        x = self.conv10(x)
        x = F.relu(x)
        # x = self.conv11(x)
        # x = F.relu(x)
        x = self.conv12(x)
        x = F.relu(x)
        # fig.savefig("conv_outputs.svg")
        # plt.close(fig)

        return x

    def forward(self, obs):
        x = obs["data"]
        # online normalization would be better, but it's scaling is too
        # little... seems to scale by sqrt(std) instead of std
        # x = self.onl_f(x)

        # TODO: not hardcoded multiplier
        # scale mutliplier for precision
        scale = 1e18
        x = x*scale
        # take variance for each sample
        x_var, x_mean = torch.var_mean(x, unbiased=False)
        # normalize by variance
        x = (x-x_mean)/torch.sqrt(x_var)

        # insert channel dimension
        x = torch.unsqueeze(x, 1)

        x = self.conv(x)

        # flatten into feature vector of shape (batch_size, 2*samples) 
        x = x.view(-1, self.n_features)

        return x


class myHead(swyft.Module):
    """
    Head network to extract features.
    TODO: try inception blocks
    """
    def __init__(self, sim_shapes):
        super().__init__(sim_shapes=sim_shapes)

        segment_length = sim_shapes["data"][0]
        self.onl_f = swyft.OnlineNormalizationLayer(torch.Size([segment_length]))

        n_channels = 1
        groups = 1
        if SPECTRAL:
            n_channels = 2
            # depth-wise convolution
            groups = 2

        self.conv1 = nn.Conv1d(n_channels, groups*8, groups=groups, kernel_size=64, dilation=1, stride=1)
        self.conv2 = nn.Conv1d(groups*8, groups*8, groups=groups, kernel_size=32, dilation=1, stride=1)
        self.conv3 = nn.Conv1d(groups*8, groups*16, kernel_size=16, dilation=8, stride=1)
        self.conv4 = nn.Conv1d(groups*16, 16, kernel_size=16, dilation=4, stride=1)
        self.conv5 = nn.Conv1d(16, 32, kernel_size=3, dilation=3)
        self.conv6 = nn.Conv1d(32, 32, kernel_size=3, dilation=3)

        # this allows (almost) any size for the input data,
        # but I'm also too lazy to calculate
        # calculation *is* possible  and would be slightly more efficient than
        # doing a partial forward pass at initialisation

        # sim shapes is used in swyft to give you the shape of the simulator
        # output, in our case a 1d segment
        # swyft needs this to construct the tail network
        self.n_features = self.num_features(self.
                                            conv(torch.randn(1, 1,
                                                             segment_length)))

    def num_features(self, x):
        # calculates the number of features when the tensor is flattened
        size = x.size()[1:]  # ignore batch dimension
        count = 1
        for s in size:
            count *= s
        return count

    def conv(self, x):
        if SPECTRAL:
            x_fft = torch.fft.rfft(x)
            x_mag = torch.abs(x_fft)
            x_arg = torch.angle(x_fft)
            x = torch.cat((x_mag, x_arg), dim=1)
        spec = gs.GridSpec(nrows=3, ncols=2)
        fig = plt.figure(figsize=(10, 10)) 
        ax = fig.add_subplot(spec[0, 0])
        ax.plot(x.cpu().detach().numpy()[0].flatten())
        # ax.hist(x.cpu().detach().numpy().flatten(), bins=100)
        ax.set_title("Input waveform")

        biases = []
        kernels = []
        for idx, param in enumerate(self.parameters()):
            # first param is first layer weights
            if idx%2:
                biases += param.cpu().detach().numpy().flatten().tolist()
            else:

                kernels += param.cpu().detach().numpy().flatten().tolist()

        ax = fig.add_subplot(spec[0, 1])
        ax.hist(kernels, bins=100)
        ax.set_title("conv1d kernels")

        ax = fig.add_subplot(spec[1, 1])
        ax.hist(biases, bins=100)
        ax.set_title("conv1d biases")

        x = self.conv1(x)
        x = F.relu(x)
        # ax = fig.add_subplot(spec[1,0])
        # ax.hist(x.cpu().detach().numpy().flatten())
        # ax.set_title("conv1")

        x = self.conv2(x)
        x = F.relu(x)
        # x = F.max_pool1d(x, 4)
        # ax = fig.add_subplot(spec[1,1])
        # ax.hist(x.cpu().detach().numpy().flatten())
        # ax.set_title("conv2")

        x = self.conv3(x)
        x = F.relu(x)
        # ax = fig.add_subplot(spec[2,0])
        # ax.hist(x.cpu().detach().numpy().flatten())
        # ax.set_title("conv3")

        x = self.conv4(x)
        x = F.relu(x)
        # x = F.max_pool1d(x, 8)
        # ax = fig.add_subplot(spec[2,1])
        # ax.hist(x.cpu().detach().numpy().flatten())
        # ax.set_title("conv4")

        fig.savefig("conv_outputs.svg")
        plt.close(fig)

        return x

    def forward(self, obs):
        x = obs["data"]
        # online normalization would be better, but it's scaling is too
        # little... seems to scale by sqrt(std) instead of std
        # x = self.onl_f(x)

        # TODO: not hardcoded multiplier
        # scale mutliplier for precision
        scale = 1e18
        x = x*scale
        # take variance and mean for the batch
        # if you want this for each sample, add dim=1 and keepdim=True
        x_var, x_mean = torch.var_mean(x, unbiased=False)
        # normalize by variance
        x = (x-x_mean)/torch.sqrt(x_var)

        # insert channel dimension
        x = torch.unsqueeze(x, 1)

        x = self.conv(x)

        # flatten into feature vector of shape (batch_size, 2*samples) 
        x = x.view(-1, self.n_features)

        return x


class myTail(swyft.Module):
    def __init__(
        self,
        n_head_features: int,
        marginals,
        n_compressed: int = 50
    ):
        """My own tail network.

        Args:
            n_head_features and marginals are expected when this is delivered
            uninitialized to swyft.posterior.

            other args should be kwargs!
        """

        super().__init__()

        self.marginals = marginals
        n_channels, pdim = swyft.networks.tail._get_z_shape(marginals)
        self.n_channels = n_channels

        # one compression layer
        n_compressed = n_head_features
        self.lnc1 = LinearWithChannel(n_channels,
                                      n_head_features,
                                      n_compressed)

        # normalization for parameters
        self.norm = swyft.OnlineNormalizationLayer(
            torch.Size([n_channels, pdim])
        )

        # ratio estimator: takes in features + parameters, outputs ln(ratio)
        # for each marginal

        self.lnc2 = LinearWithChannel(n_channels, pdim + n_compressed, 100)
        self.lnc3 = LinearWithChannel(n_channels, 100, 100)
        self.lnc4 = LinearWithChannel(n_channels, 100, 100)
        self.lnc5 = LinearWithChannel(n_channels, 100, 1)

    def forward(self, f: torch.Tensor, params):
        if INSPECT:
            spec = gs.GridSpec(nrows=2, ncols=2)
            fig = plt.figure(figsize=(10, 10))

        # each marginal will have a channel, all start with the same features
        f = f.unsqueeze(1).repeat(1, self.n_channels, 1)

        # compressor
        f = F.relu(self.lnc1(f))

        if INSPECT:
            ax = fig.add_subplot(spec[0,0])
            ax.hist(f.cpu().detach().numpy().flatten(), bins = 50)
            ax.set_title("compressor")

        # my normalization of params
        par_var = torch.var(params, dim=1, keepdim=True)
        params = params/torch.sqrt(par_var)

        if INSPECT:
            ax = fig.add_subplot(spec[0,1])
            ax.hist(params.cpu().detach().numpy().flatten(), bins = 50)
            ax.set_title("Params")

        # make parameter vector, for each channel (marginal)
        z = swyft.networks.tail._combine(params, self.marginals)
        # normalize parameter values
        # z = self.norm(z)

        # concatenate features with parameters, for each channel
        x = torch.cat([f, z], -1)

        # produce ratio estimates for each channel
        x = F.relu(self.lnc2(x))
        x = F.relu(self.lnc3(x))
        x = F.relu(self.lnc4(x))
        x = self.lnc5(x)

        if INSPECT:
            ax = fig.add_subplot(spec[1,0])
            ax.hist(x.cpu().detach().numpy().flatten(), bins = 50)
            ax.set_title("Out")

            fig.savefig("lin_outputs.svg")
            plt.close(fig)

        return x


def doTraining(dataset, hyper_pars):

    optimizer_args = {key: hyper_pars[key] for key in ["lr"]}
    scheduler_args = {key: hyper_pars[key] for key in ["factor", "patience"]}

    post = swyft.Posteriors(dataset)
    # TODO don't hardcode
    marginals = [0]
    post.add(marginals, device=DEVICE, head=theHead)  # , tail=myTail)
    post.train(marginals, max_epochs=EPOCHS,
               early_stopping_patience=hyper_pars["max_fruitless_epoch"],
               optimizer=torch.optim.SGD,
               optimizer_args=optimizer_args,
               scheduler_args=scheduler_args,
               batch_size=BATCH_SIZE)

    return post


def trainAgent(dataset, hyper_pars):
    """
    Trains agent with hyper_pars and returns the metrics of interest in a
    dictionary.
    """
    post = doTraining(dataset, hyper_pars)

    diagnostics = post.train_diagnostics([0])[0]
    train_loss = np.array(diagnostics["train_loss"])
    valid_loss = np.array(diagnostics["valid_loss"])

    # metrics of interest
    best_loss = valid_loss[-1]
    loss_mse = ((train_loss - valid_loss)**2).mean()

    return dict(best_loss=best_loss, loss_mse=loss_mse), diagnostics


# globals
rng = default_rng()
INSPECT = False
SPECTRAL = False
LOAD = True
SAVE = True
PARALLEL = False
DEVICE = torch.device("cuda:0")
EPOCHS = 100
SIGMA = 10.
DO_PLOT = True
GENS = 50  # generations
CHECKPOINTS = 50
# winners and stayers for each ranking
WINNERS = 8
STAYERS = 2
PERF_BIAS = {"best_loss": .75,
             "loss_mse": .25}
# POP = WINNERS*len(PERF_BIAS)*3 + STAYERS*len(PERF_BIAS)  # population
POP = 50
n_processes = 2
MAX_BITS = 32

hyper_range = {"max_fruitless_epoch": (1, EPOCHS, "int"),
               "patience": (1, EPOCHS, "int"),
               "factor": (1e-1, 1, "double"),
               "lr": (1e-8, 1e-1, "double")}

# Initialize Genetics
genetics = Genetics(hyper_range, MAX_BITS)

pop = []

for i in range(POP):
    hyper_pars = {}
    for key, (lo, hi, type_str) in hyper_range.items():
        if type_str == "int":
            # uniform over integers
            hyper_pars[key] = rng.integers(low=lo, high=hi)
        elif type_str == "double":
            # uniform in exponent
            exponent = rng.uniform(low=np.log(lo), high=np.log(hi))
            hyper_pars[key] = np.exp(exponent)
        else:
            print(f"the given type {type_str} is not supported!",
                  flush=True)
            raise ValueError

    pop.append(hyper_pars)

srate = 2048  # in Hz
npoints = 1*srate  # seconds
nwaves = 5e3
BATCH_SIZE = 400
# sim
print("Setting up swyft simulator", flush=True)
# global function that will be called by helper function
# SIM = SimulateWaveform(npoints, srate, "IMRPhenomD", "H1", snr=10)
par_names = ["m1", "m2"]
npars = len(par_names)
sim = swyft.Simulator(simpleGW, par_names, sim_shapes={"data": (npoints,)})

# max mass is 9, more than this gives too little signal length after 40 Hz
m_min = 1
m_max = 9.
prior = swyft.Prior(lambda u: m_min + (m_max-m_min)*u, npars)
# run genetic algo
print("\ninitial population:", flush=True)
genetics.printPop(pop)

pop_hist = []

if CHECKPOINTS > 0:
    CHECKGEN = GENS//CHECKPOINTS
else:
    CHECKGEN = GENS

# make new memory store and dataset
base_path = "swyft_saved"
if LOAD:
    print("Loading in data from disk")
    store = swyft.MemoryStore.load(base_path+"_store.pt")
    dataset = swyft.Dataset.load(base_path+"_dataset.pt", store)
else:
    store = swyft.MemoryStore(sim)
    store.add(nwaves, prior)
    store.simulate()
    dataset = swyft.Dataset(nwaves, prior, store)
    if SAVE:
        store.save(base_path+"_store.pt")
        dataset.save(base_path+"_dataset.pt")

for gen in range(GENS):

    # run training on population and keep relevant info
    print(f"\nTraining generation {gen}\n", flush=True)

    pop_perf, pop_diag = [], []
    if PARALLEL:
        # TODO: implement this at the PyTorch level, using a single dataloader
        datasets = [dataset for hyper_pars in pop]

        with MyPool(n_processes) as pool:
            results = pool.starmap(trainAgent, zip(datasets, pop))

        for performance, diagnostics in results:
            pop_perf.append(performance)
            pop_diag.append(diagnostics)
    else:
        for hyper_pars in pop:
            performance, diagnostics = trainAgent(dataset, hyper_pars)
            pop_perf.append(performance)
            pop_diag.append(diagnostics)

    # record history
    pop_hist.append(deepcopy(pop_perf))

    # save to file
    if gen % CHECKGEN == 0:
        # write mode automatically clears the file
        f = open("genetic_results.pkl", 'wb')
        pickle.dump(dict(pop=pop, pop_diag=pop_diag, pop_hist=pop_hist), f)
        f.close()

    # create next generation
    pop = genetics.nextGen(pop, pop_perf, n_winners=WINNERS,
                           carry_over=STAYERS, n_children=None,
                           perf_bias=PERF_BIAS)

f = open("genetic_results.pkl", 'wb')
pickle.dump(dict(pop=pop, pop_diag=pop_diag, pop_hist=pop_hist), f)
f.close()

if DO_PLOT:
    # plot diagnostics for each agent in population
    # TODO don't hardcode this
    # TODO plot top k performers
    nrows = 10
    ncols = 5
    if nrows*ncols < POP:
        # make sure that the population fits
        nrows = POP//ncols + int(POP % ncols > 0)
    height = 2
    width = 2
    spec = gs.GridSpec(nrows=nrows, ncols=ncols)
    fig = plt.figure(figsize=(height*ncols, width*nrows))

    for idx, diagnostics in enumerate(pop_diag):
        row = idx//ncols
        col = idx % ncols
        ax = fig.add_subplot(spec[row, col])
        ax.plot(diagnostics["train_loss"], label="training")
        ax.plot(diagnostics["valid_loss"], label="validation")
        ax.set_title(f"Agent {idx}")
        ax.set_xlabel("Epoch")
        ax.set_ylabel("Loss")
        # ax.legend()

    fig.tight_layout()

    plot_path = os.path.join("..", "Plots", "Loss_history_gravwaves.svg")
    fig.savefig(plot_path)
