import os
visible_devs = "0,1,2"
os.environ["CUDA_VISIBLE_DEVICES"] = visible_devs
# add parent directory as searchable for modules 
import sys
sys.path.insert(0, '..')
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec as gs
import numpy as np
from numpy.random import default_rng
import swyft
from swyft.utils import plot_posterior
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.fft
import pickle
import multiprocessing
import multiprocessing.pool
from multiprocessing import Pool
from copy import deepcopy
from Genetics import Genetics


class NoDaemonProcess(multiprocessing.Process):
    # make 'daemon' attribute always return False
    def _get_daemon(self):
        return False

    def _set_daemon(self, value):
        pass

    daemon = property(_get_daemon, _set_daemon)


# We sub-class multiprocessing.pool.Pool
# instead of multiprocessing.Pool
# because the latter is only a wrapper
# function, not a proper class.
class MyPool(multiprocessing.pool.Pool):
    Process = NoDaemonProcess


def mockWave(pars):
    alpha, beta = pars
    # alpha, beta are in [0,1]
    # scaling
    alpha = 0.1 + (alpha)*2/5

    # add some Gaussian noise to beta
    beta = beta/2
    # beta = np.random.normal(beta, .05)
    # make sure it's in domain
    # eps = 1e-3
    # beta = min(max(beta, eps), 1)
    xs = np.linspace(0, 5, npoints)
    ys = np.exp(1+alpha*xs)*np.sin(np.exp(3.5+beta*xs)-1)

    return dict(ys=ys)


def noise(sim, v=None):
    ys = sim['ys']
    ys = ys + SIGMA*np.random.randn(len(ys))
    return dict(ys=ys)


class Head(swyft.Module):
    """
    Head network do extract sine-features.
    """
    def __init__(self, sim_shapes):
        super().__init__(sim_shapes=sim_shapes)

        segment_length = sim_shapes["ys"][0]
        self.onl_f = swyft.OnlineNormalizationLayer(torch.Size([segment_length]))
        self.conv1 = nn.Conv1d(1, 32, kernel_size=16, stride=1)
        self.conv2 = nn.Conv1d(32, 8, kernel_size=8, stride=4)
        self.conv3 = nn.Conv1d(8, 16, kernel_size=16, stride=8)

        # this allows (almost) any size for the input data,
        # but I'm also too lazy to calculate
        # calculation *is* possible  and would be slightly more efficient than
        # doing a partial forward pass at initialisation

        # sim shapes is used in swyft to give you the shape of the simulator
        # output, in our case a 1d segment
        # swyft needs this to construct the tail network
        self.n_features = self.num_features(self.
                                            conv(torch.randn(1, 1,
                                                             segment_length)))

    def num_features(self, x):
        # calculates the number of features when the tensor is flattened
        size = x.size()[1:]  # ignore batch dimension
        count = 1
        for s in size:
            count *= s
            return count

    def conv(self, x):
        x = self.conv1(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 8)

        x = self.conv2(x)
        x = F.elu(x)

        x = self.conv3(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 2)

        return x

    def forward(self, obs):
        x = obs["ys"]
        x = self.onl_f(x)

        # insert channel dimension
        x = torch.unsqueeze(x, 1)

        x = self.conv(x)

        # flatten into feature vector
        x = x.view(-1, self.n_features)

        return x


def doTraining(dataset, hyper_pars):

    optimizer_args = {key: hyper_pars[key] for key in ["lr"]}
    scheduler_args = {key: hyper_pars[key] for key in ["factor", "patience"]}

    post = swyft.Posteriors(dataset)
    marginals = [0, 1, (0, 1)]
    post.add(marginals, device=DEVICE, head=Head)
    post.train(marginals, max_epochs=EPOCHS,
               early_stopping_patience=hyper_pars["max_fruitless_epoch"],
               optimizer_args=optimizer_args,
               scheduler_args=scheduler_args)

    return post


def mutation(param, p=.5):
    """
    Returns a mutation given the parameter.
    p controls the size of expected deviation compared to the size of the
    parameter range.
    """

    lo, hi, type_str = hyper_range[param]
    size = hi-lo

    if type_str == "int":
        return rng.binomial((size//2)*2, p) - size//2
    elif type_str == "double":
        return rng.normal(scale=p*size/4)
    else:
        print(f"the given type {type_str} is not supported!",
              flush=True)
        raise ValueError


def mutate(param, value, p=.01):
    """
    Performs a mutation on value. This mutation will be akin to a bit flip, but
    scaled to the parameter range. This means the change will be linear in
    log.
    """

    lo, hi, type_str = hyper_range[param]
    size = hi-lo

    """
    Should be changed to a true bit-flip. So:
        -represent the numbers as bytecode
        -flip each bit with same probability (rng.binomial(1,p,n_bits))
        -return, rejection sampling should be done externally
        """

    # trial with odds p
    if rng.binomial(1, p) == 1:
        if type_str == "int":
            # TODO bit flip-ish
            return value + rng.binomial((size//2)*2, p) - size//2
        elif type_str == "double":
            # TODO change bytecode
            expo = rng.uniform(np.log(lo), np.log(hi))
            sign = rng.integers(2)*2-1
            return value + sign*np.exp(expo)
        else:
            print(f"the given type {type_str} is not supported!",
                  flush=True)
            raise ValueError
    else:
        # no mutation
        return value


def mate(parents, n_children, rng=np.random.default_rng()):
    """
    Takes in a tuple of N parents and returns n_children agents.
    """

    N = len(parents)     # number of vertices
    D = len(parents[0])  # parametric dimension

    # for now: only allow sampling on specific points of a hypercuboid defined
    # by a simplex.
    assert N <= D+1, ("Too many parents to embed the"
                      "simplex they define")

    """
    The possible points on the hypercuboid are:
        - all it's vertices
        - all midpoints of vertices
        - the centre of mass

    TODO: A review on the combinatorics.
    TODO: Support larger set of parents
    TODO: Different sampling (Fréchet mean?)
    """

    # seperate possible vertex values per dimension
    vertex_options = {param: [parent[param] for parent in parents]
                      for param in parents[0].keys()}

    # randomly pick vertices for children
    # this is done by a series of permutations of parent indexes, which
    # guarantees the most equall participation of each parent possible
    # Using map because of old version of numpy, better to use permute
    assignment = map(rng.permutation, np.tile(np.arange(N), (D//N + 1, 1)))
    assignment = np.array(list(assignment)).flatten()[:D]

    children = []
    for child_idx in range(n_children):
        child = {}

        # loop over D parent indexes and parameters
        for (parent_idx,
             (param, options)) in zip(assignment,
                                      vertex_options.items()):
            value = options[parent_idx]
            lower_bound, upper_bound, _ = hyper_range[param]

            new_value = mutate(param, value, p=0.1)
            while new_value < lower_bound or new_value > upper_bound:
                # retry until in range
                new_value = mutate(param, value, p=1.)

            child[param] = new_value
            children.append(child)

    return children


def nextGenAlt(pop, pop_perf, n_parents=4, min_mates=2, carry_over=2):
    """
    Implementation using intuition on the parameter space
    """

    perf_keys = pop_perf[0].keys()
    pop_ranks = {}

    n_agents = len(pop)
    assert n_agents >= n_parents, "too many parents requested for population"

    for perf_key in perf_keys:
        # sort population wrt perf_key
        # for all ranking statistics we have lower to be better
        sorted_pop = [agent for _, agent
                      in sorted(zip(pop_perf, pop),
                                key=lambda pair: pair[0][perf_key])]

        pop_ranks[perf_key] = sorted_pop

    # select survivors
    survivors = []
    for perf_key in perf_keys:
        # pick top carry_over to survive
        for pick_idx in range(carry_over):
            survivor = pop_ranks[perf_key][pick_idx]
            # TODO mutate
            survivors.append(survivor)

    n_survivors = len(survivors)

    # select parents from top ranking agents
    """
    For now just select equally from rankings.
    TODO: include biased selection.
    TODO: include growth factor
    TODO: indlude biased children alocation
    """

    n_ranks = len(pop_ranks)
    if n_parents < n_ranks:
        raise NotImplementedError
    if n_parents*n_ranks > n_agents:
        raise ValueError("Too many parents to keep population stable!")

    parents_per_rank = n_parents//n_ranks
    remaining_parents = n_parents % n_ranks

    parents = []

    for perf_key in perf_keys:
        # take the top performing agents in each ranking
        for pick_idx in range(parents_per_rank):
            parents.append(pop_ranks[perf_key][pick_idx])

    if remaining_parents > 0:
        # distribute remaining parents over ranks
        # TODO: use generator
        perf_keys_rem = np.random.choice(list(perf_keys), remaining_parents,
                                         replace=False)

        for perf_key in perf_keys_rem:
            parents.append(pop_ranks[perf_key][parents_per_rank])

    np.random.shuffle(parents)
    n_mates = n_parents//min_mates
    simplices = np.array_split(parents, n_mates)

    children = []
    n_agents_left = n_agents - n_survivors
    children_per_mates = n_agents_left//n_mates
    remaining_children = n_agents_left % n_mates

    n_children_mates = np.ones(n_mates, dtype=int)*children_per_mates
    if remaining_children > 0:
        add_child_idxs = np.random.choice(n_mates, remaining_children,
                                          replace=False)
        n_children_mates[add_child_idxs] += 1

    for mates, n_children in zip(simplices, n_children_mates):
        children += mate(mates, n_children)

    return children + survivors


def trainAgent(dataset, hyper_pars):
    """
    Trains agent with hyper_pars and returns the metrics of interest in a
    dictionary.
    """
    post = doTraining(dataset, hyper_pars)

    diagnostics = post.train_diagnostics([0, 1, (0, 1)])[0]
    train_loss = np.array(diagnostics["train_loss"])
    valid_loss = np.array(diagnostics["valid_loss"])

    # metrics of interest
    best_loss = valid_loss[-1]
    loss_mse = ((train_loss - valid_loss)**2).mean()

    return dict(best_loss=best_loss, loss_mse=loss_mse), diagnostics


# globals
NAME = "High-Noise"
PARALLEL = True
n_processes = 16
DEVICE = torch.device("cuda:0")
EPOCHS = 30
SIGMA = 15.
DO_PLOT = False
GENS = 100  # generations
CHECKPOINTS = 10
# winners and stayers for each ranking
WINNERS = 12
STAYERS = 4
PERF_BIAS = {"best_loss": .75,
             "loss_mse": .25}
win_mult = 3
stay_mult = 1
POP = WINNERS*len(PERF_BIAS)*win_mult + STAYERS*len(PERF_BIAS)*stay_mult
MAX_BITS = 32

# for some reason this can't be much larger, gives error:
#   File
#   "/project/gravwav/kkoender/.MLGWvenv/lib64/python3.6/site-packages/swyft/networks/tail.py",
#   line 145, in forward
#   x = torch.cat([f, z], -1)
#   RuntimeError: Sizes of tensors must match except in dimension 2. Expected
#   size 256 but got size 128 for tensor number 1 in the list.
npoints = 1024
nwaves = 3000

hyper_range = {"max_fruitless_epoch": (1, EPOCHS, "int"),
               "patience": (1, EPOCHS, "int"),
               "factor": (1e-3, 1, "double"),
               "lr": (1e-10, 1e-2, "double")}

# initialize our genetics object
genetics = Genetics(hyper_range, MAX_BITS)

# sim
print("Setting up swyft simulator", flush=True)
par_names = ["$\\alpha$", "$\\beta$"]
npars = len(par_names)
sim = swyft.Simulator(mockWave, par_names, sim_shapes={"ys": (npoints,)})

# parameters get re-scaled in simulator
prior = swyft.Prior(lambda u: u, npars)

# sample initial population
print("Sampling initial population", flush=True)
rng = default_rng()
pop = []

for i in range(POP):
    hyper_pars = {}
    for key, (lo, hi, type_str) in hyper_range.items():
        if type_str == "int":
            hyper_pars[key] = rng.integers(low=lo, high=hi)
        elif type_str == "double":
            # linear in exponent
            # TODO maybe random bytecode?
            exponent = rng.uniform(low=np.log(lo), high=np.log(hi))
            hyper_pars[key] = np.exp(exponent)
        else:
            print(f"the given type {type_str} is not supported!",
                  flush=True)
            raise ValueError

    pop.append(hyper_pars)

# run genetic algo
print("\ninitial population:", flush=True)
genetics.printPop(pop)

pop_hist = []

if CHECKPOINTS > 0:
    CHECKGEN = GENS//CHECKPOINTS
else:
    CHECKGEN = GENS

# make new memory store and dataset
for gen in range(GENS):
    store = swyft.MemoryStore(sim)
    store.add(nwaves, prior)
    store.simulate()
    dataset = swyft.Dataset(nwaves, prior, store, simhook=noise)

    # run training on population and keep relevant info
    print(f"\nTraining generation {gen}\n", flush=True)

    pop_perf, pop_diag = [], []
    if PARALLEL:
        # TODO: implement this at the PyTorch level, using a single dataloader
        datasets = [dataset for hyper_pars in pop]

        with MyPool(n_processes) as pool:
            results = pool.starmap(trainAgent, zip(datasets, pop))

        for performance, diagnostics in results:
            pop_perf.append(performance)
            pop_diag.append(diagnostics)
    else:
        for hyper_pars in pop:
            """
            TODO: make multiprocessing work by having a seperate file that does all
            the swyft stuff so it can be done at the same time.
            However, could a MemoryStore be used by multiple processes? Otherwise
            find workaround to atleast keep the same simulation results (deepcopy?)
            """
            performance, diagnostics = trainAgent(dataset, hyper_pars)
            pop_perf.append(performance)
            pop_diag.append(diagnostics)

    # record history
    pop_hist.append(deepcopy(pop_perf))

    # save to file
    if gen % CHECKGEN == 0:
        # write mode automatically clears the file
        file_name = f"genetic-results-{NAME}.pkl"
        f = open(file_name, 'wb')
        pickle.dump(dict(pop=pop, pop_diag=pop_diag, pop_hist=pop_hist), f)
        f.close()

    # create next generation
    pop = genetics.nextGen(pop, pop_perf, n_winners=WINNERS,
                           carry_over=STAYERS, n_children=None,
                           perf_bias=PERF_BIAS, verbose=True)

file_name = f"genetic-results-{NAME}.pkl"
f = open(file_name, 'wb')
pickle.dump(dict(pop=pop, pop_diag=pop_diag, pop_hist=pop_hist), f)
f.close()

if DO_PLOT:
    # plot diagnostics for each agent in population
    # TODO don't hardcode this
    # TODO plot top k performers
    nrows = 10
    ncols = 5
    if nrows*ncols < POP:
        # make sure that the population fits
        nrows = POP//ncols + int(POP % ncols > 0)
        height = 2
        width = 2
        spec = gs.GridSpec(nrows=nrows, ncols=ncols)
        fig = plt.figure(figsize=(height*ncols, width*nrows))

    for idx, diagnostics in enumerate(pop_diag):
        row = idx//ncols
        col = idx % ncols
        ax = fig.add_subplot(spec[row, col])
        ax.plot(diagnostics["train_loss"], label="training")
        ax.plot(diagnostics["valid_loss"], label="validation")
        ax.set_title(f"Agent {idx}")
        ax.set_xlabel("Epoch")
        ax.set_ylabel("Loss")
        # ax.legend()

    fig.tight_layout()

    plot_path = os.path.join("..", "Plots", f"Loss-history-mockwave-{NAME}.svg")
    fig.savefig(plot_path)
