import os
visible_devs = "0,1,2,3"
os.environ["CUDA_VISIBLE_DEVICES"] = visible_devs
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec as gs
import numpy as np
import swyft
from swyft.utils import plot_posterior
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.fft



def sinSim(pars):
    phi, theta = pars
    n = 1024
    xs = np.linspace(0, np.pi, n)
    ys = np.cos(theta*xs) + np.sin(phi*xs)

    return dict(ys=ys)


def mockWave(pars):
    alpha, beta = pars
    # scaling
    alpha = 0.1 + (alpha)*2/5
    beta = beta/2
    xs = np.linspace(0, 5, npoints)
    ys = np.exp(1+alpha*xs)*np.sin(np.exp(3.5+beta*xs)-1)

    return dict(ys=ys)


def noise(sim, v=None):
    ys = sim['ys']
    ys = ys + SIGMA*np.random.randn(len(ys))
    return dict(ys=ys)


class Head(swyft.Module):
    """
    Head network do extract sine-features.
    """
    def __init__(self, sim_shapes):
        super().__init__(sim_shapes=sim_shapes)

        segment_length = sim_shapes["ys"][0]
        self.onl_f = swyft.OnlineNormalizationLayer(torch.Size([segment_length]))
        self.conv1 = nn.Conv1d(1, 32, kernel_size=16, stride=1)
        self.conv2 = nn.Conv1d(32, 8, kernel_size=8, stride=4)
        self.conv3 = nn.Conv1d(8, 16, kernel_size=16, stride=8)

        # this allows (almost) any size for the input data,
        # but I'm also too lazy to calculate
        # calculation *is* possible  and would be slightly more efficient than
        # doing a partial forward pass at initialisation

        # sim shapes is used in swyft to give you the shape of the simulator
        # output, in our case a 1d segment
        # swyft needs this to construct the tail network
        self.n_features = self.num_features(self.
                                            conv(torch.randn(1, 1,
                                                             segment_length)))

    def num_features(self, x):
        # calculates the number of features when the tensor is flattened
        size = x.size()[1:]  # ignore batch dimension
        count = 1
        for s in size:
            count *= s
        return count

    def conv(self, x):
        x = self.conv1(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 8)

        x = self.conv2(x)
        x = F.elu(x)

        x = self.conv3(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 2)

        return x

    def forward(self, obs):
        x = obs["ys"]
        x = self.onl_f(x)

        # insert channel dimension
        x = torch.unsqueeze(x, 1)

        x = self.conv(x)

        # flatten into feature vector
        x = x.view(-1, self.n_features)

        return x


def doRound(bound, obs, final=False):
    """
    Does a round of the nested posterior estimation. Effectively "zooming in"
    on the parameter space by Truncation.
    """
    # Update store to truncated prior
    store.add(nsines, prior, bound=bound)
    # Simulate needed samples
    store.simulate()

    # The usual
    dataset = swyft.Dataset(nsines, prior, store, bound=bound, simhook=noise)
    post = swyft.Posteriors(dataset)
    # Only add the 2-dim marginal in last round, is not needed for truncation
    if final:
        marginals = [0, 1, (0, 1)]
    else:
        marginals = [0, 1]
    post.add(marginals, device=DEVICE, head=Head)
    post.train(marginals, max_epochs=EPOCHS,
               early_stopping_patience=max_fruitless_epoch,
               optimizer_args=dict(lr=lr),
               scheduler_args=dict(factor=factor, patience=patience))

    # Truncate
    new_bound = post.truncate(marginals, obs)
    return post, new_bound


def centralConfidence(v, zm, conf=0.68):
    """
    Returns the indices that define the central confidence
    """
    v = v.flatten()
    v_arg = np.argsort(v)[::-1]  # Sort backwards
    total_mass = v.sum()
    enclosed_mass = np.cumsum(v[v_arg]) # Sum large to small
    select_conf = enclosed_mass >= total_mass * conf
    # translate to original indexes
    return v_arg[select_conf]


def plotProbableSignals(post_samples, n_plot, alpha_base=0.05, conf=.68, ax=None):
    """
    Uses posterior samples to plot examples within confidence, returns last
    drawn line.
    """

    x_alpha = post_samples['v'][:, 0]
    x_beta = post_samples['v'][:, 1]

    xs = np.linspace(0, np.pi, npoints)

    # determine confidence interval 
    # TODO: shoould do this jointly...
    v_a, e_a = np.histogram(x_alpha, bins=100, density=True)
    # central values
    zm_a = (e_a[1:] + e_a[:-1])/2
    conf_idx_a = centralConfidence(v_a, zm_a)
    # to be sure sort indices
    conf_idx_a.sort()
    # find when idx changes with more than 1
    # edges = (conf_idx_a[1:] - conf_idx_a[:1]) > 1
    conf_int_a = (zm_a[conf_idx_a[0]], zm_a[conf_idx_a[-1]])

    v_b, e_b = np.histogram(x_beta, density=True)
    zm_b = (e_b[1:] + e_b[:-1])/2
    conf_idx_b = centralConfidence(v_b, zm_b)
    conf_idx_b.sort()
    conf_int_b = (zm_b[conf_idx_b[0]], zm_b[conf_idx_b[-1]])

    select_conf = (conf_int_a[0] <= x_alpha) & (x_alpha <= conf_int_a[1]) &(conf_int_b[0] <= x_beta) & (x_beta <= conf_int_b[1])
    alpha_conf = x_alpha[select_conf]
    beta_conf = x_beta[select_conf]

    # plot the draws with high opacity
    if ax is None:
        fig = plt.figure(figsize=(5, 10))
        ax = fig.add_subplot(111)

    n_conf = len(alpha_conf)
    plot_idx = np.random.permutation(n_conf)
    if n_plot < n_conf:
        plot_idx = plot_idx[:n_plot]

    for idx in plot_idx:
        alpha = alpha_conf[idx]
        beta = beta_conf[idx]

        signal = mockWave((alpha, beta))["ys"]
        guess = ax.plot(xs, signal, linewidth=0.5, color='tab:orange',
                        alpha=alpha_base + 1/n_plot)

    return guess


# swyft
DEVICE = torch.device("cuda:3")
max_fruitless_epoch = 10
patience = 5
factor = .5
lr = 2e-4
EPOCHS = 50
ROUNDS = 10
MIN_VOL = 0.005

# noise
SIGMA = 10.

npoints = 1024
nsines = 10000

# known working
# max_fruitless_epoch = 8
# EPOCHS = 40
#
# npars = 1
# npoints = 1024
# nsines = 1000
# lr = 2e-3

# sim
par_names = ["$\\alpha$", "$\\beta$"]
npars = len(par_names)
sim = swyft.Simulator(mockWave, par_names, sim_shapes={"ys": (npoints,)})

# prior
# prior = swyft.Prior(lambda u: np.array([10, 5])*u + np.array([-5, 0]), npars)
prior = swyft.Prior(lambda u: u, npars)

store = swyft.MemoryStore(sim)

u = np.random.uniform(size=npars)
# v = 10*u - 5
v = (0.25, 0.8)
sgn = mockWave(v)
obs = noise(sgn)

# Truncated training
bound = None
path = "posterior_mockwave.pt"
for i in range(ROUNDS):
    post, bound = doRound(bound, obs,
                          final=i == (ROUNDS-1))
    # clear save
    if os.path.exists(path):
        os.remove(path)
    post.save(path)
    if bound.volume < MIN_VOL:
        post, bound = doRound(bound, obs,
                              final=True)

        if os.path.exists(path):
            os.remove(path)
        post.save(path)
        break

# plot loss history (up to the best validation loss)
# note that the loss for each epoch is the summed loss of each seperate tail,
# so for each seperate marginal.
diagnostics = post.train_diagnostics([0, 1, (0, 1)])[0]
np.save("diagnostics.npy", diagnostics)

fig = plt.figure(figsize=(10, 10))
plt.plot(diagnostics["train_loss"], label="training")
plt.plot(diagnostics["valid_loss"], label="validation")
plt.title("Loss evolution up to best epoch")
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.legend()

plot_path = os.path.join("..", "Plots", "Loss_history_mockwave.svg")
fig.savefig(plot_path)

resolution = 1e5
post_samples = post.sample(int(resolution), obs)

# TODO use plot_posterior and add line manually
nrows = 2
ncols = 4
height = 4
width = 4
spec = gs.GridSpec(nrows=nrows, ncols=ncols)
fig = plt.figure(figsize=(height*ncols, width*nrows))
fig.suptitle(r'SWYFT with model $\exp\left(1+\alpha x\right)\sin\left(\exp\left(3.5+\beta x\right)-1\right)$')


marg1d = [0, 1]
for idx, pois in enumerate(marg1d):
    # For now okay since we only have 2 maringals
    curr_ax = fig.add_subplot(spec[0, idx])
    plot_posterior(post_samples,
                   (pois,),
                   ax=curr_ax,
                   grid_interpolate=False,
                   color="k",
                   bins=100,
                   contours=True)
    curr_ax.set_xlabel(par_names[pois])
    curr_ax.set_xlim(0,1)

    # true value
    print(f"True value for {par_names[pois]}: {v[pois]:.3f}")
    curr_ax.axvline(v[pois], ls=":", color="r")

# 2d contour
curr_ax = fig.add_subplot(spec[:, 2:])
plot_posterior(post_samples,
               (0, 1),
               ax=curr_ax,
               grid_interpolate=False,
               color="k",
               bins=100,
               contours=True)

curr_ax.set_xlim(0,1)
curr_ax.set_ylim(0,1)

curr_ax.set_xlabel(par_names[0])
curr_ax.axvline(v[0], ls=":", color='r')

curr_ax.set_ylabel(par_names[1])
curr_ax.axhline(v[1], ls=":", color='r')


# signal
ax = fig.add_subplot(spec[1, 0:2])
xs = np.linspace(0, np.pi, npoints)
ax.plot(xs, obs["ys"], label="Observation", linewidth=0.75)
guess = plotProbableSignals(post_samples, 20, conf=38.2, alpha_base=0.2, ax=ax)
ax.plot(xs, sgn["ys"], label="Signal", color="tab:red", linestyle='--',
        linewidth=0.75)
ax.legend()

plot_path = os.path.join("..", "Plots", "posterior_mockwave.svg")
fig.savefig(plot_path)

print(f"Posterior plot saved at {plot_path}")
