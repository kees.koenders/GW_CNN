from optparse import OptionParser
from itertools import cycle
import pickle
import swyft
import SimpleSwyftUtils as utils


def parse_stdin():
    parser = OptionParser(description=__doc__)
    parser.add_option("--gpu", type="string", dest="gpu", default="0",
                      help="Set the indexes of gpus to use, seperated by"
                      "only a comma. Default is 0")
    parser.add_option("--results-file", type="string", dest="result_path",
                      help="Set the path to the file containing genetic"
                      "algorithm results")
    parser.add_option("--nwaves", type="int", dest="nwaves",
                      help="Set the amount of waves to test on")
    parser.add_option("--batch-size", type="int", dest="batch_size",
                      default=50, help="set the batch size")
    parser.add_option("--num-workers", type="int", dest="workers", default=1,
                      help="Set the maximum number of workers to use. Only"
                      "executes in parallel if greater than 1")
    parser.add_option("--out-name", type="string", dest="out_name", default="",
                      help="Set the name for plots, validation results")
    parser.add_option("--plt-ext", type="string", dest="plt_ext",
                      default=".png", help="Set the extension for plots")

    options, _ = parser.parse_args()
    return options


if __name__ == '__main__':
    options = parse_stdin()
    gpu_list = list(map(int, options.gpu.split(",")))

    with open(options.result_path, 'rb') as results_file:
        results = pickle.load(results_file)
        population = results['pop']

        # swyft setup
        # TODO get rid of some hardcoding?
        mockwave = utils.MockWave(1024, 10.)
        sim = swyft.Simulator(mockwave,
                              mockwave.par_names,
                              mockwave.sim_shapes)
        prior = mockwave.prior()
        store = swyft.MemoryStore(sim)
        store.add(options.nwaves, prior)
        store.simulate()

        dataset = swyft.Dataset(options.nwaves,
                                prior,
                                store,
                                simhook=mockwave.noise)

        marginals = [0, 1, [0, 1]]
        trainer = utils.SwyftTrainer(utils.simpleHead,
                                     dataset,
                                     marginals,
                                     epochs=30)
        if options.workers > 1:
            devices = []

            # this should really be done by creating Processes manually
            gpu_cycl = cycle(gpu_list)
            while len(devices) < len(population):
                gpu = next(gpu_cycl)
                devices.append(f'cuda:{gpu}')

            with utils.MyPool(options.workers) as pool:
                # TODO: use unordered map for efficiency, keep track of index
                # by having trainAgent return it.
                posteriors = pool.starmap(trainer.doTraining,
                                          zip(population, devices))

        else:
            posteriors = []
            for hyper_pars in population:
                posterior = (trainer.doTraining(hyper_pars,
                                                device=f'cuda:{gpu_list[0]}'))
                posteriors.append(posterior)

        print("Saving")
        save_name = f"{options.out_name}.pkl"
        with open(save_name, 'wb') as f:
            pickle.dump(dict(pop=population,
                             posts=posteriors),
                        f)
