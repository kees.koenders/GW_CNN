from bitstring import BitArray
import numpy as np


class Genetics():

    def __init__(self, par_info, max_bits=4):
        """
        Initialize globals for all encoding.
        """
        # generator
        self.rng = np.random.default_rng()
        # TODO relying on sorted order of _par_info.
        self.bit_count = {}
        self.par_info = {}
        tot_bits = 0
        for key, (lo, hi, type_str) in sorted(par_info.items(),
                                              key=lambda x: x[0]):
            print(type_str)
            if type_str == "int":
                self.par_info[key] = (lo, hi, type_str)
                # bits needed to encode all integers in range
                n_bits = int(np.log2(hi-lo)) + 1
                if n_bits > max_bits:
                    n_bits = max_bits
                self.bit_count[key] = n_bits
                tot_bits += n_bits
                continue
            elif type_str == "double":
                self.par_info[key] = (lo, hi, type_str)
                self.bit_count[key] = max_bits
                tot_bits += max_bits
                continue
            else:
                raise ValueError(f"Type {type_str} not supported!")


    def Encode(self, agent):
        """
        agent: dictionary of parameters.
        Given agent and info of parameters as a dictionary with tuples 
        (min_val, max_val, type), returns BitArray that represents these numbers in
        a "chromosome"
        """
        chromosome = BitArray()
        for key, (lo, hi, type_str) in sorted(self.par_info.items(),
                                              key=lambda x: x[0]):
            if type_str == "int":
                value = agent[key]
                # bits needed to encode all integers in range
                n_bits = self.bit_count[key]
                scaled_value = value - lo
                bits = BitArray(f"uint:{n_bits}={scaled_value}")
                chromosome += bits
                continue
            if type_str == "double":
                value = agent[key]
                n_bits = self.bit_count[key]
                scale = (hi-lo)/(2**n_bits-1)
                scaled_value = int((value - lo)/scale)
                bits = BitArray(f"uint:{n_bits}={scaled_value}")
                # append bits to chromosome
                chromosome += bits
                continue

        return chromosome

    def Decode(self, chromosome):
        """
        Decodes chromosome to agent
        """
        agent = {}
        for key, (lo, hi, type_str) in sorted(self.par_info.items(),
                                              key=lambda x: x[0]):
            if type_str == "int":
                nbits = self.bit_count[key]
                # first nself.bits bits encode this value
                agent[key] = chromosome[:nbits].uint + lo
                # update
                chromosome = chromosome[nbits:]
                continue
            if type_str == "double":
                nbits = self.bit_count[key]
                scale = (hi-lo)/(2**nbits-1)
                agent[key] = chromosome[:nbits].uint*scale + lo
                chromosome = chromosome[nbits:]
                continue

        return agent

    def Mutate(self, chromosome, p=0.03):
        """
        Each bit has chance p of flipping.
        """
        nbits = len(chromosome)
        to_flip = self.rng.binomial(1, p, size=nbits).astype(bool)
        flip_pos = np.arange(nbits, dtype=int)[to_flip]
        chromosome.invert(flip_pos)
        return chromosome

    def Crossover(self, parents, k_points=1):
        """
        Performs k-point crossover
        Assumes parents are provided as a list/array/tuple of BitArrays
        Returns: list containing two children
        """
        if len(parents) > 2:
            raise ValueError("Too many parents to perform crossover")

        nbits = len(parents[0])

        def crossover(parents):
            """
            Performs a single crossover, returning two children
            """
            cross_idx = self.rng.integers(nbits)

            # TODO extend to multiple parents?
            return (parents[0][:cross_idx] + parents[1][cross_idx:],
                    parents[1][:cross_idx] + parents[0][cross_idx:])
        # initialize for recursion
        children = parents
        for idx in range(k_points):
            # apply crossover recursively
            children = crossover(children)

        return children

    def sortPop(self, pop, pop_perf):
        """
        Returns a dict with performance keys, each having  a sorted list of agents
        idxs.
        """
        perf_keys = pop_perf[0].keys()
        pop_ranks = {}

        for perf_key in perf_keys:
            # sort population wrt perf_key
            # for all ranking statistics we have lower to be better
            # sorted list be of population indices!
            sorted_pop = [idx for idx, (_, agent)
                          in sorted(enumerate(zip(pop_perf, pop)),
                                    key=lambda x: x[1][0][perf_key])]

            pop_ranks[perf_key] = sorted_pop

        return pop_ranks

    def Benford(self, base, digit):
        """
        Returns Benford probablities for given base and digit.
        """
        return np.log(1.+1./digit)/np.log(base)

    def nextGen(self, pop, pop_perf, n_winners, carry_over=2, n_children=None,
                perf_bias=None, verbose=False):
        """
        pop is population of decoded chromosomes. n_winners is how many chromosomes enter
        the mating pool for each ranking. carry_over is how many are carried over
        to the next generation for each ranking.
        Implementation using binary encoding and standard GA techniques.

        n_winners tells us how many agents "win" wrt a ranking and are entered in
        mating.
        """

        # bias must be normalised
        if perf_bias is not None:
            tot = 0.
            for _, value in perf_bias.items():
                tot += value
            assert tot == 1.00, "bias not normalized!"

        perf_keys = pop_perf[0].keys()
        pop_ranks = {}

        for perf_key in perf_keys:
            # sort population wrt perf_key
            # for all ranking statistics we have lower to be better
            # sorted list be of population indices!
            sorted_pop = [idx for idx, (_, agent)
                          in sorted(enumerate(zip(pop_perf, pop)),
                                    key=lambda x: x[1][0][perf_key])]

            pop_ranks[perf_key] = sorted_pop

        # print best performing
        if verbose:
            print("Top agents for each performance metric:\n")
            for perf_key, sorted_pop in pop_ranks.items():
                top_idx = sorted_pop[0]
                top_perf = pop_perf[top_idx]
                print(f"Agent {top_idx}:  ", end="")
                for key, perf in top_perf.items():
                    print(f"{key}: {perf:.4g}   ", end="")
                print("")



        # select survivors
        survivors = []
        for perf_key in perf_keys:
            # pick top carry_over to survive
            for pick_idx in range(carry_over):
                survivor = pop[pop_ranks[perf_key][pick_idx]]
                # survivor = Encode(survivor)
                # survivor = Mutate(survivor)
                # survivor = Decode(survivor)
                survivors.append(survivor)

        n_survivors = len(survivors)
        pop_size = len(pop)
        if n_children is None:
            n_children = pop_size - n_survivors

        # select parents from top ranking agents
        """
        For now just select equally from rankings.
        TODO: include biased selection, using Benford's law.
        """

        n_ranks = len(pop_ranks)

        # base probability for winners, according to their placing
        # base should be 1 higher than winners as 0 is not included
        p_bases = self.Benford(n_winners+1, np.arange(n_winners) + 1)
        p = np.zeros(pop_size)  # array to hold parent-idx - probability values

        # build the probability dict/table
        for perf_key in perf_keys:
            # take the top performing agents in each ranking
            winners_idx = pop_ranks[perf_key][:n_winners]
            for place, idx in enumerate(winners_idx):
                if perf_bias is None:
                    p[idx] += p_bases[place]/n_ranks
                    continue
                p[idx] += p_bases[place]*perf_bias[perf_key]

        # If there is still room for children, use np.random.Generator.choice
        # to select 2 parents, using probability
        next_gen = []
        while len(next_gen) < n_children:
            parents_idx = self.rng.choice(pop_size, size=2, replace=False, p=p)
            # Get values and encode
            parents = [self.Encode(pop[idx]) for idx in parents_idx]
            # Mate
            children = self.Crossover(parents)
            # Mutate and Decode
            children = [self.Decode(self.Mutate(child)) for
                        child in children]
            if len(next_gen) + 2 <= n_children:
                next_gen += children
                continue

            # this only runs once when there is room for just one child
            next_gen += [children[self.rng.integers(2)]]

        return survivors + next_gen

    def printPop(self, pop):
        # should be at least 5 + decimals
        min_width = 10
        # find out decimal places
        pop_mag = int(np.log10(len(pop)) + 1)

        # Heading
        pars = pop[0]
        line = "Pars"
        if len(line) > pop_mag:
            pop_mag = len(line)

        line = line + " "*(pop_mag-len(line))
        line += "|"
        for name in sorted(pars.keys()):
            line += f"{name:{min_width}}"
        print(line, flush=True)

        line = "="*pop_mag + "|" + "="*(len(line)-pop_mag-1)
        print(line, flush=True)

        # Values
        for agent, pars in enumerate(pop):

            line = f"{agent:{pop_mag}}|"

            for name, value in sorted(pars.items()):
                cell = f"{value:{min_width}.3g}"
                line += cell
           
            print(line, flush=True)


