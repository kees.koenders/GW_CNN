import os
from optparse import OptionParser
import torch
import torch.multiprocessing as _mp
import numpy as np
import pickle
import time

from Training import Trainer
from DataLoading import load_from_metadata, make_dataloader
from Networks import GabbardNet
from Genetics import Genetics
mp = _mp.get_context("spawn")

# TODO: batch size as learnable
# TODO: layer width (n_channels, fully connected)


class Worker(mp.Process):
    """
    Takes untrained agents and trains it given the hyper pars. Saves network-
    and hyper-parameters to disk.
    """
    def __init__(self, ID, gen, max_gen, net, loss_fn, optim, device,
                 datasets, step_epochs, checkpoint_dir,
                 pop_start, pop_end, net_kwargs=None, load_dir=None):
        """
        net: torch Module UNITIALIZED!
        optim: torch optimizer UNINITIALIZED!
        loss_fn: callable and differentiable
        device: torch device for this worker to train on
        train_loader: torch dataloader for training data
        val_loader: torch dataloader for validation data
        step_epochs: how many epochs to train before eploit and explore
        checkpoint_dir: directory to save network states to
        datasets: ((train_set, workers), (val_set, workers))
        """
        super().__init__()
        self.gen = gen
        self.max_gen = max_gen
        self.checkpoint_dir = checkpoint_dir
        self.pop_start = pop_start
        self.pop_end = pop_end
        self.load_dir = load_dir
        # init model, and deepcopy so not internal states are shared!
        if net_kwargs is None:
            model = net().to(device)
        else:
            model = net(**net_kwargs).to(device)
        # init optimizer with model, training parameters later
        optim = optim(model.parameters())

        # each worker gets its own dataloader
        dataloaders = (make_dataloader(dataset, workers)
                       for dataset, workers in datasets)

        self.trainer = Trainer(model, loss_fn, optim, *dataloaders,
                               device, step_epochs, pos=ID+1)

    def run(self):
        while self.gen.value < self.max_gen:
            # train each untrained network
            if not self.pop_start.empty():
                # get an agent to train
                agent = self.pop_start.get()
                checkpoint_path = os.path.join(self.checkpoint_dir,
                                               f"agent-{agent['id']:03}.pth")

                if self.gen.value == 0 and self.load_dir is not None:
                    # if specified, start with loaded state (transfer learning)
                    load_path = os.path.join(self.load_dir,
                                             f"agent-{agent['id']:03}.pth")
                    self.trainer.load_state(load_path)
                elif self.gen.value > 0:
                    # after the first generation, load previous state
                    self.trainer.load_state(checkpoint_path)

                # for now the hyper parameters will be only optimizer params
                self.trainer.set_optim_pars(agent['hyper_pars'])
                try:
                    val_losses, train_losses = np.array(self.trainer
                                                        .train())
                    self.trainer.save_state(checkpoint_path)

                    # TODO: allow user to specify
                    loss = val_losses[-1]
                    loss_mse = ((val_losses - train_losses)**2).mean()

                    perf = dict(loss=loss, loss_mse=loss_mse)

                    # add to trained population
                    agent['perf'] = perf
                    self.pop_end.put(agent)
                except KeyboardInterrupt:
                    break


class Breeder(mp.Process):
    """
    Takes trained population when untrained is empty. Chooses parents and makes
    new population.
    """
    def __init__(self, gen, max_gen, pop_start, pop_end, checkpoint_dir,
                 hyper_info, max_bits, carry_over, n_winners, hist,
                 perf_bias=None, early_stop=None):
        super().__init__()
        self.genetics = Genetics(hyper_info, max_bits=max_bits)
        self.Encode = self.genetics.Encode
        self.Crossover = self.genetics.Crossover
        self.Mutate = self.genetics.Mutate
        self.Decode = self.genetics.Decode
        self.Benford = self.genetics.Benford
        self.gen = gen
        self.max_gen = max_gen
        self.pop_start = pop_start
        self.pop_end = pop_end
        self.checkpoint_dir = checkpoint_dir
        self.carry_over = carry_over
        self.n_winners = n_winners
        self.rng = np.random.default_rng()
        self.hist = hist
        self.perf_bias = perf_bias
        self.early_stop = early_stop
        pass

    def sortPop(self, pop):
        # sort for each performance measure, lower is better
        pop_ranks = {}
        for perf_key in pop[0]['perf'].keys():
            sorted_pop = sorted(pop, key=lambda x: x['perf'][perf_key])
            pop_ranks[perf_key] = sorted_pop
        return pop_ranks

    def nextGen(self):
        # ready for exploit and explore
        pop = []
        # the last put item can't be accessed immediately,
        # sleep for a bit
        time.sleep(1e-3)
        while not self.pop_end.empty():
            agent = self.pop_end.get()
            pop.append(agent)

        for agent in pop:
            # make sure that NaN becomes +infinity for sorting
            for perf_key, perf in agent['perf'].items():
                if np.isnan(perf):
                    agent['perf'][perf_key] = np.inf

        pop_ranks = self.sortPop(pop)

        # if current best loss worse than previous ones, stop
        if self.early_stop is not None:
            # make sure we have had enough generations
            if self.gen.value > self.early_stop:
                curr_best_loss = pop_ranks['loss'][0]['perf']['loss']
                past_best_losses = []
                for i in range(self.early_stop):
                    best_agent = self.hist[-(i+1)]['loss'][0]
                    best_loss = best_agent['perf']['loss']
                    past_best_losses.append(best_loss)

                if curr_best_loss > max(past_best_losses):
                    # make sure trainer processes break
                    print("Stopping early as loss is not improving anymore!",
                          flush=True)
                    with self.gen.get_lock():
                        self.gen.value = self.max_gen
                    # put all agents in the queue and exit
                    for agent in pop:
                        self.pop_start.put(agent)
                    time.sleep(1e-3)
                    return

        # save entire sorted population
        self.hist.append(pop_ranks)

        # pick survivors
        survivors = []
        for key, pop_rank in pop_ranks.items():
            if isinstance(self.carry_over, dict):
                carry = self.carry_over[key]
            elif isinstance(self.carry_over, int):
                carry = self.carry_over
            else:
                print("carry_over is wrong datatype", flush=True)
                raise KeyboardInterrupt
            # add top carry if not already added
            survivors += [agent for agent
                          in pop_rank[:carry]
                          if agent['id'] not in
                          [surv['id'] for surv in survivors]]

        # save ids of leftover agents
        surv_ids = [surv['id'] for surv in survivors]
        loser_ids = [agent['id'] for agent
                     in pop if agent['id']
                     not in surv_ids]

        # print survivors
        print("Survivors:", flush=True)
        for surv in survivors:
            print(f"ID: {surv['id']:2}", end="    ")
            for key, value in surv['perf'].items():
                print(f"{key}: {value:.4f}", end="    ")
            print("\n")

        # pick parents and make children
        n_pop = len(pop)
        n_ranks = len(pop_ranks)

        # base should be 1 higher than winners as 0 is not included
        p_bases = self.Benford(self.n_winners + 1,
                               np.arange(self.n_winners) + 1)
        # array to hold parent-id - probability values
        p = np.zeros(n_pop)

        # build the probability dict/table
        for perf_key, pop_rank in pop_ranks.items():
            # take the top performing agents in each ranking
            for idx, agent in enumerate(pop_rank[:self.n_winners]):
                if self.perf_bias is None:
                    p[agent['id']] += p_bases[idx]/n_ranks
                    continue
                p[agent['id']] += p_bases[idx]*self.perf_bias[perf_key]

        # If there is still room for children,
        # use np.random.Generator.choice to
        # select 2 parents, using probability p
        # holds child states by child id for writing next gen
        child_states = {}
        offspring = []
        # ids of losers will be given to children
        while loser_ids:
            # pick parents using probability table
            parent_ids = self.rng.choice(n_pop, size=2,
                                         replace=False, p=p)
            parent_ids = parent_ids.tolist()

            # Get values and encode
            parents = []
            parent_perfs = []
            for id in parent_ids:
                try:
                    parent = next(x for x in pop if x['id'] == id)
                except StopIteration:
                    print("no ID match, breaking")
                    raise KeyboardInterrupt
                parents += [self.Encode(parent['hyper_pars'])]
                parent_perfs.append(parent['perf'])

            # Mate
            children = self.Crossover(parents)
            # Mutate and Decode
            children = [self.Decode(self.Mutate(child)) for
                        child in children]

            self.rng.shuffle(children)

            # if there are children and ids left, save child
            while children and loser_ids:
                parent_id = parent_ids.pop()
                parent_perf = parent_perfs.pop()
                child_id = loser_ids.pop()

                # get parent state
                parent_path = os.path.join(self.checkpoint_dir,
                                           f"agent-{parent_id:03}.pth")
                parent_state = torch.load(parent_path, map_location='cpu')

                # save as child state
                child_states[child_id] = parent_state

                # get child
                child = children.pop()
                offspring.append(dict(id=child_id, hyper_pars=child,
                                      perf=parent_perf))

        # write next generation to disk and add to queue for training
        for agent in offspring:
            agent_path = os.path.join(self.checkpoint_dir,
                                      f"agent-{agent['id']:03}.pth")
            torch.save(child_states[agent['id']], agent_path)
            self.pop_start.put(agent)

        # add survivors to training queue
        for survivor in survivors:
            self.pop_start.put(survivor)

    def run(self):
        # last generation will only be trained
        while self.gen.value < self.max_gen - 1:
            if self.pop_start.empty() and self.pop_end.full():
                print(f"Generation {self.gen.value} trained,"
                      "exploit and explore\n", flush=True)
                with self.gen.get_lock():
                    self.gen.value += 1
                try:
                    self.nextGen()
                except KeyboardInterrupt:
                    break
        # out of while loop, add to gen so train workers stop
        with self.gen.get_lock():
            self.gen.value += 1


def init_hyper_pars(hyper_range):
    rng = np.random.default_rng()
    hyper_pars = {}
    for key, (lo, hi, type_str) in hyper_range.items():
        if type_str == "int":
            hyper_pars[key] = rng.integers(low=lo, high=hi)
        elif type_str == "double":
            # linear in exponent
            exponent = rng.uniform(low=np.log(lo), high=np.log(hi))
            hyper_pars[key] = np.exp(exponent)
        else:
            print(f"the given type {type_str} is not supported!",
                  flush=True)
            raise ValueError
    return hyper_pars


def parse_cmd():
    parser = OptionParser(description=__doc__)
    parser.add_option("--checkdir", type="string", dest="checkdir",
                      help="Set the directory where checkpoints are saved")
    parser.add_option("--basename", type="string", dest="basename",
                      help="Set the basename of the dataset")
    parser.add_option("--load", type="string", dest="load", default=None,
                      help="Set the directory where to load network"
                      " states from. This is assumed to be in the "
                      "checkpoint directory.")
    parser.add_option("--savedir", type="string", dest="savedir",
                      default="PBT-results",
                      help="Set the directory to save to")

    options, _ = parser.parse_args()
    return options


if __name__ == "__main__":
    t0 = time.time()
    hyper_info = {'lr': (1e-8, 1, "double")}
    options = parse_cmd()

    n_pop = 80
    n_winners = 25
    carry_over = dict(loss=5, loss_mse=0)
    n_workers = 4
    step_epochs = 1
    early_stop = 10
    max_gen = 75
    perf_bias = dict(loss=.6, loss_mse=.4)
    checkpoint_dir = os.path.join(options.checkdir, options.basename)
    if not os.path.exists(checkpoint_dir):
        os.makedirs(checkpoint_dir)
    load_dir = None
    if options.load is not None:
        load_dir = os.path.join(options.checkdir, options.load)
        assert os.path.exists(load_dir), (f"Load directory {load_dir} does"
                                          " not exist")

    # for keeping track of jobs in population
    mp = mp.get_context("forkserver")
    pop_start = mp.Queue(maxsize=n_pop)
    pop_end = mp.Queue(maxsize=n_pop)

    # for keeping track of current epoch and history
    gen = mp.Value('i', 0)
    hist = mp.Manager().list()

    # set up the population
    for i in range(n_pop):
        # init all agents
        hyper_pars = init_hyper_pars(hyper_info)
        pop_start.put(dict(id=i, hyper_pars=hyper_pars, perf=None))

    # set up model and datasets
    # due to some issue with multiprocessing in webdataset and dataloader when
    # spawning multiple workers that load data
    load_workers = 0
    # this returns ((dataset, workers), (dataset, workers))
    datasets = load_from_metadata("/project/gravwav/kkoender/"
                                  "datasets/IMRPhenomD",
                                  options.basename, 400, 1200,
                                  load_workers, loader=False)

    gwaves, clss = next(iter(datasets[0][0]))
    segment_length = gwaves.shape[-1]
    net_kwargs = dict(segment_length=segment_length)
    net = GabbardNet

    device1 = torch.device('cuda:0')
    device2 = torch.device('cuda:1')
    loss_fn = torch.nn.BCEWithLogitsLoss()
    optim = torch.optim.Adam

    # set up workers

    workers = [Worker(ID, gen, max_gen, net, loss_fn, optim, device1,
                      datasets, step_epochs, checkpoint_dir,
                      pop_start, pop_end, net_kwargs=net_kwargs,
                      load_dir=load_dir)
               for ID in range(n_workers)]

    workers += [Worker(ID + n_workers, gen, max_gen, net, loss_fn,
                       optim, device2, datasets, step_epochs, checkpoint_dir,
                       pop_start, pop_end, net_kwargs=net_kwargs)
                for ID in range(n_workers)]

    workers += [Breeder(gen, max_gen, pop_start, pop_end, checkpoint_dir,
                        hyper_info, max_bits=64, carry_over=carry_over,
                        n_winners=n_winners, hist=hist, perf_bias=perf_bias,
                        early_stop=early_stop)]

    print("Starting workers")
    try:
        [w.start() for w in workers]
        [w.join() for w in workers]
    except Exception as e:
        print(e)
        print("Exiting all Processes!")
        # exit all Processes
        [w.terminate() for w in workers]
        raise SystemExit

    print("Done, saving", flush=True)

    # as the last gen is only trained and not exploited,
    # all should be in pop_end
    pop_final = []
    # again, wait a short bit for the queue to be properly filled
    time.sleep(1e-3)
    while not pop_end.empty():
        pop_final += [pop_end.get()]
    # for some reason they can still be in the other queue?
    while not pop_start.empty():
        pop_final += [pop_start.get()]

    # finally, copy manager list into regular list
    pop_hist = []
    for x in hist:
        pop_hist.append(x)

    if len(pop_final) < n_pop:
        print(f"WARNING: only {len(pop_final)}/{n_pop} agents being saved.")

    # save results
    if not os.path.exists(options.savedir):
        os.makedirs(options.savedir)
    save_path = os.path.join(options.savedir, f"PBT-{options.basename}.pkl")
    f = open(save_path, 'wb')
    pickle.dump(dict(pop=pop_final, pop_hist=pop_hist), f)
    f.close()

    t1 = time.time()
    # time formatting
    m, s = divmod(t1 - t0, 60)
    h, m = divmod(m, 60)
    s, m, h = int(s), int(m), int(h)
    print(f"Script took {str(h)+'hours ' if h > 0 else ''}"
          f"{str(m)+' minutes ' if m>0 else ''}"
          f"{str(s)+' seconds ' if s>0 else ''}")
