import os
from optparse import OptionParser
import pickle
import torch


def sortPop(pop):
    # sort for each performance measure, lower is better
    pop_ranks = {}
    for perf_key in pop[0]['perf'].keys():
        sorted_pop = sorted(pop, key=lambda x: x['perf'][perf_key])
        pop_ranks[perf_key] = sorted_pop
    return pop_ranks


def bestLoss(pop):
    pop_ranks = sortPop(pop)
    loss_rank = pop_ranks['loss']
    return loss_rank[0]


def moveBestModel(pop, basename):
    # get agent with best loss
    agent = bestLoss(pop)

    # move its model params to Models
    checkpoint_dir = os.path.join("PBT-Checkpoints", basename)
    checkpoint_path = os.path.join(checkpoint_dir,
                                   f"agent-{agent['id']:03}.pth")

    checkpoint = torch.load(checkpoint_path, map_location='cpu')
    model_state = checkpoint["model_state_dict"]

    model_dir = "Models"
    model_path = os.path.join(model_dir, basename+".pt")
    torch.save(model_state, model_path)


def parse_stdin():
    parser = OptionParser(description=__doc__)
    parser.add_option("--basename", type="string", dest="basename",
                      help="set the base name")

    options, _ = parser.parse_args()
    return options


if __name__ == "__main__":
    """
    Take best performing based on validation loss and move its model.
    """

    options = parse_stdin()
    basename = options.basename
    print(f"Finding and moving best performing model for {basename}")
    result_dir = "PBT-Results"
    result_file = f"PBT-{basename}.pkl"
    result_path = os.path.join(result_dir, result_file)
    with open(result_path, 'rb') as f:
        results = pickle.load(f)

    # move best model to Models
    moveBestModel(results['pop'], basename)
