import os
import numpy as np
from Networks import GabbardNet
from DataLoading import load_from_metadata
import torch
from tqdm import tqdm
from optparse import OptionParser
import matplotlib
matplotlib.use('Agg')
import h5py


def parse_stdin():
    parser = OptionParser(description=__doc__)
    parser.add_option("--basedir", type="string", dest="basedir",
                      help=("set the directory where the testing data are"
                            "located"))
    parser.add_option("--basenames", type="string", dest="basenames",
                      help="Set the base name of the testing files")
    parser.add_option("--gpu", type="string", dest="gpu", default='0',
                      help=("Set the indixes of gpus to use, seperated by only"
                            " a comma. Default is 0."))
    parser.add_option("--batch-size", type="int", dest="batch_size",
                      default=50, help="set the batch size")
    parser.add_option("--num-workers", type="int", dest="workers", default=1,
                      help="Set the maximum number of workers to use")
    parser.add_option("--shuffle", type="int", dest="shuffle", default=1000,
                      help="Set the buffer size for shuffling")
    parser.add_option("--save", action="store_true", dest="save",
                      help="Save the results in a hdf5 file")
    parser.add_option("--out-name", type="string", dest="out_name", default="",
                      help="Set the name for plots, results")
    parser.add_option("--plt-ext", type="string", dest="plt_ext",
                      default=".png", help="Set the extension for plots")

    options, _ = parser.parse_args()
    return options


def run_net(model, dataloader, device):
    """
    Runs the model over the entire dataset and returns the output values and
    targets.
    model: PyTorch Modele
    dataloader: PyTorch DataLoader
    """

    # make sure the model is in evaluation mode
    model.eval()
    model.to(device)

    # for returning results
    out_val, true_val = [], []

    # loop over data and collect results
    for batch, target in tqdm(dataloader):
        with torch.no_grad():
            batch = batch.to(device).float()

            out = model(batch)
            # TODO incorperate this in the evaluation mode?
            out_val.append(torch.sigmoid(out.detach().cpu()).numpy())
            # to be sure shape is correct
            true_val.append(target.squeeze().detach().cpu().numpy())
    del batch, target, out

    return np.concatenate(out_val), np.concatenate(true_val)


def runGabbardNet(options, basename, device):
    # load data from test set
    dataloader = load_from_metadata(options.basedir,
                                    basename,
                                    options.batch_size,
                                    options.shuffle,
                                    options.workers)

    # load the model
    batch, target = next(iter(dataloader))
    segment_length = batch.shape[-1]

    model_name = basename + ".pt"
    model_path = os.path.join("Models", model_name)
    model_state = torch.load(model_path, map_location='cpu')

    model = GabbardNet(segment_length=segment_length)
    model.load_state_dict(model_state)

    # run the network on the test dataset
    return run_net(model, dataloader, device)


if __name__ == "__main__":
    # TODO add CPU support
    # TODO run net and save results to disk

    options = parse_stdin()

    # setting up the gpu
    gpu_list = list(map(int, options.gpu.split(",")))
    id_max = np.max(gpu_list)
    visible_devs = ",".join(map(str, np.arange(id_max+1)))
    os.environ["CUDA_VISIBLE_DEVICES"] = visible_devs

    name_list = list(options.basenames.split(","))

    # TODO parallel
    device = gpu_list[0]

    fname = "-".join(["inference", "results", options.out_name]) + ".hdf5"
    with h5py.File(fname, 'w') as h5:
        for name in name_list:
            out_val, true_val = runGabbardNet(options, name, device)
            grp = h5.create_group(name)
            grp.create_dataset("out_val", data=out_val)
            grp.create_dataset("true_val", data=true_val)

