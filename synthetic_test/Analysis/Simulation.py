import lal
import lalsimulation as lalsim
import numpy as np

#TODO import these from module Dataset_Generation

# needed lal datatypes and rng initialization
# TODO use random seed
rng = lal.gsl_rng("ranlux", 127)  # wrapper for a gsl rng

# TODO: change this module to a class that can be instantiated depending on
# detector(s) and data specifications (such as sample rate and segment length)

def add_waveform(noise, waveform, psd, SNR = 10.):
    '''
    Adds the waveform to the noise, with the peak of the waveform randomly
    between 0.75 and 0.95 of the segment, and scales the waveform to have the required SNR.
    TODO SNR should be calculated with flow corresponding to the frequency at which the
    waveform starts in the segment.

    returns: 
    '''
    seg_len = len(noise)
    low_idx = int(0.75*seg_len)
    high_idx = int(0.95*seg_len)
    peak_window = np.arange(low_idx, high_idx)
    peak_idx = np.argmax(waveform)
    wave_len = len(waveform)
    place_idx = np.random.choice(peak_window)


    lead_zeros = place_idx - peak_idx
    if lead_zeros < 0:
        #we need to cut part of the signal at the start off
        data = waveform[-lead_zeros:]
    else:
        data = np.concatenate((np.zeros(lead_zeros), waveform))

    trail_zeros = seg_len - wave_len - lead_zeros
    if trail_zeros <0:
        #we need to cut part of the signal at the end off
        data = data[:trail_zeros]
    else:
        data = np.concatenate((data, np.zeros(trail_zeros)))
    
    #TODO check if this is done properly by only using the data within the segment
    epoch = lal.LIGOTimeGPS(0) 
    data_lal = lal.CreateREAL8TimeSeries("STRAIN", epoch, 0, 1./8192., lal.StrainUnit, seg_len)
    data_lal.data.data[:] = data

    SNR_curr = lalsim.MeasureSNR(data_lal, psd, 10, -1)
    #scale waveform to get required SNR
    data *= float(SNR)/SNR_curr

    return noise + data


def gen_psd(psd_name, flow, sample_rate, segment_length):
    '''
    Generates the psd using LALsimulation, based on the psd name, and returns
    it as a LAL frequency series.
    TODO add link to named psd functions
    '''

    df = sample_rate/segment_length
    epoch = lal.LIGOTimeGPS(0)  # gps time in seconds, nanoseconds

    # starting frequency is set to a very small number due to bug in LALSimNoisePSD.c
    psd = lal.CreateREAL8FrequencySeries('', epoch, 1e-25, df, lal.SecondUnit,
                                         segment_length//2 + 1)

    _name_prefix = 'SimNoisePSD'
    # Adding 'Ptr' to the end of a lalsuite function returns it as a SWIG
    # wrapped pointer to the C function, as opposed to returning a python
    callable
    _name_suffix = 'Ptr'

    psd_func = lalsim.__dict__[_name_prefix + psd_name + _name_suffix]
    lalsim.SimNoisePSD(psd, flow, psd_func)

    return psd


def gen_noise(psd, sample_rate, segment_length):
    '''
    Generates noise based on psd and returns it as array. Make sure to call
    this function after having
    defined the gsl rng!
    '''
    epoch = lal.LIGOTimeGPS(0)
    seg = lal.CreateREAL8TimeSeries("STRAIN", epoch, 0, 1./sample_rate,
                                    lal.StrainUnit, segment_length)

    lalsim.SimNoise(seg, seg.data.length, psd, rng)

    return seg.data.data


class SimulateWaveform:
    def __init__(self, seg_len, srate, approx, detector, snr=2):
        self.approx = approx
        self.lal_detector = lalsim.DetectorPrefixToLALDetector(detector)
        self.npars = 7
        self.snr = snr
        self.seg_len = seg_len
        # First we need some basics
        flow = 10
        self.srate = srate  # sampling rate in Hz

        # Create PSD
        psd_name = "aLIGOZeroDetHighPower"
        self.psd = gen_psd(psd_name, flow, self.srate, seg_len)

    def __call__(self, pars):
        # for use in swyft, the input needs to be an array or list
        m1, m2, incl, phase, ra, decl, pol = pars
        parameters = {}

        # make sure m1>m2
        if m1 < m2:
            parameters['m1'] = lal.MSUN_SI * m2
            parameters['m2'] = lal.MSUN_SI * m1
        else:
            parameters['m1'] = lal.MSUN_SI * m1
            parameters['m2'] = lal.MSUN_SI * m2

        parameters['S1x'] = 0.
        parameters['S1y'] = 0.
        parameters['S1z'] = 0.
        parameters['S2x'] = 0.
        parameters['S2y'] = 0.
        parameters['S2z'] = 0.
        parameters['distance'] = 1.e6 * lal.PC_SI
        parameters['inclination'] = incl
        parameters['phiRef'] = phase
        parameters['longAscNodes'] = 0.
        parameters['eccentricity'] = 0.
        parameters['meanPerAno'] = 0.
        # TODO don't hardcode these
        parameters['deltaT'] = 1./self.srate
        parameters['f_min'] = 10.0
        parameters['f_ref'] = 0.
        parameters['LALparams'] = None
        parameters['approximant'] = lalsim.GetApproximantFromString(self.approx)

        hplus, hcross = lalsim.SimInspiralTD(**parameters)

        # for some reason a GPS leap-second error can occur
        epoch = lal.LIGOTimeGPS(0)
        hplus.epoch = epoch
        hcross.epoch = epoch

        h = lalsim.SimDetectorStrainREAL8TimeSeries(hplus, hcross, ra, decl,
                                                    pol, self.lal_detector)

        # generate noise
        noise = gen_noise(self.psd, self.srate, self.seg_len)

        # TODO remove
        noise = np.zeros_like(noise)

        # add waveform
        data = add_waveform(noise, h.data.data, self.psd, self.snr)
        
        # remove simulation artifacts with a high-pass filter at 20Hz
        N = len(data)
        df = self.srate/N
        cutoff_idx = int(np.ceil(20/df))
        data_f = np.fft.rfft(data)
        data_f[:cutoff_idx] = 0
        data = np.fft.irfft(data_f)

        return dict(data=data)
