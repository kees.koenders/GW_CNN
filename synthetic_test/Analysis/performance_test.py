import numpy as np
import h5py
from tqdm import trange
import time

t0 = time.time()
with h5py.File("/local/kees.koenders/training_data_from_TD.hdf5", 'r') as f:
	data = f['data']
	t1 = time.time()	
	print("loading took %.2f s"%(t1-t0))
	t0 = time.time()
	N, n = data.shape
	perm = np.random.permutation(N)
	batch_size = 100
	p_batch = perm[:batch_size]
	t1 = time.time()
	print('permuting took %.2f s'%(t1-t0))
	
	#reading all data once
	#t0 = time.time()
	#for i in trange(N//batch_size):
	#	temp = data[i*batch_size:(i+1)*batch_size]
	#t1 = time.time()
	#print("loading all data once took %.2f s"%(t1-t0))
	
    #load the data once (seems to be caching?)
	test_batch_1 = data[np.sort(p_batch), :]	

	#with mask
	t0 = time.time()
	mask = np.zeros(N, dtype='bool')
	mask[p_batch] = 1

	test_batch_0 = data[mask, :]
	t1 = time.time()
	print("mask took %.6f s"%(t1-t0))

	#with indices
	t0 = time.time()
	test_batch_1 = data[np.sort(p_batch), :]	
	t1 = time.time()
	print("indices took %.6f s"%(t1-t0))

