from bitstring import BitArray
import numpy as np

# TODO relying on sorted order of _par_info.
_bit_count = {}
_par_info = {}

# generator
rng = np.random.default_rng()


def Init(par_info, max_bits=4, pop_size=None):
    """
    Initialize globals for all encoding. Returns initialized population.
    """
    tot_bits = 0
    for key, (lo, hi, type_str) in sorted(par_info.items(),
                                          key=lambda x: x[0]):
        print(type_str)
        if type_str == "int":
            _par_info[key] = (lo, hi, type_str)
            # bits needed to encode all integers in range
            n_bits = int(np.log2(hi-lo)) + 1
            if n_bits > max_bits:
                n_bits = max_bits
            _bit_count[key] = n_bits
            tot_bits += n_bits
            print(_bit_count)
            print(_par_info)
            continue
        elif type_str == "double":
            _par_info[key] = (lo, hi, type_str)
            _bit_count[key] = max_bits
            tot_bits += max_bits
            continue
        else:
            raise ValueError(f"Type {type_str} not supported!")


    if pop_size is None:
        return

    pop = []
    for i in range(pop_size):
        # init all zeroes
        chrom = BitArray(f"uint:{tot_bits}=0")
        chrom = Mutate(chrom, p=0.5)
        # Decode and add to list
        pop += [Decode(chrom)]

    return pop


def Encode(agent):
    """
    agent: dictionary of parameters.
    Given agent and info of parameters as a dictionary with tuples 
    (min_val, max_val, type), returns BitArray that represents these numbers in
    a "chromosome"
    """
    chromosome = BitArray()
    for key, (lo, hi, type_str) in sorted(_par_info.items(),
                                          key=lambda x: x[0]):
        if type_str == "int":
            value = agent[key]
            # bits needed to encode all integers in range
            n_bits = _bit_count[key]
            scaled_value = value - lo
            bits = BitArray(f"uint:{n_bits}={scaled_value}")
            chromosome += bits
            continue
        if type_str == "double":
            value = agent[key]
            n_bits = _bit_count[key]
            scale = (hi-lo)/(2**n_bits-1)
            scaled_value = int((value - lo)/scale)
            bits = BitArray(f"uint:{n_bits}={scaled_value}")
            # append bits to chromosome
            chromosome += bits
            continue

    return chromosome


def Decode(chromosome):
    """
    Decodes chromosome to agent
    """
    agent = {}
    for key, (lo, hi, type_str) in sorted(_par_info.items(),
                                          key=lambda x: x[0]):
        if type_str == "int":
            n_bits = _bit_count[key]
            # first n_bits bits encode this value
            agent[key] = chromosome[:n_bits].uint + lo
            # update
            chromosome = chromosome[n_bits:]
            continue
        if type_str == "double":
            n_bits = _bit_count[key]
            scale = (hi-lo)/(2**n_bits-1)
            agent[key] = chromosome[:n_bits].uint*scale + lo
            chromosome = chromosome[n_bits:]
            continue

    return agent


def Mutate(chromosome, p=0.03):
    """
    Each bit has chance p of flipping.
    """
    n_bits = len(chromosome)
    to_flip = rng.binomial(1, p, size=n_bits).astype(bool)
    flip_pos = np.arange(n_bits, dtype=int)[to_flip]
    chromosome.invert(flip_pos)
    return chromosome


def Crossover(parents, k_points=1):
    """
    Performs k-point crossover
    Assumes parents are provided as a list/array/tuple of BitArrays
    Returns: list containing two children
    """
    if len(parents) > 2:
        raise ValueError("Too many parents to perform crossover")

    n_bits = len(parents[0])

    def crossover(parents):
        """
        Performs a single crossover, returning two children
        """
        cross_idx = rng.integers(n_bits)

        # TODO extend to multiple parents?
        return (parents[0][:cross_idx] + parents[1][cross_idx:],
                parents[1][:cross_idx] + parents[0][cross_idx:])
    # initialize for recursion
    children = parents
    for idx in range(k_points):
        # apply crossover recursively
        children = crossover(children)

    return children

def sortPop(pop, pop_perf):
    """
    Returns a dict with performance keys, each having  a sorted list of agents
    idxs.
    """
    perf_keys = pop_perf[0].keys()
    pop_ranks = {}

    for perf_key in perf_keys:
        # sort population wrt perf_key
        # for all ranking statistics we have lower to be better
        # sorted list be of population indices!
        sorted_pop = [idx for idx, (_, agent)
                      in sorted(enumerate(zip(pop_perf, pop)),
                                key=lambda x: x[1][0][perf_key])]

        pop_ranks[perf_key] = sorted_pop

    return pop_ranks

def nextGen(pop, pop_perf, n_winners, carry_over=2, n_children=None,
            perf_bias=None):
    """
    pop is population of decoded chromosomes. n_winners is how many chromosomes enter
    the mating pool for each ranking. carry_over is how many are carried over
    to the next generation for each ranking.
    Implementation using binary encoding and standard GA techniques.

    n_winners tells us how many agents "win" wrt a ranking and are entered in
    mating.
    """

    # bias must be normalised
    if perf_bias is not None:
        tot = 0.
        for _, value in perf_bias.items():
            tot += value
        assert tot == 1.00, "bias not normalized!"

    perf_keys = pop_perf[0].keys()
    pop_ranks = {}

    for perf_key in perf_keys:
        # sort population wrt perf_key
        # for all ranking statistics we have lower to be better
        # sorted list be of population indices!
        sorted_pop = [idx for idx, (_, agent)
                      in sorted(enumerate(zip(pop_perf, pop)),
                                key=lambda x: x[1][0][perf_key])]

        pop_ranks[perf_key] = sorted_pop

    # select survivors
    survivors = []
    for perf_key in perf_keys:
        # pick top carry_over to survive
        for pick_idx in range(carry_over):
            survivor = pop[pop_ranks[perf_key][pick_idx]]
            # survivor = Encode(survivor)
            # survivor = Mutate(survivor)
            # survivor = Decode(survivor)
            survivors.append(survivor)

    n_survivors = len(survivors)
    pop_size = len(pop)
    if n_children is None:
        n_children = pop_size - n_survivors

    # select parents from top ranking agents
    """
    For now just select equally from rankings.
    TODO: include biased selection, using Benford's law.
    """

    n_ranks = len(pop_ranks)

    p_base = 1./(n_winners)
    p = np.zeros(pop_size)  # array to hold parent-idx - probability values

    # build the probability dict/table
    # TODO make sparse
    for perf_key in perf_keys:
        # take the top performing agents in each ranking
        winners_idx = pop_ranks[perf_key][:n_winners]
        for idx in winners_idx:
            if perf_bias is None:
                p[idx] += p_base/n_ranks
                continue
            p[idx] += p_base*perf_bias[perf_key]

    # If there is still room for children, use np.random.Generator.choice to
    # select 2 parents, using probability
    next_gen = []
    while len(next_gen) < n_children:
        parents_idx = rng.choice(pop_size, size=2, replace=False, p=p)
        # Get values and encode
        parents = [Encode(pop[idx]) for idx in parents_idx]
        # Mate
        children = Crossover(parents)
        # Mutate and Decode
        children = [Decode(Mutate(child)) for
                    child in children]
        if len(next_gen) + 2 <= n_children:
            next_gen += children
            continue

        # this only runs once when there is room for just one child
        next_gen += [children[rng.integers(2)]]

    return survivors + next_gen


def printPop(pop):
    # should be at least 5 + decimals
    min_width = 10
    # find out decimal places
    pop_mag = int(np.log10(len(pop)) + 1)

    # Heading
    pars = pop[0]
    line = "Pars"
    if len(line) > pop_mag:
        pop_mag = len(line)

    line = line + " "*(pop_mag-len(line))
    line += "|"
    for name in sorted(pars.keys()):
        line += f"{name:{min_width}}"
    print(line, flush=True)

    line = "="*pop_mag + "|" + "="*(len(line)-pop_mag-1)
    print(line, flush=True)

    # Values
    for agent, pars in enumerate(pop):

        line = f"{agent:{pop_mag}}|"

        for name, value in sorted(pars.items()):
            cell = f"{value:{min_width}.3g}"
            line += cell
        
        print(line, flush=True)


