import math


def conv1d(L_in, padding, dilation, kernel_size, stride):
    L_out = math.floor(
            (L_in + 2*padding - dilation*(kernel_size-1)-1)/stride + 1
    )

    return L_out
