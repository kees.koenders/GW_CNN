from optparse import OptionParser

def parse_command_line():
    parser = OptionParser(description = __doc__)
    parser.add_option("--cpu", action = "store_true", dest = "cpu", help = "Run on the cpu")
    parser.add_option("--spectral", action = "store_true", dest = "spectral", help = "Analyze the fourier transform")
    parser.add_option("--gpu", type =  "string", dest = "gpu", default = 0, help = "Set the indexes of gpus to use, seperated by only a comma. Default is 0")
    parser.add_option("--train-dir", type= "string", dest = "dir", help = "set the directory where the training files are located")
    parser.add_option("--train-file" , type= "string", dest = "file", help = "Set the base name of the training files")
    parser.add_option("--epochs", type = "int", dest = "epochs", default = 10, help = "set the amount of training epochs")
    parser.add_option("--batch-size", type = "int", dest = "batch_size", default = 50, help = "set the batch size")
    parser.add_option("--num-workers", type="int", dest = "workers", default = 1, help = "Set the maximum number of workers to use")
    parser.add_option("--shuffle", type="int", dest ="shuffle", default = 1000, help = "Set the buffer size for shuffling")
    parser.add_option("--step-losses", action = "store_true", dest = "step_losses", help = "Record the losses at each opimizer step, WARNING: this comes at a extremely significant performance penalty!")
    parser.add_option("--save-val", action = "store_true", dest = "save_val", help = "Save the validation results in a hdf5 file")
    parser.add_option("--out-name", type="string", dest = "out_name", default = "", help = "Set the name for plots, validation results")
    parser.add_option("--plt-ext", type = "string", dest = "plt_ext", default = ".png", help = "Set the extension for plots")
    parser.add_option("--lr", type = "float", dest = "lr", default = 1e-5 , help = "Set the learning rate for the optimizer")

    options, filenames = parser.parse_args()
    return options, filenames

options, filnames = parse_command_line()
#needs to be done before the other imports
import os
import sys
import numpy as np
#for some reason pytorch (atleast dataparallel) needs this to be incrementing by 1 from 0. So = "2"  will never work... weird behavior
gpu_list = list(map(int,options.gpu.split(",")))
id_max = np.max(gpu_list)
visible_devs = ",".join(map(str,np.arange(id_max+1)))
os.environ["CUDA_VISIBLE_DEVICES"] = visible_devs

#rest of imports
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optimizer
from torch.optim import Optimizer
import torch.fft

import webdataset as wds
import h5py
import numpy as np
import math

import time

import subprocess
from tqdm import tqdm

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.ioff()

from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score

class Nadam(Optimizer):
    """
    TAKEN FROM rwightman on github:  https://github.com/rwightman/pytorch-commands/blob/master/optim/nadam.py
    Implements Nadam algorithm (a variant of Adam based on Nesterov momentum).
    It has been proposed in `Incorporating Nesterov Momentum into Adam`__.
    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 2e-3)
        betas (Tuple[float, float], optional): coefficients used for computing
            running averages of gradient and its square
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        schedule_decay (float, optional): momentum schedule decay (default: 4e-3)
    __ http://cs229.stanford.edu/proj2015/054_report.pdf
    __ http://www.cs.toronto.edu/~fritz/absps/momentum.pdf
    """

    def __init__(self, params, lr=1e-5, betas=(0.9, 0.999), eps=1e-8,
                 weight_decay=0, schedule_decay=4e-3):
        defaults = dict(lr=lr, betas=betas, eps=eps,
                        weight_decay=weight_decay, schedule_decay=schedule_decay)
        super(Nadam, self).__init__(params, defaults)

    def step(self, closure=None):
        """Performs a single optimization step.
        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                grad = p.grad.data
                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    state['m_schedule'] = 1.
                    state['exp_avg'] = grad.new().resize_as_(grad).zero_()
                    state['exp_avg_sq'] = grad.new().resize_as_(grad).zero_()

                # Warming momentum schedule
                m_schedule = state['m_schedule']
                schedule_decay = group['schedule_decay']
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                beta1, beta2 = group['betas']
                state['step'] += 1
                t = state['step']

                if group['weight_decay'] != 0:
                    grad = grad.add(group['weight_decay'], p.data)

                momentum_cache_t = beta1 * \
                    (1. - 0.5 * (0.96 ** (state['step'] * schedule_decay)))
                momentum_cache_t_1 = beta1 * \
                    (1. - 0.5 * (0.96 ** ((state['step']+ 1) * schedule_decay)))
                m_schedule_new = m_schedule * momentum_cache_t
                m_schedule_next = m_schedule * momentum_cache_t * momentum_cache_t_1
                state['m_schedule'] = m_schedule_new

                # Decay the first and second moment running average coefficient
                #changed .add_ since it was deprecated, same for addcmul
                exp_avg.mul_(beta1).add_(grad, alpha = 1. - beta1)
                exp_avg_sq.mul_(beta2).addcmul_(grad, grad, value = 1. - beta2)
                exp_avg_sq_prime = exp_avg_sq.div(1. - beta2 ** state['step'])
                denom = exp_avg_sq_prime.sqrt_().add_(group['eps'])

                p.data.addcdiv_(grad, denom, value = -group['lr'] * (1. - momentum_cache_t) / (1. - m_schedule_new))
                p.data.addcdiv_(exp_avg, denom, value = -group['lr'] * momentum_cache_t_1 / (1. - m_schedule_next))

        return loss

class test_net(nn.Module):

    def __init__(self, segment_length = 100, spectral = False):
        super().__init__()
        self.spectral = spectral
        #define layers
        if spectral:
            n_channels = 2
        else:
            n_channels = 1
        self.conv1 = nn.Conv1d(n_channels, 8, kernel_size = 64, stride = 1)
        self.conv2 = nn.Conv1d(8,  8, kernel_size = 32, stride = 1) 
        self.conv3 = nn.Conv1d(8, 16, kernel_size = 32) 
        self.conv4 = nn.Conv1d(16, 16, kernel_size = 16) 
        self.conv5 = nn.Conv1d(16, 32, kernel_size = 16) 
        self.conv6 = nn.Conv1d(32, 32, kernel_size = 16) 
        #this allows (almost) any size for the input data, but I'm also too lazy to calculate
        #calculation *is* possible  and would be slightly more efficient than doing a partial forward pass at initialisation
        self.num_fc_in = self.num_features(self.conv(torch.randn(1,1,segment_length)))
        self.drop1 = nn.Dropout(p=0.5)
        self.fc1 = nn.Linear(self.num_fc_in, 64) 
        self.drop2 = nn.Dropout(p=0.5)
        self.fc2 = nn.Linear(64, 64)
        #when using BCE, this should only output 1 number, not 2.
        self.fc3 = nn.Linear(64,1)

    def conv(self, x):
        #forward pass just the convolutional layers
        if self.spectral:
            x_fft = torch.fft.rfft(x)
            x_mag = torch.abs(x_fft)
            x_arg = torch.angle(x_fft)
            #concatenate magnitude and argument in channel dimension
            x = torch.cat((x_mag, x_arg), dim = 1) 
        x = self.conv1(x)
        x = F.elu(x)
        x = F.max_pool1d(x,8)

        x = self.conv2(x)
        x = F.elu(x)

        x = self.conv3(x)
        x = F.elu(x)
        x = F.max_pool1d(x,6)

        x = self.conv4(x)
        x = F.elu(x)

        x = self.conv5(x)
        x = F.elu(x)
        x = F.max_pool1d(x,4)

        x = self.conv6(x)
        x = F.elu(x)
        return x

    def num_features(self, x):
        #calculates the number of features when the tensor is flattened
        size = x.size()[1:] #ignore batch dimension
        count = 1
        for  s in size:
            count *= s
        return count


    def forward(self, x):
        #how does data flow through the layers
        x = self.conv(x)
        #make sure the shape is correct
        x = x.view(-1, self.num_fc_in)
        x = self.drop1(x)
        x = self.fc1(x)
        x = F.elu(x)
        
        x = self.drop2(x)
        x = self.fc2(x)
        x = F.elu(x)

        x = self.fc3(x)

        #loss has sigmoid built-in for numerical stability
        #the paper uses softmax, but that is only usefull  when doing multi-class
        #or when writing binary classification with two output neurons.
        return x 

class Scale(object):
    """scale all values by scale.
       This is made into a callable class, so it can be initialized with the right scaling parameter and then used directly with webdataset's data transform pipeline"""
    
    def __init__(self, scale):
        self.scale = scale
    
    def __call__(self, data):
        #assuming data is a numpy array 
        return data*self.scale

def train_transform(data):
    #reshape to have channel dimension (only 1 channel)
    return np.reshape(data, (1,-1))

def target_transform(cls):
    #make is the right shape
    return np.reshape(cls,1)

def identity(x):
    return x

use_cpu = False
#select the correct device
if torch.cuda.is_available() and not options.cpu:
    device = torch.device(f"cuda:{gpu_list[0]}")
    print("CUDA is available, running on:")
    subprocess.run(["nvidia-smi", "--query-gpu=gpu_name,memory.total,memory.free,utilization.gpu", "--format=csv", "-i", f"{options.gpu}"])
else:
    print("CUDA is unavailable or cpu was requested")
    device = torch.device("cpu")
    use_cpu = True #maybe this can be done by checking device?

#Load the data
meta_path = os.path.join(options.dir, f"{options.file}-metadata.tar")

#I have metadata stored in a seperate tar file
metadataset = (
    wds.Dataset(meta_path)
    .decode()
)
metadata = next(iter(metadataset))["pyd"]

n_train = metadata["n_train"]
n_val = metadata["n_test"]
train_shard_samples = metadata["train_shard_samples"]
val_shard_samples = metadata["val_shard_samples"]
train_shards = metadata["train_shards"]
val_shards = metadata["val_shards"]
#train_mag = metadata["train_mag"]
#val_mag = metadata["val_mag"]
train_shard_mag =metadata["train_shard_mag"]
val_shard_mag =metadata["val_shard_mag"]
std = metadata["std"]

#curly brace notation also works for {1..1}
train_path = os.path.join(options.dir, "training", f"{options.file}-train-{{{0:0{train_shard_mag + 1}d}..{train_shards - 1:0{train_shard_mag + 1}d}}}.tar") 
val_path = os.path.join(options.dir, "validation", f"{options.file}-validate-{{{0:0{val_shard_mag}d}..{val_shards - 1:0{val_shard_mag + 1}d}}}.tar")

"""TODO check if shuffle happens each EPOCH. My current understanding is:
Buffer size: buffer in which is shuffled
Each EPOCH a new iterator is build (since dataloader is a generator, e.g. can only iterate through it *once*.
Each time it is shuffled. Both on shard and data level.

The iterator is build:
First shards are shuffled by shuffling the urls/paths.
Then samples are shuffled in buffer size
"""

'''
Workers are handled as follows:
Each worker gets its own shard, collecting batches.
When done with a shard, a worker moves over to the next available shard, carrying the remainder shard_samples % batch_size.
This means workers DO NOT wait on other workers before moving over. So when no shard is available, it just releases the remainder samples.
Thus: batch_size/shard_remainder shards are needed for a worker to carry no more remainder, or the first whole multiple.
Total workers should be less than total shards divided by shards needed for each worker.
A single worker will always work properly.
TODO: figure out how this works when there is a final shard that has less samples than the others. I could see this become problematic when shards are shuffled...
TODO: should include the possibilty that shards have a varying amount of samples, this happens when the size limit is reached before the samples limit.
'''
#for now assume all shards have the same amount of samples, except for the last
shard_samples = max(train_shard_samples)

shard_rem = n_train % shard_samples
worker_rem = shard_samples % options.batch_size
if worker_rem > 0:
    print(f"For optimal I/O, select a batch size which is a divisor of {shard_samples}")
    #calculate least common multiple. This is natively implemented in the latest version of Python (3.9 as of writing) and numpy (1.20 as of writing). These are not installed on CIT.
    lcm_rem = worker_rem*options.batch_size // math.gcd(worker_rem, options.batch_size)
    #how many shards a worker needs to have no more remainder
    worker_shards = lcm_rem//worker_rem
else:
    worker_shards = 1

if worker_shards > train_shards:
    print("TRAINING: the number of shards needed ({worker_shards}) for workers to carry no remainder is larger than the total amound of shards ({val_shards}). Only a single worker will be used.")
    train_workers = 1
else:
    train_workers = train_shards//worker_shards

if train_workers > options.workers:
    print("If possible, please allow for more workers to increase performance. With current batch size, the optimal would be {train_workers}")
    train_workers = options.workers

if(train_workers < train_shards):
    print(f"Only using {train_workers} workers for training, while absoute optimal would be {train_shards}")

if worker_shards > val_shards:
    print(f"VALIDATION: the number of shards needed ({worker_shards}) for workers to carry no remainder is larger than the total amound of shards ({val_shards}). Only a single worker will be used.")
    val_workers = 1
else:
    val_workers = val_shards//worker_shards

if val_workers > options.workers:
    val_workers = options.workers

if(val_workers < val_shards):
    print(f"Only using {val_workers} workers for validation, while absoute optimal would be {val_shards}")

#TODO is it possible to write better code for the assignment of workers? For instance: if worker_shards is larger than half of the shards, we'd still want to use two workers but have only one carry a remainder
# which will be the actual remainder batch

#initialize callable class for scaling by std
scale = Scale(1/std)

#the length will be interpreted as EPOCH length, e.g. number of batches
#Turns out the remainder n_train % batch_size is given as a final batch
batch_remainder = n_train % options.batch_size
if batch_remainder > 0:
    print(f"Batch size is not a divisor of the total number of samples, the remainder is {batch_remainder}")

num_batches = n_train//options.batch_size + (batch_remainder>0)

train_set = (
    wds.Dataset(train_path, length=num_batches)
    .shuffle(options.shuffle)
    .decode() #automatically decodes .cls and .pyd data (and others, see documentation). For images you can specify. This took me too long to figure out...
    .to_tuple("pyd", "cls")
    .map_tuple(scale, identity) #scaling by std
    .map_tuple(train_transform, target_transform) #shaping the data properly
    .batched(options.batch_size)
)

train_loader = torch.utils.data.DataLoader(train_set, batch_size =None, shuffle = False, num_workers=train_workers)

#validation set is set up as a single batch. Hence length = 1 and a batch size of all validation data
val_set = (
    wds.Dataset(val_path, length=1)
    .decode()
    .to_tuple("pyd", "cls")
    .map_tuple(scale, identity)
    .map_tuple(train_transform, target_transform)
    .batched(n_val)
)

val_loader = torch.utils.data.DataLoader(val_set, batch_size=None, shuffle = False, num_workers = val_workers)

#data has to be of the shape (batch dim, channel dim, segment length)
#cast to float for now, as double gives errors with backward(), even after re-casting the model

#Initialise net
#hopefully this does not mess up the dataloader
gwave, cls = next(iter(train_loader))
segment_length = gwave.shape[-1]

if use_cpu:
    net = test_net(segment_length = segment_length, spectral = options.spectral).to(device)
else:
    #Let's go parallel
    net = nn.DataParallel(test_net(segment_length = segment_length, spectral = options.spectral).to(device), device_ids = gpu_list)


#training loop
loss = nn.BCEWithLogitsLoss()
#Nadam optimizer from paper
#optim = Nadam(net.parameters(), lr = 1e-5, betas=(0.9, 0.999), eps=1e-8, weight_decay=0, schedule_decay=4e-3)
optim = torch.optim.Adam(net.parameters(), lr = options.lr)
EPOCHS = options.epochs

LOSS_IN = []
ACC_IN = []
TPR_IN = []
FPR_IN = []
TNR_IN = []
FNR_IN = []

LOSS_OUT = []
ACC_OUT = []
TPR_OUT = []
FPR_OUT = []
TNR_OUT = []
FNR_OUT = []

#for recording each optimizer step
train_losses = []
val_losses = []

print("training network...")
t0 = time.time()
for epoch in range(EPOCHS):
    net.train()

    #start in-epoch loop
    for batch, target in tqdm(train_loader):

        #This has to be done before the optimizer step to be comparable to in-sample losses
        if(options.step_losses):
            with torch.no_grad():
                net.eval()
                batch, target = next(iter(val_loader))
                batch = batch.float().to(device)
                target = target.to(device)
                out = net(batch)

                #use .item to get the value in a python data type, so the tensor is garbage collected
                val_losses.append(loss(out.squeeze(), target.squeeze().float()).item())

            net.train()

        #move batch and target to device (usually gpu)
        #Also, convert to float as input data is too double...
        batch = batch.float()#.to(device)
        target = target.to(device)

        optim.zero_grad()
        out = net(batch)
        loss_val = loss(out.squeeze(), target.squeeze().float())
        loss_val.backward() #compute gradient
        optim.step() #update net parameters

        #store loss
        loss_in = loss_val.item()
        train_losses.append(loss_in)
    #end in-epoch loop



    del loss_val #not used from here onwards

    with torch.no_grad(): #saves some memory
        print("Running validation")
        net.eval()
        #in-sample validation, making sure that the temporary iterator is removed from memory quickly
        temp_iter = iter(train_loader)
        batch, target = next(temp_iter)
        out = net(batch.float()) 
        target = target.to(device)
        loss_in = loss(out.squeeze(), target.squeeze().float()).item()
        del temp_iter
        #output has shape Batch X 1
        out_class = torch.round(torch.sigmoid(out)).int().squeeze()
        #target has shape Batch
        true_class = target.squeeze()

        true_pos = float(torch.sum(out_class & true_class).item())
        false_neg = float(torch.sum(~out_class & true_class).item())
        false_pos = float(torch.sum(out_class & ~true_class).item())
        true_neg = float(torch.sum((~out_class & ~true_class)%2).item())

        #Can use len since out has been squeezed
        acc = torch.sum(out_class == true_class).item()/float(len(out_class))
 
        if true_pos+false_neg >0: 
            tpr = true_pos/(true_pos + false_neg) 
        else:
            tpr = 0.
        if false_pos + true_neg > 0:
            fpr = false_pos/(false_pos + true_neg)
        else:
            fpr = 0.
        if true_neg + false_pos >0:
            tnr = true_neg/(true_neg + false_pos)
        else:
            tnr = 0.
        if false_neg + true_pos >0:
            fnr = false_neg/(false_neg + true_pos)
        else:
            fnr = 0.
    
        LOSS_IN.append(loss_in)
        ACC_IN.append(acc)
        TPR_IN.append(tpr)
        FPR_IN.append(fpr)
        TNR_IN.append(tnr)
        FNR_IN.append(fnr)

        #out-of-sample validation
        batch, target = next(iter(val_loader))
        batch = batch.float().to(device)
        target = target.to(device)
        out = net(batch)

        #use .item to get the value in a python data type, so the tensor is garbage collected
        loss_out = loss(out.squeeze(), target.squeeze().float()).item()

        #out_class = torch.argmax(out, dim=1)
        #true_class = torch.argmax(target, dim=1)
        out_class = torch.round(torch.sigmoid(out)).int().squeeze()
        true_class = target.squeeze()

        true_pos = float(torch.sum(out_class & true_class).item())
        false_neg = float(torch.sum(~out_class & true_class).item())
        false_pos = float(torch.sum(out_class & ~true_class).item())
        true_neg = float(torch.sum((~out_class & ~true_class)%2).item())

        acc = torch.sum(out_class == true_class).item()/float(len(out_class))

        if true_pos+false_neg >0:
            tpr = true_pos/(true_pos + false_neg)
        else:
            tpr = 0.
        if false_pos + true_neg > 0:
            fpr = false_pos/(false_pos + true_neg)
        else:
            fpr = 0.
        if true_neg + false_pos >0:
            tnr = true_neg/(true_neg + false_pos)
        else:
            tnr = 0.
        if false_neg + true_pos >0:
            fnr = false_neg/(false_neg + true_pos)
        else:
            fnr = 0.

        LOSS_OUT.append(loss_out)
        ACC_OUT.append(acc)
        TPR_OUT.append(tpr)
        FPR_OUT.append(fpr)
        TNR_OUT.append(tnr)
        FNR_OUT.append(fnr)

        #Save final validation raw output
        if(epoch == EPOCHS -1):
            final_out_val = torch.sigmoid(out.detach().cpu()).numpy()
            final_target_val = target.detach().cpu().numpy()

        print("In-sample loss:%.4f,     out-of-sample loss: %.4f,   memory in use: %i MiB,  epoch: %i/%i" %(loss_in, loss_out, torch.cuda.memory_allocated(device)/2**20., epoch + 1, EPOCHS))

        ##free up memory, not strictly needed as these are re-assigned every loop. However, after the final loop they will stay in memory.
        del batch, target, out, out_class, true_class

t1 = time.time()
#time formatting
m, s = divmod(t1 - t0, 60)
h, m = divmod(m, 60)
s, m , h = int(s), int(m), int(h)
print(f"Training took {str(h)+'hours ' if h > 0 else ''}{str(m)+' minutes ' if m>0 else ''}{str(s)+' seconds ' if s>0 else ''}")

#Save validation results in hdf5 file
if(options.save_val):
    fname = "_".join(filter(None, ("validation_results", options.out_name))) + ".hdf5"
    fpath = os.path.join("Validation_Results", fname)

    with h5py.File(fpath, 'w') as h5:
        h5.create_dataset("train_losses", data = np.array(train_losses))
        h5.create_dataset("LOSS_IN", data = np.array(LOSS_IN))
        h5.create_dataset("LOSS_OUT", data = np.array(LOSS_OUT))
        h5.create_dataset("ACC_IN", data = np.array(ACC_IN))
        h5.create_dataset("ACC_OUT", data = np.array(ACC_OUT))
        h5.create_dataset("TPR_IN", data = np.array(TPR_IN))
        h5.create_dataset("TPR_OUT", data = np.array(TPR_OUT))
        h5.create_dataset("FPR_IN", data = np.array(FPR_IN))
        h5.create_dataset("FPR_OUT", data = np.array(FPR_OUT))
        h5.create_dataset("TNR_IN", data = np.array(TNR_IN))
        h5.create_dataset("TNR_OUT", data = np.array(TNR_OUT))
        h5.create_dataset("FNR_IN", data = np.array(FNR_IN))
        h5.create_dataset("FNR_OUT", data = np.array(FNR_OUT))
        h5.create_dataset("final_out", data = final_out_val)
        h5.create_dataset("final_target", data = final_target_val)

#plotting
n_row = 3
n_col = 2

fig = plt.figure(figsize = [12.8, 9.6], dpi = 150.)

ax = fig.add_subplot(n_row, n_col, 1)
ax.plot(LOSS_IN, label = f"training loss: {LOSS_IN[-1]: .2f}")
ax.plot(LOSS_OUT, label = f"validation loss: {LOSS_OUT[-1]: .2f}")
ax.set_ylim(0, 0.8)
ax.legend(loc="lower left")

ax = fig.add_subplot(n_row, n_col,2)
ax.plot(ACC_IN, label = f"training accuracy: {ACC_IN[-1]: .2f}")
ax.plot(ACC_OUT, label = f"validation accuracy: {ACC_OUT[-1]: .2f}")
ax.set_ylim(0, 1)
ax.legend(loc="lower right")

ax = fig.add_subplot(n_row, n_col, 3)
ax.plot(TNR_IN, label = f'training TNR: {TNR_IN[-1]: .2f}')
ax.plot(TNR_OUT, label = f'validation TNR: {TNR_OUT[-1]: .2f}')
ax.set_ylim(0, 1)
ax.legend(loc="lower right")

ax = fig.add_subplot(n_row, n_col, 4)
ax.plot(TPR_IN, label = f'training TPR: {TPR_IN[-1]}')
ax.plot(TPR_OUT, label = f'validation TPR: {TPR_OUT[-1]}')
ax.set_ylim(0, 1)
ax.legend(loc="lower right")

ax = fig.add_subplot(n_row, n_col, 5)
ax.plot(FNR_IN, label = f"training FNR: {FNR_IN[-1]: .2f}")
ax.plot(FNR_OUT, label = f"validation FNR: {FNR_OUT[-1]: .2f}")
ax.set_ylim(0, 1)
ax.legend(loc="upper right")

ax = fig.add_subplot(n_row, n_col, 6)
ax.plot(FPR_IN, label = f"training FPR: {FPR_IN[-1]: .2f}")
ax.plot(FPR_OUT, label = f"validation FPR: {FPR_OUT[-1]: .2f}")
ax.set_ylim(0, 1)
ax.legend(loc="upper right")

fig_name = "_".join(filter(None, ("validation", options.out_name))) + options.plt_ext
plot_path = os.path.join("..","Plots", fig_name)
plt.savefig(plot_path)

fig2, ax2 = plt.subplots()
ax2.plot(train_losses, label="training (batch)")
ax2.set_title("Loss for each batch during training (before optim step)")
ax2.set_xlabel("Batch/step")
ax2.set_ylabel("Loss")


#also draw validation losses
if options.step_losses:
    ax2.plot(val_losses, label = "validation")
else:
    xs = np.arange(num_batches, num_batches*(EPOCHS +1), num_batches)
    ax2.plot(xs, LOSS_OUT, label = "validation")
    ax2.plot(xs, LOSS_IN, label="training (epoch)")

ax2.set_ylim(0, 0.8)
ax2.legend(loc = "lower left")

fig_name = "_".join(filter(None, ("train_losses", options.out_name))) + options.plt_ext
plot_path = os.path.join("..","Plots", fig_name)
plt.savefig(plot_path)


#weirdly, sklearn names the classifier output "target"
y_score, y_true = final_out_val[:,0], final_target_val[:,0]
fprs, tprs, thresholds = roc_curve(y_true, y_score)
auc = roc_auc_score(y_true, y_score)

#plot
fig3, ax3 = plt.subplots()
ax3.plot(fprs, tprs, label = f"Test Net(AUC={auc:.2f})")
ax3.set_yscale("log")
ax3.set_xscale("log")
ax3.set_title("ROC")
ax3.set_xlabel("false positive rate")
ax3.set_ylabel("true positive rate")
ax3.legend(loc = "lower right")

fig_name = "_".join(filter(None, ("ROC", options.out_name))) + options.plt_ext
plot_path = os.path.join("..","Plots", fig_name)
plt.savefig(plot_path)
