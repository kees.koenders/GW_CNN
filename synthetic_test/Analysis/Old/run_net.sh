#!/bin/bash

python3 test_net.py --train-dir /local/kees.koenders/shards_SNR2/ --train-file SNR2 --epochs 300 --batch-size 400 --num-workers 42 --shuffle 2400 --save-val --out-name snr2_e300_b400_lr6e-6 --plt-ext .svg --gpu 1 --lr 6e-6
python3 test_net.py --train-dir /local/kees.koenders/shards_SNR2/ --train-file SNR2 --epochs 300 --batch-size 400 --num-workers 42 --shuffle 2400 --save-val --out-name snr2_e300_b400_lr7e-6 --plt-ext .svg --gpu 1 --lr 7e-6
python3 test_net.py --train-dir /local/kees.koenders/shards_SNR2/ --train-file SNR2 --epochs 300 --batch-size 400 --num-workers 42 --shuffle 2400 --save-val --out-name snr2_e300_b400_lr8e-6 --plt-ext .svg --gpu 1 --lr 8e-6
python3 test_net.py --train-dir /local/kees.koenders/shards_SNR2/ --train-file SNR2 --epochs 300 --batch-size 400 --num-workers 42 --shuffle 2400 --save-val --out-name snr2_e300_b400_lr9e-6 --plt-ext .svg --gpu 1 --lr 9e-6
python3 test_net.py --train-dir /local/kees.koenders/shards_SNR2/ --train-file SNR2 --epochs 300 --batch-size 400 --num-workers 42 --shuffle 2400 --save-val --out-name snr2_e300_b400_lr1e-5 --plt-ext .svg --gpu 1 --lr 1e-5
