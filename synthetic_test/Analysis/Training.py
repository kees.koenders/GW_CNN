import torch
import numpy as np
from tqdm import tqdm
from Networks import class_perf


class Nadam(torch.optim.Optimizer):
    ''' Implements Nadam algorithm (a variant of Adam based on Nesterov momentum).
    TAKEN FROM rwightman on github:
    https://github.com/rwightman/pytorch-commands/blob/master/optim/nadam.py

    It has been proposed in `Incorporating Nesterov Momentum into Adam`__.
    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 2e-3)
        betas (Tuple[float, float], optional): coefficients used for computing
            running averages of gradient and its square
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        schedule_decay (float, optional):
            momentum schedule decay (default: 4e-3)
    __ http://cs229.stanford.edu/proj2015/054_report.pdf
    __ http://www.cs.toronto.edu/~fritz/absps/momentum.pdf
    '''

    def __init__(self, params, lr=1e-5, betas=(0.9, 0.999), eps=1e-8,
                 weight_decay=0, schedule_decay=4e-3):
        defaults = dict(lr=lr, betas=betas, eps=eps,
                        weight_decay=weight_decay,
                        schedule_decay=schedule_decay)
        super(Nadam, self).__init__(params, defaults)

    def step(self, closure=None):
        """Performs a single optimization step.
        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                grad = p.grad.data
                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    state['m_schedule'] = 1.
                    state['exp_avg'] = grad.new().resize_as_(grad).zero_()
                    state['exp_avg_sq'] = grad.new().resize_as_(grad).zero_()

                # Warming momentum schedule
                m_schedule = state['m_schedule']
                schedule_decay = group['schedule_decay']
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                beta1, beta2 = group['betas']
                state['step'] += 1

                if group['weight_decay'] != 0:
                    grad = grad.add(group['weight_decay'], p.data)

                momentum_cache_t = beta1 * \
                    (1. - 0.5 * (0.96 ** (state['step'] * schedule_decay)))
                momentum_cache_t_1 = beta1 * \
                    (1. - 0.5 * (0.96 ** ((state['step'] + 1) *
                                          schedule_decay)))
                m_schedule_new = m_schedule * momentum_cache_t
                m_schedule_next = m_schedule * momentum_cache_t * \
                    momentum_cache_t_1
                state['m_schedule'] = m_schedule_new

                # Decay the first and second moment running average coefficient
                # changed .add_ since it was deprecated, same for addcmul
                exp_avg.mul_(beta1).add_(grad, alpha=1. - beta1)
                exp_avg_sq.mul_(beta2).addcmul_(grad, grad, value=1. - beta2)
                exp_avg_sq_prime = exp_avg_sq.div(1. - beta2 ** state['step'])
                denom = exp_avg_sq_prime.sqrt_().add_(group['eps'])

                p.data.addcdiv_(grad, denom,
                                value=-group['lr'] *
                                (1. - momentum_cache_t) /
                                (1. - m_schedule_new))
                p.data.addcdiv_(exp_avg, denom,
                                value=-group['lr'] *
                                momentum_cache_t_1 /
                                (1. - m_schedule_next))

        return loss


class Trainer:
    """
    Generic class that can train a given model, save or load it and the
    optimizer from disk.
    """

    def __init__(self, model, loss_fn, optim, train_loader, val_loader, device,
                 epochs, pos=0):
        self.model = model
        self.loss_fn = loss_fn
        self.optim = optim
        self.train_loader = train_loader
        self.val_loader = val_loader
        self.device = device
        self.epochs = epochs
        self.pos = pos

    def train(self):
        self.model.train()
        val_losses, train_losses = [], []
        for epoch in range(self.epochs):
            for batch, target in tqdm(self.train_loader,
                                      desc=f"Training epoch {epoch}",
                                      position=self.pos,
                                      leave=None):
                batch = batch.to(self.device).float()
                target = target.to(self.device).float()

                self.optim.zero_grad()
                out = self.model(batch)
                loss = self.loss_fn(out, target)

                loss.backward()
                self.optim.step()

            # after-epoch evaluation
            del out, loss, batch, target

            val_losses.append(self.infer(self.val_loader))
            # only do part of the dataset to asses loss, saving time
            # TODO make user accesible
            train_losses.append(self.infer(self.train_loader, n_batches=2))
        return val_losses, train_losses

    def infer(self, dataloader, n_batches=None):
        self.model.eval()
        loss_tot = 0.
        n_samples = 0.
        for idx, (batch, target) in enumerate(tqdm(dataloader,
                                                   desc="Inference",
                                                   position=self.pos,
                                                   leave=None)):
            batch = batch.to(self.device).float()
            target = target.to(self.device).float()
            out = self.model(batch)
            loss_tot += len(batch)*self.loss_fn(out, target).item()
            n_samples += len(batch)
            del batch, target, out
            if n_batches is not None:
                # early stopping
                if idx + 1 == n_batches:
                    break
        return loss_tot/n_samples

    def set_optim_pars(self, optim_pars):
        """
        Sets the optimizer parameters accordingly.
        """
        for name, value in optim_pars:
            for param_group in self.optim.state_dict()['param_groups']:
                # in case there are multipe param groups
                # TODO support per-layer learning parameters
                param_group[name] = value

    def save_state(self, path):
        """
        Saves the current state of the network and its optimizer to path.
        """
        state = dict(model_state_dict=self.model.state_dict(),
                     optim_state_dict=self.optim.state_dict())
        torch.save(state, path)

    def load_state(self, path):
        """
        Loads a state of the network and its optimizer from path.
        """
        state = torch.load(path)
        self.model.load_state_dict(state['model_state_dict'])
        self.optim.load_state_dict(state['optim_state_dict'])


def Train(model, loss_fn, optim, train_loader, val_loader, device, epochs,
          ext_perf=False):
    '''
    Trains the model given loss function, optimizer, datasets, device, and
    epochs.

    model: torch.Module, initialized
    loss_fn: callable, must be differentiable
    optim: torch.optim.Optimizer, initialized
    epochs: int

    Returns: loss for each batch during training, training performance
    dictionary, validation performance dictionary
    '''

    if ext_perf:
        # TODO: allow user to define this externally
        # lists to save performance for each epoch
        perf_keys = ["loss", "acc", "tpr", "fpr", "tnr", "fnr"]
        train_perf = [[], [], [], [], [], []]

        val_perf = [[], [], [], [], [], []]

        performance = class_perf
    else:
        perf_keys = ["loss"]
        train_perf = [[]]
        val_perf = [[]]

    # for saving loss after each step
    train_losses = []


    for epoch in range(epochs):
        model.train()

        # start in-epoch loop
        for batch, target in tqdm(train_loader):
            # move batch and target to device (usually gpu)
            # TODO: depends on dataparallel
            # Also, convert to float as input data is double...
            batch = batch.float()
            target = target.to(device)

            optim.zero_grad()  # zero gradient
            out = model(batch)  # model output on batch
            # compute loss
            # TODO have loss be part of the forward pass, so it is aso fully
            # parallel
            loss_val = loss_fn(out.squeeze(), target.squeeze().float())
            loss_val.backward()  # compute gradient
            optim.step()  # update model parameters

            # store loss
            train_losses.append(loss_val.item())
        # end in-epoch loop

        # save current memory in use in MiB
        mem_use = torch.cuda.memory_allocated(device)/(2**20)

        del loss_val  # not used from here onwards

        with torch.no_grad():  # saves some memory
            print("Running validation...", end=" ")
            model.eval()
            # only doing this on a single batch for speed. Note that this batch
            # is furthest in "memory".
            # in-sample validation.
            for batch, target in train_loader:
                out = model(batch.float())
                target = target.to(device)
                train_loss = loss_fn(out.squeeze(),
                                     target.squeeze().float()).item()
                break

            if ext_perf:
                # cut at 0.5
                out_class = torch.round(torch.sigmoid(out)).int().squeeze()
                # target has shape Batch
                true_class = target.squeeze()

                # save performance metrics
                [metric.append(performance) for
                 metric, performance in
                 zip(train_perf,
                     (train_loss,)+performance(out_class, true_class))]
            else:
                train_perf[0].append(train_loss)

            print("In sample done...", end=" ")
            # out-of-sample validation
            val_losses, out_classes, true_classes = [], [], []
            for batch, target in val_loader:
                # this can be done faster by making it fullly parallel
                batch = batch.float().to(device)
                target = target.to(device)
                out = model(batch)

                # use .item to get the value in a python data type, so the
                # tensor is garbage collected
                val_losses.append(loss_fn(out.squeeze(),
                                          target.squeeze().float()).item())

                if ext_perf:
                    out_classes.append(torch.round(torch.sigmoid(out).detach())
                                       .int().squeeze())
                    true_classes.append(target.detach().squeeze())

            # calculate average loss
            val_loss = np.mean(val_losses)

            if ext_perf:
                # make single tensor
                out_class = torch.cat(out_classes)
                true_class = torch.cat(true_classes)
                # save performance metrics
                [metric.append(performance) for
                 metric, performance in
                 zip(val_perf,
                     (val_loss,)+performance(out_class, true_class))]
                del out_class, true_class

            else:
                val_perf[0].append(val_loss)

            print(f"Training loss: {train_loss:.4f},    "
                  f"validation loss: {val_loss:.4f},    "
                  f"memory in use: {mem_use:.3f} MiB,    "
                  f"epoch: {epoch+1}/{epochs}\n")

            # free up memory, not strictly needed as these are re-assigned
            # every loop. However, after the final loop they will stay in
            # memory.
            del batch, target, out
    # prepare output
    train_perf_dict = dict(zip(perf_keys, train_perf))
    val_perf_dict = dict(zip(perf_keys, val_perf))

    return train_losses, train_perf_dict, val_perf_dict
