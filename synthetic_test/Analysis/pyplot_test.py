import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
from matplotlib import patches
import numpy as np

xs = np.linspace(0, 10, 100)

fig = plt.figure(figsize=(4, 2))
spec = gs.GridSpec(nrows=1, ncols=2)
ax = fig.add_subplot(spec[0, 0])
ax.plot(xs, np.sin(xs))
ax = fig.add_subplot(spec[0,1])
ax.plot(xs, np.cos(xs))


plot_path = os.path.join("..", "Plots", "pyplot_test_1.svg")
fig.savefig(plot_path)

# create new fig and move old axes to it
# axs = fig.axes
# fig2 = plt.figure(figsize=(4, 4))
# spec2 = gs.GridSpec(ncols=2, nrows=2, figure=fig2)
# for idx, ax in enumerate(axs):
#     # remove axes from it's original Figure context
#     ax.remove()
# 
#     # set pointer to new figure
#     ax.figure = fig2
# 
#     # add it to relevant parts
#     fig2.axes.append(ax)
#     fig2.add_axes(ax)
# 
#     ax.reset_position()
# 
#     # place for the axes to go:
#     dummy = fig2.add_subplot(spec2[0, idx])
#     ax.set_position(dummy.get_position()) 
# 
#     #plot position
#     bb = dummy.get_position()
#     patch = patches.Rectangle((bb.xmin, bb.ymin), bb.width, bb.height)
#     ax.add_patch(patch)
#     # dummy.remove()
# 
# # close old fig
# plt.close(fig)

# add new axes
ax = fig.add_subplot(2, 1, 2)
ax.plot(xs, np.exp(xs))
fig.tight_layout()

plot_path = os.path.join("..", "Plots", "pyplot_test_2.svg")
fig.savefig(plot_path)
