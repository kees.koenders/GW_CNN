import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.fft

import swyft


def class_perf(out_class, true_class):
    '''
    Caculates the performance measures: true positive rate, false positive
    rate, true negative rate, false negative rate, and accuracy, given network
    output and
    labels.
    '''

    # TODO asert that both vectors are integer

    # make sure that we are working with squeezed vectors
    out_class = out_class.squeeze()
    true_class = true_class.squeeze()

    true_pos = float(torch.sum(out_class & true_class).item())
    false_neg = float(torch.sum(~out_class & true_class).item())
    false_pos = float(torch.sum(out_class & ~true_class).item())
    true_neg = float(torch.sum((~out_class & ~true_class) % 2).item())

    acc = torch.sum(out_class == true_class).item()/float(len(out_class))

    if true_pos+false_neg > 0:
        tpr = true_pos/(true_pos + false_neg)
    else:
        tpr = 0.
    if false_pos + true_neg > 0:
        fpr = false_pos/(false_pos + true_neg)
    else:
        fpr = 0.
    if true_neg + false_pos > 0:
        tnr = true_neg/(true_neg + false_pos)
    else:
        tnr = 0.
    if false_neg + true_pos > 0:
        fnr = false_neg/(false_neg + true_pos)
    else:
        fnr = 0.

    return tpr, fpr, tnr, fnr, acc


class GabbardNet(nn.Module):
    '''
    Convolution neural network based on the work from Gabbard et al.
    '''

    def __init__(self, segment_length = 100, spectral = False):
        super().__init__()
        self.spectral = spectral
        #define layers
        if spectral:
            n_channels = 2
        else:
            n_channels = 1
        self.conv1 = nn.Conv1d(n_channels, 8, kernel_size = 64, stride = 1)
        self.conv2 = nn.Conv1d(8,  8, kernel_size = 32, stride = 1)
        self.conv3 = nn.Conv1d(8, 16, kernel_size = 32)
        self.conv4 = nn.Conv1d(16, 16, kernel_size = 16)
        self.conv5 = nn.Conv1d(16, 32, kernel_size = 16)
        self.conv6 = nn.Conv1d(32, 32, kernel_size = 16)
        #this allows (almost) any size for the input data, but I'm also too lazy to calculate
        #calculation *is* possible  and would be slightly more efficient than doing a partial forward pass at initialisation
        self.num_fc_in = self.num_features(self.conv(torch.randn(1, 1, segment_length)))
        self.drop1 = nn.Dropout(p=0.5)
        self.fc1 = nn.Linear(self.num_fc_in, 64)
        self.drop2 = nn.Dropout(p=0.5)
        self.fc2 = nn.Linear(64, 64)
        # when using BCE, this should only output 1 number, not 2.
        # in the paper by Gabbard they use 2 outputs but the way their loss is
        # defined makes it effectively 1 output neuron with classic BCE
        self.fc3 = nn.Linear(64,1)

    def conv(self, x):
        #forward pass just the convolutional layers
        if self.spectral:
            x_fft = torch.fft.rfft(x)
            x_mag = torch.abs(x_fft)
            x_arg = torch.angle(x_fft)
            #concatenate magnitude and argument in channel dimension
            x = torch.cat((x_mag, x_arg), dim = 1)
        x = self.conv1(x)
        x = F.elu(x)
        x = F.max_pool1d(x,8)

        x = self.conv2(x)
        x = F.elu(x)

        x = self.conv3(x)
        x = F.elu(x)
        x = F.max_pool1d(x,6)

        x = self.conv4(x)
        x = F.elu(x)

        x = self.conv5(x)
        x = F.elu(x)
        x = F.max_pool1d(x,4)

        x = self.conv6(x)
        x = F.elu(x)
        return x

    def num_features(self, x):
        #calculates the number of features when the tensor is flattened
        size = x.size()[1:] #ignore batch dimension
        count = 1
        for  s in size:
            count *= s
        return count


    def forward(self, x):
        #how does data flow through the layers
        x = self.conv(x)
        #make sure the shape is correct
        x = x.view(-1, self.num_fc_in)
        x = self.drop1(x)
        x = self.fc1(x)
        x = F.elu(x)

        x = self.drop2(x)
        x = self.fc2(x)
        x = F.elu(x)

        x = self.fc3(x)

        #loss has sigmoid built-in for numerical stability
        #the paper uses softmax, but that is only usefull  when doing multi-class
        #or when writing binary classification with two output neurons.
        return x

    def performance(out_class, true_class):
        '''
        Helper function that connects the proper performance measures to the
        network.
        '''
        return class_perf(out_class, true_class)

'''
Networks below are for swyft!
'''

class HeadNet(swyft.Module):
    """
    Performs abstract feature extraction on timeseries data, returning a
    flattened feature vector. Based on work by Gabbard et al.
    """

    def __init__(self, sim_shapes, state_dict = None, spectral = False):
        super().__init__(sim_shapes = sim_shapes)
        self.spectral = spectral
        #define layers
        if spectral:
            n_channels = 2
        else:
            n_channels = 1
        segment_length = sim_shapes["data"][0]
        # define layers
        # TODO: normalization?
        self.onl_f1 = swyft.OnlineNormalizationLayer(torch.Size([segment_length]))
        self.conv1 = nn.Conv1d(n_channels, 8, kernel_size=int(64/n_channels), stride=1)
        self.conv2 = nn.Conv1d(8,  8, kernel_size=32, stride=1)
        self.conv3 = nn.Conv1d(8, 16, kernel_size=int(32/n_channels))
        self.conv4 = nn.Conv1d(16, 16, kernel_size=16)
        self.conv5 = nn.Conv1d(16, 32, kernel_size=int(16/n_channels))
        self.conv6 = nn.Conv1d(32, 32, kernel_size=int(16/n_channels))
        # this allows (almost) any size for the input data,
        # but I'm also too lazy to calculate
        # calculation *is* possible  and would be slightly more efficient than
        # doing a partial forward pass at initialisation

        # sim shapes is used in swyft to give you the shape of the simulator
        # output, in our case a 1d segment
        # swyft needs this to construct the tail network
        self.n_features = self.num_features(self.
                                           conv(torch.randn(1, 1,
                                                            segment_length)))

        # Load pre-trained values
        if state_dict is not None:
            self.load_state_dict(state_dict, strict=False)

    def conv(self, x):
        # forward pass just the convolutional layers
        if self.spectral:
            x_fft = torch.fft.rfft(x)
            x_mag = torch.abs(x_fft)
            x_arg = torch.angle(x_fft)
            #concatenate magnitude and argument in channel dimension
            x = torch.cat((x_mag, x_arg), dim = 1) 
        x = self.conv1(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 8)

        x = self.conv2(x)
        x = F.elu(x)

        x = self.conv3(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 6)

        x = self.conv4(x)
        x = F.elu(x)

        x = self.conv5(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 4)

        x = self.conv6(x)
        x = F.elu(x)
        return x

    def num_features(self, x):
        # calculates the number of features when the tensor is flattened
        size = x.size()[1:]  # ignore batch dimension
        count = 1
        for s in size:
            count *= s
        return count

    def forward(self, obs):
        x = obs['data']
        # TODO: online normilization
        x = x*5e21
        # insert channel dimension
        x = torch.unsqueeze(x,1)
        x = self.conv(x)
        # collapse feature vecture
        x = x.view(-1, self.n_features)

        return x


class SimpleHead(swyft.Module):
    """
    Smaller than Gabbard net.
    """

    def __init__(self, sim_shapes):
        super().__init__(sim_shapes = sim_shapes)
        segment_length = sim_shapes["data"][0]
        # define layers
        # TODO: normalization?
        self.onl_f = swyft.OnlineNormalizationLayer(torch.Size([segment_length]))
        self.conv1 = nn.Conv1d(1, 8, kernel_size=64, stride=1)
        self.conv2 = nn.Conv1d(8,  8, kernel_size=32, stride=1)
        self.conv3 = nn.Conv1d(8, 16, kernel_size=32)
        # this allows (almost) any size for the input data,
        # but I'm also too lazy to calculate
        # calculation *is* possible  and would be slightly more efficient than
        # doing a partial forward pass at initialisation

        # sim shapes is used in swyft to give you the shape of the simulator
        # output, in our case a 1d segment
        # swyft needs this to construct the tail network
        self.n_features = self.num_features(self.
                                           conv(torch.randn(1, 1,
                                                            segment_length)))

    def conv(self, x):
        # forward pass just the convolutional layers
        x = self.conv1(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 8)

        x = self.conv2(x)
        x = F.elu(x)

        x = self.conv3(x)
        x = F.elu(x)
        x = F.max_pool1d(x, 6)

        return x


    def num_features(self, x):
        # calculates the number of features when the tensor is flattened
        size = x.size()[1:]  # ignore batch dimension
        count = 1
        for s in size:
            count *= s
        return count

    def forward(self, obs):
        x = obs['data']
        # TODO: online normilization
        # x = x*5e21
        x = self.onl_f(x)
        # insert channel dimension
        x = torch.unsqueeze(x,1)
        x = self.conv(x)
        # collapse feature vecture
        x = x.view(-1, self.n_features)

        return x
