from optparse import OptionParser
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import h5py

from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from scipy import interpolate

import os

def parse_command_line():
    parser = OptionParser(description=__doc__)
    parser.add_option("--names", type="string", dest="names",
                      help="Give the names of the validation results to read"
                      "in, seperated by comma")
    parser.add_option("--out-name", dest="out_name",
                      help="Set the output name")
    parser.add_option("--plt-ext", dest="plt_ext", default=".png",
                      help="Set the figure extension, default is .png")

    return parser.parse_args()


# TODO: make into module that has a command line call option
# TDOO: naming convention for inference output
options, filenames = parse_command_line()

# remove whitespaces, split on comma, filter out empty strings
name_list = list(filter(None, options.names.replace(" ", "").split(",")))

fig, ax = plt.subplots()
prefix = "validation_results"
for name in name_list:
    fname = "_".join([prefix, name]) + ".hdf5"
    path = os.path.join("Validation_Results", fname)
    with h5py.File(path, 'r') as f:
        y_score, y_true = f["final_out"][:, 0], f["final_target"][:, 0]
    fprs, tprs, thresholds = roc_curve(y_true, y_score)
    # with current naming, this should give the snr. Filter is for security
    snr = next(filter(None, name.split("_")))

    # plot
    ax.plot(fprs, tprs, label=f"{snr}")

ax.set_xlim([1e-4, 1])
ax.set_ylim([1e-4, 1])
ax.set_yscale("log")
ax.set_xscale("log")
ax.set_title("ROC")
ax.set_xlabel("false positive rate")
ax.set_ylabel("true positive rate")
ax.legend(loc="lower right")

fig_name = "_".join(filter(None, ("ROC", options.out_name))) + options.plt_ext
plot_path = os.path.join("..", "Plots", fig_name)
plt.savefig(plot_path)
