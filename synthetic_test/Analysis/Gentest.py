import Genetics

b = dict(test=(5, 100, "int"), best=(943, 1578, "double"))

c = [dict(test=22, best=1022), dict(test=11, best=1298)]
n = 16

Genetics.Init(b, max_bits=n)
chroms = [Genetics.Encode(pars) for pars in c]

print("parents: ")
for chrom in chroms:
    print(chrom.bin)

print("\nchildren: ")
children = Genetics.Crossover(chroms)
for child in children:
    print(child.bin)
