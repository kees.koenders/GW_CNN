from optparse import OptionParser

def parse_command_line():
    parser = OptionParser(description=__doc__)
    parser.add_option("--cpu", action="store_true", dest="cpu",
                      help="Run on the cpu")
    parser.add_option("--spectral", action="store_true", dest="spectral",
                      help="Analyze the fourier transform")
    parser.add_option("--gpu", type="string", dest="gpu", default=0,
                      help="Set the indexes of gpus to use, seperated by"
                      "only a comma. Default is 0")
    parser.add_option("--dir", type="string", dest="dir",
                      help="set the directory where the"
                      "training files are located")
    parser.add_option("--basename", type="string", dest="basename",
                      help="Set the base name of the training files")
    parser.add_option("--epochs", type="int", dest="epochs", default=10,
                      help="set the amount of training epochs")
    parser.add_option("--batch-size", type="int", dest="batch_size",
                      default=50, help="set the batch size")
    parser.add_option("--num-workers", type="int", dest="workers", default=1,
                      help="Set the maximum number of workers to use")
    parser.add_option("--shuffle", type="int", dest="shuffle", default=1000,
                      help="Set the buffer size for shuffling")
    parser.add_option("--save-model", action="store_true", dest="save_model",
                      help="Save the trained model")
    parser.add_option("--out-name", type="string", dest="out_name", default="",
                      help="Set the name for plots, validation results")
    parser.add_option("--plt-ext", type="string", dest="plt_ext",
                      default=".png", help="Set the extension for plots")
    parser.add_option("--lr", type="float", dest="lr", default=1e-5,
                      help="Set the learning rate for the optimizer")

    options, filenames = parser.parse_args()
    return options, filenames


options, filnames = parse_command_line()
# needs to be done before the other imports
import os
import numpy as np
# for some reason pytorch (atleast dataparallel) needs this to be incrementing
# by 1 from 0. So = "2"  will never work... weird behavior
gpu_list = list(map(int, options.gpu.split(",")))
id_max = np.max(gpu_list)
visible_devs = ",".join(map(str, np.arange(id_max+1)))
os.environ["CUDA_VISIBLE_DEVICES"] = visible_devs

# rest of imports
from Networks import GabbardNet
from DataLoading import load_from_metadata
from Training import Train
import torch
from torch import nn
import numpy as np

import time

import subprocess
from tqdm import tqdm

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve



# set up dataloaders
train_loader, val_loader = load_from_metadata(options.dir, options.basename,
                                              options.batch_size,
                                              options.shuffle, options.workers)


gwave, cls = next(iter(train_loader))
segment_length = gwave.shape[-1]

gabbardNet = GabbardNet(segment_length=segment_length,
                        spectral=options.spectral)

# select the correct device
if torch.cuda.is_available() and not options.cpu:
    device = torch.device(f"cuda:{gpu_list[0]}")
    print("CUDA is available, running on:")
    subprocess.run(["nvidia-smi",
                    "--query-gpu=gpu_name,memory.total,"
                    "memory.free,utilization.gpu",
                    "--format=csv", "-i", f"{options.gpu}"])
    if len(gpu_list) > 1:
        # Let's go parallel
        net = nn.DataParallel(gabbardNet.to(device),
                              device_ids=gpu_list)
    else:
        # only 1 gpu
        net = gabbardNet.to(device)
else:
    print("CUDA is unavailable or cpu was requested")
    device = torch.device("cpu")
    gabbardNet.to(device)

print("\n")

# set up loss and optimizer
loss = nn.BCEWithLogitsLoss()
# Nadam optimizer from paper
# optim = Nadam(net.parameters(), lr = 1e-5, betas=(0.9, 0.999),
#               eps=1e-8, weight_decay=0, schedule_decay=4e-3)
optim = torch.optim.Adam(net.parameters(), lr=options.lr)


# train
t0 = time.time()

train_losses, train_perf, val_perf = Train(net, loss, optim, train_loader,
                                           val_loader, device, options.epochs)

t1 = time.time()
# time formatting
m, s = divmod(t1 - t0, 60)
h, m = divmod(m, 60)
s, m, h = int(s), int(m), int(h)
print(f"Training took {str(h)+'hours ' if h > 0 else ''}"
      f"{str(m)+' minutes ' if m>0 else ''}"
      f"{str(s)+' seconds ' if s>0 else ''}")

# save model
if(options.save_model):
    model_name = "_".join(filter(None, ("model", options.basename))) + ".pt"
    model_path = os.path.join("Models", model_name)
    if(use_cpu):
        torch.save(net.state_dict(), model_path)
    else:
        # data parallel has been used, saving works slightly different
        torch.save(net.module.state_dict(), model_path)


# TODO: use gridspec and automate plotting using dict.items()

# plotting
n_row = 1
n_col = 1

fig = plt.figure(figsize=[12.8, 9.6], dpi=150.)

ax = fig.add_subplot(n_row, n_col, 1)
ax.plot(train_perf["loss"],
        label=f"training loss: {train_perf['loss'][-1]: .2f}")
ax.plot(val_perf["loss"],
        label=f"validation loss: {val_perf['loss'][-1]: .2f}")
ax.set_ylim(0, 0.8)
ax.legend(loc="lower left")

# ax = fig.add_subplot(n_row, n_col, 2)
# ax.plot(train_perf['acc'],
#         label=f"training accuracy: {train_perf['acc'][-1]: .2f}")
# ax.plot(val_perf['acc'],
#         label=f"validation accuracy: {val_perf['acc'][-1]: .2f}")
# ax.set_ylim(0, 1)
# ax.legend(loc="lower right")
# 
# ax = fig.add_subplot(n_row, n_col, 3)
# ax.plot(train_perf['tnr'],
#         label=f"training TNR: {train_perf['tnr'][-1]: .2f}")
# ax.plot(val_perf['tnr'],
#         label=f"validation TNR: {val_perf['tnr'][-1]: .2f}")
# ax.set_ylim(0, 1)
# ax.legend(loc="lower right")
# 
# ax = fig.add_subplot(n_row, n_col, 4)
# ax.plot(train_perf['tpr'],
#         label=f"training TPR: {train_perf['tpr'][-1]: .2f}")
# ax.plot(val_perf['tpr'],
#         label=f"validation tpr: {val_perf['tpr'][-1]: .2f}")
# ax.set_ylim(0, 1)
# ax.legend(loc="lower right")
# 
# ax = fig.add_subplot(n_row, n_col, 5)
# ax.plot(train_perf['fnr'],
#         label=f"training FNR: {train_perf['fnr'][-1]: .2f}")
# ax.plot(val_perf['fnr'],
#         label=f"validation FNR: {val_perf['fnr'][-1]: .2f}")
# ax.set_ylim(0, 1)
# ax.legend(loc="upper right")
# 
# ax = fig.add_subplot(n_row, n_col, 6)
# ax.plot(train_perf['fpr'],
#         label=f"training FPR: {train_perf['fpr'][-1]: .2f}")
# ax.plot(val_perf['fpr'],
#         label=f"validation FPR: {val_perf['fpr'][-1]: .2f}")
# ax.set_ylim(0, 1)
# ax.legend(loc="upper right")
# 

fig_name = "_".join(filter(None,
                           ("validation",
                            options.out_name))) + options.plt_ext
plot_path = os.path.join("..", "Plots", fig_name)
plt.savefig(plot_path)

# train loss evolution per batch
fig2, ax2 = plt.subplots()
ax2.plot(train_losses, label="training (batch)")
ax2.set_title("Loss for each batch during training (before optim step)")
ax2.set_xlabel("Batch/step")
ax2.set_ylabel("Loss")

ax2.set_ylim(0, 0.8)
ax2.legend(loc="lower left")

fig_name = "_".join(filter(None,
                           ("train_losses",
                            options.out_name))) + options.plt_ext
plot_path = os.path.join("..", "Plots", fig_name)
plt.savefig(plot_path)

