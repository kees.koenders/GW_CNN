import numpy as np
import torch
import webdataset as wds

import math
import os


class Scale(object):

    """scale all values by scale.
       This is made into a callable class, so it can be initialized with the
       right scaling parameter and then used directly with webdataset's data
       transform pipeline"""

    def __init__(self, scale):
        self.scale = scale

    def __call__(self, data):
        # assuming data is a numpy array
        return data*self.scale


def train_transform(data):

    '''
    reshape to have channel dimension (only 1 channel)
    '''

    return np.reshape(data, (1, -1))


def target_transform(cls):

    '''
    reshape to only have 1 dimension.
    '''

    return np.reshape(cls, 1)


def identity(x):

    '''
    returns x
    '''

    return x


def optim_workers(n_workers, shard_samples, n_shards, batch_size):
    '''
    Workers are handled as follows:
    Each worker gets its own shard, collecting batches.
    When done with a shard, a worker moves over to the next available shard,
    carrying the remainder shard_samples % batch_size. This means workers DO
    NOT wait on other workers before moving over. So when no shard is available
    , it just releases the remainder samples. Thus: batch_size/shard_remainder
    shards are needed for a worker to carry no more remainder, or the first
    whole multiple. Total workers should be less than total shards divided by
    shards needed for each worker. A single worker will always work properly.
    TODO: figure out how this works when there is a final shard that has less
    samples than the others. I could see this become problematic when shards
    are shuffled...
    TODO: should include the possibilty that shards have a varying amount of
    samples, this happens when the size limit is reached before the samples
    limit.
    '''
    # for now assume all shards have the same amount of samples, except for the
    # last

    # shard_samples can/should be a list
    if isinstance(shard_samples, list):
        # for now we just use the first entry
        shard_samples = shard_samples[0]

    assert isinstance(shard_samples, int), ("shard_samples should be an"
                                            "integer!")

    # shard_rem = n_samples % shard_samples
    worker_rem = shard_samples % batch_size
    if worker_rem > 0:
        print(f"For optimal I/O, select a batch size which is a divisor of"
              f"{shard_samples}")
        # calculate least common multiple. This is natively implemented in the latest version of Python (3.9 as of writing) and numpy (1.20 as of writing). These are not installed on CIT.
        lcm_rem = worker_rem*batch_size // math.gcd(worker_rem, batch_size)
        # how many shards a worker needs to have no more remainder
        worker_shards = lcm_rem//worker_rem
    else:
        worker_shards = 1

    if worker_shards > n_shards:
        print(f"The number of shards needed ({worker_shards}) for workers to "
              "carry no remainder is larger than the total amound of shards "
              f"({n_shards}). Only a single worker will be used.")
        workers = 1
    else:
        workers = n_shards//worker_shards

    if workers > n_workers:
        print("\nIf possible, please allow for more workers to increase "
              "performance. With current batch size, the optimal would be "
              f"{workers}")
        workers = n_workers

    if(workers < n_shards):
        print(f"\nOnly using {workers} workers for training, while absoute "
              f"optimal would be {n_shards}")

    # TODO is it possible to write better code for the assignment of workers? For instance: if worker_shards is larger than half of the shards, we'd still want to use two workers but have only one carry a remainder
    # which will be the actual remainder batch

    return workers


def load_wds(path, batch_size, std, n_samples, shard_samples, n_shards,
             n_workers, shuffle=None):

    '''
    Loads data from the webdataset. Returns a pytorch dataloader..
    '''

    # shard_samples can/should be a list
    if isinstance(shard_samples, list):
        # for now we just use the first entry
        shard_samples = shard_samples[0]

    assert isinstance(shard_samples, int), ("shard_samples should be an"
                                            "integer!")

    workers = optim_workers(n_workers, shard_samples, n_shards, batch_size)

    if shuffle is None:
        shuffle = shard_samples

    # TODO assert other input parameters

    """TODO check if shuffle happens each EPOCH. My current understanding is:
    Buffer size: buffer in which is shuffled
    Each EPOCH a new iterator is build (since dataloader is a generator, e.g.
    can only iterate through it *once*.
    Each time it is shuffled. Both on shard and data level.

    The iterator is build:
    First shards are shuffled by shuffling the urls/paths.
    Then samples are shuffled in buffer size
    """

    # initialize callable class for scaling by std
    scale = Scale(1/std)

    # the length will be interpreted as EPOCH length, e.g. number of batches
    # Turns out the remainder n_train % batch_size is given as a final batch
    batch_remainder = n_samples % batch_size
    if batch_remainder > 0:
        print(f"\nBatch size is not a divisor of the total number of samples, "
              f"the remainder is {batch_remainder}")

    num_batches = n_samples//batch_size + (batch_remainder > 0)

    dataset = (
        wds.WebDataset(path)
        .shuffle(shuffle)
        .decode()  # automatically decodes .cls and .pyd data (and others, see documentation). For images you can specify. This took me too long to figure out...
        .to_tuple("pyd", "cls")
        .map_tuple(scale, identity)  # scaling by std
        .map_tuple(train_transform, target_transform)  # shaping the data properly
        .batched(batch_size)
        .with_length(num_batches)  # Creates __len__
    )

    return dataset, workers


def make_dataloader(dataset, n_workers):
    dataloader = torch.utils.data.DataLoader(dataset,
                                             batch_size=None, shuffle=False,
                                             num_workers=n_workers)
    return dataloader


def load_from_metadata(directory, basename, batch_size, shuffle, n_workers,
                       loader=True):

    '''
    Loads training and validation, or testing, sets using a metadata file.
    Returns pytorch dataloaders or wds datasets for both sets.
    '''

    meta_path = os.path.join(directory, "shards_"+basename,
                             f"{basename}-metadata.tar")

    metadataset = (
        wds.WebDataset(meta_path)
        .decode()
    )
    metadata = next(iter(metadataset))["pyd"]

    dset_type = metadata["type"]

    if dset_type == "train":
        datasets = load_training(directory, basename, metadata,
                                 batch_size, shuffle, n_workers)
        if loader:
            return [make_dataloader(dataset, workers)
                    for dataset, workers in datasets]
        else:
            return datasets

    elif dset_type == "test":
        dataset, workers = load_testing(directory, basename, metadata,
                                        batch_size, shuffle, n_workers)
        if loader:
            return make_dataloader(dataset, workers)
        else:
            return dataset, workers
    else:
        raise ValueError(f"The dataset type {dset_type} is not supported")


def load_training(directory, basename, metadata,
                  batch_size, shuffle, n_workers):
    '''
    Loads training and validation sets specified by the metadata.
    '''

    n_train = metadata["n_train"]
    n_val = metadata["n_val"]
    train_shard_samples = metadata["train_shard_samples"]
    val_shard_samples = metadata["val_shard_samples"]
    train_shards = metadata["train_shards"]
    val_shards = metadata["val_shards"]
    # train_mag = metadata["train_mag"]
    # val_mag = metadata["val_mag"]
    train_shard_mag = metadata["train_shard_mag"]
    val_shard_mag = metadata["val_shard_mag"]
    std = metadata["std"]

    shard_dir = os.path.join(directory, "shards_" + basename)

    # curly brace notation also works for {1..1}
    train_path = os.path.join(shard_dir, "training",
                              (f"{basename}-training-{{"
                               f"{0:0{train_shard_mag + 1}d}.."
                               f"{train_shards - 1:0{train_shard_mag + 1}d}"
                               "}.tar"))
    val_path = os.path.join(shard_dir, "validation",
                            (f"{basename}-validate-{{"
                             f"{0:0{val_shard_mag}d}.."
                             f"{val_shards - 1:0{val_shard_mag + 1}d}"
                             "}.tar"))

    return (load_wds(train_path, batch_size, std, n_train, train_shard_samples,
                     train_shards, n_workers, shuffle=shuffle),
            load_wds(val_path, n_val, std, n_val, val_shard_samples,
                     val_shards, n_workers))


def load_testing(directory, basename, metadata,
                 batch_size, shuffle, n_workers):
    """
    Loads a test set using a metadata file. Returns pytorch dataloader.
    """

    n_samples = metadata["n_test"]
    shard_samples = metadata["shard_samples"]
    shards = metadata["shards"]
    shard_mag = metadata["shard_mag"]
    std = metadata["std"]

    shard_dir = os.path.join(directory, "shards_" + basename)

    # curly brace notation also works for {1..1}
    test_path = os.path.join(shard_dir, "testing",
                             (f"{basename}-testing-{{{0:0{shard_mag + 1}d}.."
                              f"{shards - 1:0{shard_mag + 1}d}}}.tar"))

    return (load_wds(test_path, batch_size, std, n_samples, shard_samples,
                     shards, n_workers, shuffle=shuffle))
